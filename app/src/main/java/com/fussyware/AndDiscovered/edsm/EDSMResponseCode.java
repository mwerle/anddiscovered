package com.fussyware.AndDiscovered.edsm;

/**
 * Created by wes on 8/8/15.
 */
public enum EDSMResponseCode
{
    SYSTEM_ADDED(100),
    SYSTEM_NON_EXISTANT(101),
    SYSTEM_ADDED_WITH_COORDS(102),
    SYSTEM_COORDS_FOUND(104),
    SYSTEM_ALTERNATIVE_COORDS(105),
    SYSTEM_NO_MATCH(106),
    SYSTEM_REF_COUNT_EXCEEDED(107),
    SYSTEM_MIN_REF_NOT_MET(108),
    SYSTEM_COORDS_NOT_FOUND(109),
    SYSTEM_NAME_NOT_FOUND(110),
    UNKNOWN_ERROR(199),
    DISTANCE_SAVED(200),
    DISTANCE_NOT_NUMBER(201),
    DISTANCE_EXIST_CR_SAVED(202),
    DISTANCE_EXIST_CR_INC(203),
    SYSTEM_COORDS_FOUND2(300),
    SYSTEM_ALTERNATIVE_COORDS2(305),
    SYSTEM_NO_MATCH2(306),
    SYSTEM_REF_COUNT_EXCEEDED2(307),
    SYSTEM_MIN_REF_NOT_MET2(308);

    private int value;

    EDSMResponseCode(int value)
    {
        this.value = value;
    }

    public int value()
    {
        return value;
    }

    public static EDSMResponseCode findResponseCode(String value) {
        try {
            return findResponseCode(new Integer(value));
        } catch (Exception ignored) {
            return UNKNOWN_ERROR;
        }
    }

    public static EDSMResponseCode findResponseCode(int value)
    {
        for (EDSMResponseCode code : EDSMResponseCode.values()) {
            if (code.value() == value) {
                return code;
            }
        }

        return EDSMResponseCode.UNKNOWN_ERROR;
    }
}
