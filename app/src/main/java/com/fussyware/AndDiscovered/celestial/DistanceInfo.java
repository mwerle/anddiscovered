package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wboyd on 7/7/15.
 */
public class DistanceInfo
{
    private final String first;
    private String second;
    private double distance;

    protected final Object mutex = new Object();

    public DistanceInfo(String first, String second, double distance)
    {
        this.first = first;
        this.second = second;
        this.distance = (distance < 0) ? -distance : distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof DistanceInfo)) return false;

        DistanceInfo that = (DistanceInfo) o;

        if (Double.compare(that.distance, distance) != 0) return false;
        return !(first != null ? !first.equalsIgnoreCase(that.first) : that.first != null) && !(second != null ? !second.equalsIgnoreCase(that.second) : that.second != null);

    }

    @Override
    public int hashCode()
    {
        int result;
        long temp;
        result = first != null ? first.hashCode() : 0;
        result = 31 * result + (second != null ? second.hashCode() : 0);
        temp = Double.doubleToLongBits(distance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String getFirstSystem()
    {
        return first;
    }

    public String getSecondSystem()
    {
        synchronized (mutex) {
            return second;
        }
    }

    protected void setSecondSystem(String system)
    {
        synchronized (mutex) {
            second = system;
        }
    }

    public double getDistance()
    {
        synchronized (mutex) {
            return distance;
        }
    }

    protected void setDistance(double distance)
    {
        synchronized (mutex) {
            this.distance = (distance < 0) ? -distance : distance;
        }
    }

    @Override
    public String toString() {
        return "DistanceInfo{" +
                "distance=" + distance +
                ", first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}
