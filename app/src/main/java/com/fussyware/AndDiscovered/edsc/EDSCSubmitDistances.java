package com.fussyware.AndDiscovered.edsc;

import android.util.Pair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by wes on 6/25/15.
 */
public class EDSCSubmitDistances extends EDSCJSON
{
    public static final int VERSION = 2;
    public static final boolean DEFAULT_TEST = false;
    public static final String DEFAULT_COMMANDER = "";

    private boolean test = DEFAULT_TEST;
    private String commander = DEFAULT_COMMANDER;
    private String systemName;
    private ArrayList<Pair<String, Double>> toList;

    public EDSCSubmitDistances(String systemName, ArrayList<Pair<String, Double>> toList)
    {
        this.systemName = systemName;
        this.toList = toList;
    }

    public String getCommander()
    {
        synchronized (mutex) {
            return commander;
        }
    }

    public void setCommander(String commander)
    {
        synchronized (mutex) {
            this.commander = commander;
        }
    }

    public String getSystemName()
    {
        return systemName;
    }

    public ArrayList<Pair<String, Double>> getToList()
    {
        return toList;
    }

    @Override
    protected void fillJSON(JSONObject baseObject) throws JSONException
    {
        if (!DEFAULT_COMMANDER.equals(getCommander())) {
            baseObject.put("commander", getCommander());
        }

        {
            JSONObject o = new JSONObject();
            o.put("name", getSystemName());

            baseObject.put("p0", o);
        }

        {
            JSONArray array = new JSONArray();

            DecimalFormat df = new DecimalFormat(".##");
            df.setRoundingMode(RoundingMode.HALF_UP);

            for (Pair<String, Double> pair : toList) {
                JSONObject o = new JSONObject();

                o.put("name", pair.first);
                o.put("dist", df.format(pair.second));

                array.put(o);
            }

            baseObject.put("refs", array);
        }
    }
}
