package com.fussyware.AndDiscovered.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 11/1/15.
 */
public class GalnetNewsActivity extends BaseActivity
{
    private static final String LOG_NAME = GalnetNewsActivity.class.getSimpleName();

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.galnet_news_activity_title);
        setContentView(R.layout.galnet_activity_layout);

        webView = (WebView) findViewById(R.id.galnet_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new GalnetWebClient());

        if (savedInstanceState != null) {
            webView.loadUrl(savedInstanceState.getString("url"));
        } else {
            webView.loadUrl("https://community.elitedangerous.com/galnet");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("url", webView.getUrl());
    }

    private class GalnetWebClient extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Uri uri = Uri.parse(url);
            String host = uri.getHost();
            String path = uri.getPath();

            Log.d(LOG_NAME, "Host: " + host + ", Path: " + path);
            if ("community.elitedangerous.com".equals(host) &&
                (path != null) &&
                path.startsWith("/galnet")) {
                return false;
            }

            startActivity(new Intent(Intent.ACTION_VIEW, uri));
            return true;
        }
    }
}
