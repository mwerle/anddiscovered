package com.fussyware.AndDiscovered.edsc;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.fussyware.AndDiscovered.eddatabase.CmdrDbContract;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

/**
 * Created by wboyd on 7/7/15.
 */
public class EDSCSystemInfo extends SystemInfo
{
    private SQLiteDatabase db;
    private long id;

    EDSCSystemInfo(SQLiteDatabase db, long id, String system, Position xyz)
    {
        super(system, xyz);

        this.db = db;
        this.id = id;
    }

    @Override
    public void setPosition(Position xyz)
    {
        if ((xyz != null) && (getPosition() != xyz)) {
            super.setPosition(xyz);

            String selection = EDSCDbContract.EDSCSystems._ID + "=?";
            String[] selectionArgs = {Long.toString(id)};

            ContentValues values = new ContentValues();

            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD, xyz.x);
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD, xyz.y);
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD, xyz.z);

            db.update(EDSCDbContract.EDSCSystems.TABLE_NAME,
                      values,
                      selection,
                      selectionArgs);
        }
    }
}
