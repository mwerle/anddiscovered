package com.fussyware.AndDiscovered.edsm;

import com.fussyware.AndDiscovered.TrilatResponse;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by wes on 8/21/15.
 */
class StaticTrilatResponseFuture implements Future<TrilatResponse>
{
    private Position position;
    private int distances;

    StaticTrilatResponseFuture()
    {
        this(null, -1);
    }

    StaticTrilatResponseFuture(Position position, int distances)
    {
        this.position = position;
        this.distances = distances;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning)
    {
        return false;
    }

    @Override
    public boolean isCancelled()
    {
        return false;
    }

    @Override
    public boolean isDone()
    {
        return true;
    }

    @Override
    public TrilatResponse get() throws InterruptedException, ExecutionException
    {
        return new InnerTrilatResponse();
    }

    @Override
    public TrilatResponse get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException
    {
        return new InnerTrilatResponse();
    }

    private class InnerTrilatResponse implements TrilatResponse
    {
        @Override
        public Position getPosition()
        {
            return position;
        }

        @Override
        public int getDistancesProcessed()
        {
            return distances;
        }

        @Override
        public boolean isTrilatPerformed()
        {
            return (position != null);
        }
    }
}
