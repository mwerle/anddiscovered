package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 11/8/15.
 */
public enum ScanLevel
{
    Level_2(2),
    Level_3(3),
    Unknown(Integer.MAX_VALUE);

    public final int value;

    ScanLevel(int value)
    {
        this.value = value;
    }

    public static ScanLevel getScanLevel(int value)
    {
        for (ScanLevel level : ScanLevel.values()) {
            if (level.value == value) {
                return level;
            }
        }

        return Unknown;
    }
}
