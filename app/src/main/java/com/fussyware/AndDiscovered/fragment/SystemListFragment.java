package com.fussyware.AndDiscovered.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.fussyware.AndDiscovered.EDProxyIntentService;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.StarCoordinator;
import com.fussyware.AndDiscovered.adapter.SystemListCursorAdapter;
import com.fussyware.AndDiscovered.dialog.OkAlertDialogFragment;
import com.fussyware.AndDiscovered.preference.PreferenceTag;

import java.util.Date;

/**
 * Created by wes on 8/29/15.
 */
public class SystemListFragment extends Fragment
{
    private static final String LOG_NAME = SystemListFragment.class.getSimpleName();

    private SystemListCursorAdapter adapter;
    private SystemSelectedListener selectedListener;
    private ListView systemListView;

    private LocalBroadcastManager broadcastManager;
    private BroadcastReceiver broadcastReceiver;

    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.system_list_layout, container, false);

        broadcastManager = LocalBroadcastManager.getInstance(getActivity().getApplicationContext());
        broadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String action = intent.getAction();

                switch (action) {
                    case EDProxyIntentService.BROADCAST_SYSTEM_UPDATED:
                    case StarCoordinator.BROADCAST_SYSTEM_UPDATED:
                    case StarCoordinator.BROADCAST_DISTANCE_UPDATED:
                        Log.d(LOG_NAME, "Notify database changed: " + action);
                        adapter.notifyDatabaseChanged();
                        break;
                    default:
                        break;
                }
            }
        };

        adapter = new SystemListCursorAdapter(getActivity());

        systemListView = (ListView) layout.findViewById(R.id.LargeSystemList);
        systemListView.setAdapter(adapter);

        systemListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                SystemListCursorAdapter.Item item = adapter.getItem(position);

                if (item instanceof SystemListCursorAdapter.SystemEntryView) {
                    SystemListCursorAdapter.SystemEntryView entry = (SystemListCursorAdapter.SystemEntryView) item;

                    ((ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE)).setPrimaryClip(
                            ClipData.newPlainText("SystemName", entry.map.get("system")));

                    Toast.makeText(getActivity(),
                                   "Copied system [" +
                                   entry.map.get("system") +
                                   "] to the clipboard",
                                   Toast.LENGTH_SHORT).show();
                }

                return true;
            }
        });

        systemListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (selectedListener != null) {
                    SystemListCursorAdapter.Item item = adapter.getItem(position);

                    if (item instanceof SystemListCursorAdapter.SystemEntryView) {
                        SystemListCursorAdapter.SystemEntryView entry = (SystemListCursorAdapter.SystemEntryView) item;
                        selectedListener.onSystemSelected(entry.map.get("system"),
                                                          view,
                                                          position,
                                                          id);
                    }
                }
            }
        });

        prefListener = new SharedPreferences.OnSharedPreferenceChangeListener()
        {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
            {
                if (PreferenceTag.DISPLAY_DAYS.equals(key)) {
                    setDateRange(getDateRange(sharedPreferences.getInt(key, 7)));
                }
            }
        };

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        preferences.registerOnSharedPreferenceChangeListener(prefListener);

        setDateRange(getDateRange(preferences.getInt(PreferenceTag.DISPLAY_DAYS, 7)));

        IntentFilter filter = new IntentFilter();
        filter.addAction(EDProxyIntentService.BROADCAST_SYSTEM_UPDATED);
        filter.addAction(StarCoordinator.BROADCAST_SYSTEM_UPDATED);
        filter.addAction(StarCoordinator.BROADCAST_DISTANCE_UPDATED);

        broadcastManager.registerReceiver(broadcastReceiver, filter);

        return layout;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        broadcastManager.unregisterReceiver(broadcastReceiver);

        adapter.destroy();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (!(activity instanceof SystemSelectedListener)) {
            throw new ClassCastException("Activity is does not implement SystemSelectedListener.");
        }

        selectedListener = (SystemSelectedListener) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        selectedListener = null;

        if (prefListener != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    getActivity().getApplicationContext());
            preferences.unregisterOnSharedPreferenceChangeListener(prefListener);
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPostExecute(Void aVoid)
            {
                if (adapter.getCount() == 0) {
                    DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.system_list_no_items_title,
                                                                                R.string.system_list_no_items_message);
                    fragment.show(getFragmentManager(), FragmentTag.system_list_fragment.no_items_dialog);
                }
            }

            @Override
            protected Void doInBackground(Void... params)
            {
                while (!adapter.isReady()) {
                    SystemClock.sleep(120);
                }

                return null;
            }
        }.execute();
    }

    public void invalidate()
    {
        adapter.notifyDatabaseChanged();
    }

    public void setDateRange(Date dateRange)
    {
        if (systemListView != null) {
            SystemListCursorAdapter adapter = (SystemListCursorAdapter) systemListView.getAdapter();
            adapter.setDateRange(dateRange);
        }
    }

    private Date getDateRange(int value)
    {
        return new Date(new Date().getTime() - ((long) value * 24 * 60 * 60 * 1000));
    }

    public interface SystemSelectedListener
    {
        void onSystemSelected(String system, View view, int position, long id);
    }
}
