package com.fussyware.AndDiscovered.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by wes on 9/20/15.
 */
public class AutoFitGridRecyclerView extends RecyclerView
{
    private static final String LOG_NAME = AutoFitGridRecyclerView.class.getSimpleName();

    private GridLayoutManager layoutManager;
    private int columnWidth;

    public AutoFitGridRecyclerView(Context context)
    {
        super(context);
        init(context, null);
    }

    public AutoFitGridRecyclerView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public AutoFitGridRecyclerView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec)
    {
        super.onMeasure(widthSpec, heightSpec);

        if (columnWidth > 0) {
            int spanCount = Math.max(1, getMeasuredWidth() / columnWidth);
            layoutManager.setSpanCount(spanCount);
        }
    }

    private void init(Context context, AttributeSet attrs)
    {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.columnWidth
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            columnWidth = array.getDimensionPixelSize(0, -1);
            array.recycle();
        }

        layoutManager = new GridLayoutManager(context, 1);
        setLayoutManager(layoutManager);
    }
}
