package com.fussyware.AndDiscovered;

import android.content.Context;

import com.fussyware.AndDiscovered.edsc.EDSCStarCoordinator;
import com.fussyware.AndDiscovered.edsm.EDSMStarCoordinator;

/**
 * Created by wboyd on 7/6/15.
 */
public class StarCoordinatorFactory
{
    public enum StarCoordinatorType {
        EDSC,
        EDSM
    }

    public static StarCoordinator getInstance(Context context, StarCoordinatorType type)
    {
        switch (type) {
            case EDSC:
                return EDSCStarCoordinator.getInstance(context);
            case EDSM:
                return EDSMStarCoordinator.getInstance(context);
            default:
                throw new IllegalArgumentException("Invalid Star Coordinator service type.");
        }
    }
}
