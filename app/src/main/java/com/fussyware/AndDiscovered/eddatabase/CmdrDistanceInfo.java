package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.fussyware.AndDiscovered.celestial.DistanceInfo;

import java.util.Date;

/**
 * Created by wboyd on 7/7/15.
 */
public class CmdrDistanceInfo extends DistanceInfo
{
    private SQLiteDatabase db;
    private long id;
    private boolean remoteUpdated;
    private Date createdDate;
    private boolean referenceSystem;

    CmdrDistanceInfo(SQLiteDatabase db, long id, String first, String second, double distance, Date createdDate, boolean remoteUpdated, boolean referenceSystem)
    {
        super(first, second, distance);

        this.db = db;
        this.id = id;
        this.remoteUpdated = remoteUpdated;
        this.createdDate = createdDate;
        this.referenceSystem = referenceSystem;
    }

    public long getRowId()
    {
        return id;
    }

    public boolean updateSecondSystem(String system)
    {
        synchronized (mutex) {
            if (getSecondSystem() != null) return false;

            setSecondSystem(system);
        }

        String selection = CmdrDbContract.CmdrDistances._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_TO, system);

        db.update(CmdrDbContract.CmdrDistances.TABLE_NAME,
                  values,
                  selection,
                  selectionArgs);

        return true;
    }

    @Override
    public void setDistance(double distance)
    {
        distance = (distance < 0) ? -distance : distance;

        if (this.getDistance() != distance) {
            super.setDistance(distance);

            String table;
            String selection;
            String[] selectionArgs = new String[] { Long.toString(id) };
            ContentValues values = new ContentValues();

            if (referenceSystem) {
                selection = CmdrDbContract.CmdrReferenceDistances._ID + "=?";
                values.put(CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_DISTANCE, distance);
                table = CmdrDbContract.CmdrReferenceDistances.TABLE_NAME;
            } else {
                selection = CmdrDbContract.CmdrDistances._ID + "=?";
                values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE, distance);
                table = CmdrDbContract.CmdrDistances.TABLE_NAME;
            }

            db.update(table, values, selection, selectionArgs);
        }
    }

    public void setRemoteUpdated(boolean remoteUpdated)
    {
        synchronized (mutex) {
            if (this.remoteUpdated == remoteUpdated) return;

            this.remoteUpdated = remoteUpdated;
        }

        String selection = CmdrDbContract.CmdrDistances._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED, remoteUpdated);

        db.update(CmdrDbContract.CmdrDistances.TABLE_NAME,
                  values,
                  selection,
                  selectionArgs);
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public boolean isRemoteUpdated()
    {
        synchronized (mutex) {
            return remoteUpdated;
        }
    }

    @Override
    public String toString() {
        return "CmdrDistanceInfo{" +
                "id=" + id +
                ", remoteUpdated=" + remoteUpdated +
                ", createdDate=" + createdDate +
                ", referenceSystem=" + referenceSystem +
                "} " + super.toString();
    }
}
