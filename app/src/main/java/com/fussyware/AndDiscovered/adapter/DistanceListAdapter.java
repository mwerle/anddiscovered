package com.fussyware.AndDiscovered.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.StarCoordinator;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by wes on 6/20/15.
 */
public class DistanceListAdapter extends BaseCursorAdapter
{
    public static final String NEXT_SYSTEM_IN_ROUTE = "Next system in route";

    private static final String LOG_NAME = DistanceListAdapter.class.getSimpleName();

    private String systemName = "NULL_NAME";
    private final StarCoordinator starCoordinator;
    private final ArrayList<SystemDistanceInfo> distanceList = new ArrayList<SystemDistanceInfo>();

    private final int pressedColor;
    private final int referenceColor;

    public DistanceListAdapter(Context context, StarCoordinator starCoordinator)
    {
        this.starCoordinator = starCoordinator;
        this.pressedColor = context.getResources().getColor(R.color.ed_light_orange);
        this.referenceColor = context.getResources().getColor(android.R.color.holo_red_light);
    }

    public void onSaveInstanceState(Bundle outState)
    {
        try {
            lock.readLock().lock();
            outState.putString("DistanceListAdapter.system", systemName);
            outState.putParcelableArrayList("DistanceListAdapter.infoList", distanceList);
        } finally {
            lock.readLock().unlock();
        }
    }

    public void onViewStateRestored(Bundle savedInstanceState)
    {
        try {
            lock.writeLock().lock();

            distanceList.clear();
            systemName = savedInstanceState.getString("DistanceListAdapter.system");

            ArrayList<SystemDistanceInfo> list =savedInstanceState.getParcelableArrayList("DistanceListAdapter.infoList");

            if (list != null) {
                distanceList.addAll(list);
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void setSystemName(String name)
    {
        try {
            lock.writeLock().lock();
            if (!systemName.equals(name)) {
                systemName = name;

                if ((systemName == null) || systemName.isEmpty()) {
                    systemName = "NULL_NAME";
                }

                new SphereAsyncTask().execute(systemName);
//                new UnionDbAsyncTask().execute(systemName);
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public int getCount()
    {
        try {
            lock.readLock().lock();
            return distanceList.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public SystemDistanceInfo getItem(int position)
    {
        try {
            lock.readLock().lock();
            return distanceList.get(position);
        } catch (Exception e) {
            Log.e(LOG_NAME, "Failed to get the item at position: " + Integer.toString(position), e);
            return null;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public void notifyDatabaseChanged()
    {
        String system;

        try {
            lock.readLock().lock();
            system = systemName;
        } finally {
            lock.readLock().unlock();
        }

        new SphereAsyncTask().execute(system);
//        new UnionDbAsyncTask().execute(system);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.distance_info_listview, parent, false);

            TextView systemView = (TextView) convertView.findViewById(R.id.SystemText);
            TextView distance = (TextView) convertView.findViewById(R.id.DistanceText);

            viewHolder = new ViewHolder(systemView,
                                        distance,
                                        systemView.getTextColors()); //distanceListeners);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SystemDistanceInfo pair = getItem(position);

        viewHolder.systemView.setText(pair.toSystem);

        if (pair.toSystem.equals(NEXT_SYSTEM_IN_ROUTE)) {
            viewHolder.systemView.setTextColor(pressedColor);
            viewHolder.distanceView.setTextColor(pressedColor);
        } else if (pair.isReferenceSystem) {
            viewHolder.systemView.setTextColor(referenceColor);
            viewHolder.distanceView.setTextColor(referenceColor);
        } else {
            viewHolder.systemView.setTextColor(viewHolder.defaultColors);
            viewHolder.distanceView.setTextColor(viewHolder.defaultColors);
        }

        viewHolder.distanceView.setText(pair.formatDistance());

        return convertView;
    }

    private class ViewHolder
    {
        final TextView systemView;
        final TextView distanceView;
        final ColorStateList defaultColors;

        ViewHolder(TextView systemView,
                   TextView distanceView,
                   ColorStateList defaultColors)
        {
            this.systemView = systemView;
            this.distanceView = distanceView;
            this.defaultColors = defaultColors;
        }
    }

    public static class SystemDistanceInfo implements Parcelable
    {
        public final String fromSystem;
        public final String toSystem;
        public final Double distance;
        public final boolean isReferenceSystem;

        private static final DecimalFormat distanceFormatter = new DecimalFormat("0.00");

        static {
            distanceFormatter.setRoundingMode(RoundingMode.HALF_UP);
        }

        SystemDistanceInfo(String fromSystem,
                           String toSystem,
                           Double distance,
                           boolean isReferenceSystem)
        {
            this.fromSystem = fromSystem;
            this.toSystem = toSystem;
            this.distance = distance;
            this.isReferenceSystem = isReferenceSystem;
        }

        private SystemDistanceInfo(Parcel parcel)
        {
            fromSystem = parcel.readString();
            toSystem = parcel.readString();
            distance = parcel.readDouble();
            isReferenceSystem = (parcel.readInt() == 1);
        }

        public String formatDistance()
        {
            return distanceFormatter.format(distance);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            SystemDistanceInfo infoPair = (SystemDistanceInfo) o;

            return toSystem.equalsIgnoreCase(infoPair.toSystem);

        }

        @Override
        public int hashCode()
        {
            int result = toSystem.hashCode();
            result = 31 * result + distance.hashCode();
            return result;
        }

        @Override
        public String toString()
        {
            return "SystemDistanceInfo {" +
                   "systemName='" + toSystem + '\'' +
                   ", distance=" + distance +
                   ", isReferenceSystem=" + isReferenceSystem +
                   '}';
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeString(fromSystem);
            dest.writeString(toSystem);
            dest.writeDouble(distance);
            dest.writeInt(isReferenceSystem ? 1 : 0);
        }

        public static final Parcelable.Creator<SystemDistanceInfo> CREATOR = new Parcelable.Creator<SystemDistanceInfo>() {

            @Override
            public SystemDistanceInfo createFromParcel(Parcel source)
            {
                return new SystemDistanceInfo(source);
            }

            @Override
            public SystemDistanceInfo[] newArray(int size)
            {
                return new SystemDistanceInfo[size];
            }
        };
    }

    private class UnionDbAsyncTask extends AsyncTask<String, Void, ArrayList<SystemDistanceInfo>>
    {
        @Override
        protected void onPostExecute(ArrayList<SystemDistanceInfo> pairs)
        {
            try {
                lock.writeLock().lock();
                distanceList.clear();
                distanceList.addAll(pairs);
                notifyDataSetChanged();
            } finally {
                lock.writeLock().unlock();
            }
        }

        @Override
        protected ArrayList<SystemDistanceInfo> doInBackground(String... params)
        {
            ArrayList<SystemDistanceInfo> list = new ArrayList<SystemDistanceInfo>();
            String system = params[0];

            Date startt0 = new Date();
            if (!system.equals("NULL_NAME")) {
                Date t0 = new Date();
                List<? extends DistanceInfo> ldi = starCoordinator.getDistances(system, false);
                SystemDistanceInfo unknownSystem = null;

                for (DistanceInfo di : ldi) {
                    if ((di.getSecondSystem() != null) && (!di.getSecondSystem().isEmpty()) && (di.getDistance() != 0.0)) {
                        list.add(new SystemDistanceInfo(system, di.getSecondSystem(), di.getDistance(), false));
                    }
                }
                Log.d(LOG_NAME, "1: " + (new Date().getTime() - t0.getTime()));
                t0 = new Date();

                ldi = dbHelper.getDistances(system, false);
                for (DistanceInfo di : ldi) {
                    SystemDistanceInfo pair = new SystemDistanceInfo(system, di.getSecondSystem(), di.getDistance(), false);

                    if ((pair.toSystem == null) || pair.toSystem.isEmpty()) {
                        unknownSystem = new SystemDistanceInfo(system, NEXT_SYSTEM_IN_ROUTE, di.getDistance(), false);
                    } else if (!list.contains(pair)) {
                        list.add(pair);
                    }
                }
                Log.d(LOG_NAME, "1: " + (new Date().getTime() - t0.getTime()));
                t0 = new Date();

                ldi = dbHelper.getReferenceDistances(system);
                for (DistanceInfo di : ldi) {
                    SystemDistanceInfo pair = new SystemDistanceInfo(system, di.getSecondSystem(), di.getDistance(), false);

                    if (!list.contains(pair)) {
                        list.add(pair);
                    }
                }
                Log.d(LOG_NAME, "1: " + (new Date().getTime() - t0.getTime()));
                t0 = new Date();

                Collections.sort(list, new Comparator<SystemDistanceInfo>()
                {
                    @Override
                    public int compare(SystemDistanceInfo lhs, SystemDistanceInfo rhs)
                    {
                        return lhs.distance.compareTo(rhs.distance);
                    }
                });
                Log.d(LOG_NAME, "1: " + (new Date().getTime() - t0.getTime()));

                if (unknownSystem != null) {
                    list.add(0, unknownSystem);
                }

                t0 = new Date();
                CmdrSystemInfo info = dbHelper.getSystem(system);
                if (info.getPosition() == null) {
                    List<CmdrSystemInfo> infoList = dbHelper.getSystemsWithPosition();

                    for (int i = 0, added = 0; (i < infoList.size()) && (added < 5); i++) {
                        SystemDistanceInfo pair = new SystemDistanceInfo(system, infoList.get(i).getSystem(), 0.0, true);

                        if (!list.contains(pair)) {
                            list.add(pair);
                            added++;
                        }
                    }
                }
                Log.d(LOG_NAME, "1: " + (new Date().getTime() - t0.getTime()));
            }

            Log.d(LOG_NAME, "Overall Time: " + (new Date().getTime() - startt0.getTime()));
            return list;
        }
    }

    private class SphereAsyncTask extends AsyncTask<String, Void, ArrayList<SystemDistanceInfo>>
    {
        @Override
        protected void onPostExecute(ArrayList<SystemDistanceInfo> pairs)
        {
            try {
                lock.writeLock().lock();
                distanceList.clear();
                distanceList.addAll(pairs);
                notifyDataSetChanged();
            } finally {
                lock.writeLock().unlock();
            }
        }

        @Override
        protected ArrayList<SystemDistanceInfo> doInBackground(String... params)
        {
            ArrayList<SystemDistanceInfo> list = new ArrayList<SystemDistanceInfo>();
            String system = params[0];

            if (!system.equals("NULL_NAME")) {
                List<? extends DistanceInfo> ldi = starCoordinator.getDistances(system, false);
                SystemDistanceInfo unknownSystem = null;

                for (DistanceInfo di : ldi) {
                    if ((di.getSecondSystem() != null) && (!di.getSecondSystem().isEmpty()) && (di.getDistance() != 0.0)) {
                        list.add(new SystemDistanceInfo(system, di.getSecondSystem(), di.getDistance(), false));
                    }
                }

                ldi = dbHelper.getDistances(system, false);
                for (DistanceInfo di : ldi) {
                    SystemDistanceInfo pair = new SystemDistanceInfo(system, di.getSecondSystem(), di.getDistance(), false);

                    if ((pair.toSystem == null) || pair.toSystem.isEmpty()) {
                        unknownSystem = new SystemDistanceInfo(system, NEXT_SYSTEM_IN_ROUTE, di.getDistance(), false);
                    } else if (!list.contains(pair)) {
                        list.add(pair);
                    }
                }

                ldi = dbHelper.getReferenceDistances(system);
                for (DistanceInfo di : ldi) {
                    SystemDistanceInfo pair = new SystemDistanceInfo(system, di.getSecondSystem(), di.getDistance(), false);

                    if (!list.contains(pair)) {
                        list.add(pair);
                    }
                }

                Collections.sort(list, new Comparator<SystemDistanceInfo>()
                {
                    @Override
                    public int compare(SystemDistanceInfo lhs, SystemDistanceInfo rhs)
                    {
                        return lhs.distance.compareTo(rhs.distance);
                    }
                });

                if (unknownSystem != null) {
                    list.add(0, unknownSystem);
                }

                CmdrSystemInfo info = dbHelper.getSystem(system);
                if (info.getPosition() == null) {
                    List<CmdrSystemInfo> lastSystems = dbHelper.getSystemsWithPosition(10);
                    int sortBy = StarCoordinator.SORT_BY_ANY;

                    Position origin = new Position(0, 0, 0);
                    if (!lastSystems.isEmpty()) {
                        origin = lastSystems.get(0).getPosition();
                        Position p2 = lastSystems.get(lastSystems.size() - 1).getPosition();

                        double x, y, z;

                        x = p2.x - origin.x;
                        y = p2.y - origin.y;
                        z = p2.z - origin.z;

                        x = (x < 0) ? -x : x;
                        y = (y < 0) ? -y : y;
                        z = (z < 0) ? -z : z;

                        Position vector = new Position(x, y, z);
                        Log.d(LOG_NAME, "p1: " + origin + ", p2: " + p2 + ", vector: " + vector);

                        double min = (x <= y) ? x : y;
                        min = (min <= z) ? min : z;

                        double max = (x >= y) ? x : y;
                        max = (max >= z) ? max : z;

                        Log.d(LOG_NAME, "min: " + min + ", max: " + max);

//                        if (min == vector.x) {
//                            sortBy = StarCoordinator.SORT_BY_X;
//                        } else if (min == vector.y) {
//                            sortBy = StarCoordinator.SORT_BY_Y;
//                        } else {
//                            sortBy = StarCoordinator.SORT_BY_Z;
//                        }

                        if (max == vector.x) {
                            sortBy = StarCoordinator.SORT_BY_X;
                        } else if (max == vector.y) {
                            sortBy = StarCoordinator.SORT_BY_Y;
                        } else {
                            sortBy = StarCoordinator.SORT_BY_Z;
                        }

                        Log.d(LOG_NAME, "Sort by: " + sortBy);
                    }

                    int added = 0;
                    List<? extends SystemInfo> sphereList = starCoordinator.getSystemSphere(origin,
                                                                                            2000.0,
                                                                                            sortBy);

                    for (int i = 0; ((i < sphereList.size()) && (added < 5)); i++) {
                        SystemDistanceInfo pair = new SystemDistanceInfo(system,
                                                                         sphereList.get(i)
                                                                                   .getSystem(),
                                                                         0.0,
                                                                         true);

                        if (!list.contains(pair)) {
                            list.add(pair);
                            added++;
                        }
                    }

                    if (added < 5) {
                        List<CmdrSystemInfo> infoList = dbHelper.getSystemsWithPosition();

                        for (int i = 0; (i < infoList.size()) && (added < 5); i++) {
                            SystemDistanceInfo pair = new SystemDistanceInfo(system,
                                                                             infoList.get(i)
                                                                                     .getSystem(),
                                                                             0.0,
                                                                             true);

                            if (!list.contains(pair)) {
                                list.add(pair);
                                added++;
                            }
                        }
                    }
                }
            }

            return list;
        }
    }
}
