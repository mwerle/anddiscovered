package com.fussyware.AndDiscovered;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrDistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.SystemImageInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.eddiscovery.AnnounceMessage;
import com.fussyware.AndDiscovered.eddiscovery.DiscoveryListener;
import com.fussyware.AndDiscovered.eddiscovery.DiscoveryService;
import com.fussyware.AndDiscovered.eddiscovery.QueryMessage;
import com.fussyware.AndDiscovered.edproxy.ImageMessage;
import com.fussyware.AndDiscovered.edproxy.InitMessage;
import com.fussyware.AndDiscovered.edproxy.ProxyClient;
import com.fussyware.AndDiscovered.edproxy.ProxyListener;
import com.fussyware.AndDiscovered.edproxy.ProxyMessage;
import com.fussyware.AndDiscovered.edproxy.ProxyMessageType;
import com.fussyware.AndDiscovered.edproxy.SystemMessage;
import com.fussyware.AndDiscovered.preference.PreferenceTag;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/** Background service that maintains a connection with the EDProxy server in order to receive new netLog changes.
 * The service will provide as a means for discovery of, and connection to, the EDProxy server through
 * the ED Discovery protocol.
 *
 * Once a connection is acquired the service will track the last System line that it receives so that on
 * future updates the system only asks for what it needs.
 *
 * @author Weston Boyd
 */
public class EDProxyIntentService extends IntentService
{
    public static final String BROADCAST_DISCOVERY_STARTED = "EDProxyIntentService.DiscoveryStarted";
    public static final String BROADCAST_DISCOVERY_SUCCESS = "EDProxyIntentService.DiscoverySuccess";
    public static final String BROADCAST_DISCOVERY_FAILED = "EDProxyIntentService.DiscoveryFailed";
    public static final String BROADCAST_CONNECTED = "EDProxyIntentService.Connected";
    public static final String BROADCAST_DISCONNECTED = "EDProxyIntentService.Disconnected";

    public static final String BROADCAST_SYSTEM_UPDATED = "EDProxyIntentService.SystemUpdated";
    public static final String BROADCAST_IMAGE_EVENT = "EDProxyIntentService.ImageEvent";

    public static final String EXTRA_HOSTNAME = "Hostname";
    public static final String EXTRA_PORT = "Port";
    public static final String EXTRA_IMAGE_URL = "ImageUrl";
    public static final String EXTRA_IMAGE_INFO = "image_info";
    public static final String EXTRA_IMAGE_PATH = "ImagePath";

    private static final String LOG_NAME = EDProxyIntentService.class.getSimpleName();
    private static final int DEFAULT_EDPROXY_PORT = 45550;
    private static final int HEARTBEAT_TIMEOUT = 30;

    private static final int[] backoff = { 0, 0, 2000, 5000, 10000, 30000, 60000, 120000 };

    private DiscoveryService edDiscovery;

    private final AtomicBoolean running = new AtomicBoolean(false);

    private final Object discoveryCond = new Object();
    private final Object queueCond = new Object();

    private EDProxyAddrInfo addrInfo;

    private ProxyClient client;
    private CmdrDbHelper dbHelper;
    private final ConcurrentLinkedQueue<ProxyMessage> queue = new ConcurrentLinkedQueue<ProxyMessage>();
    private final Timer timer = new Timer("ProxyTimer");
    private TimerTask timerTask;
    private SharedPreferences preferences;
    private LocalBroadcastManager broadcastManager;
    private final EDProxyBinder binder = new EDProxyBinder();
    private String lastStateBroadcast = BROADCAST_DISCONNECTED;

    private ThreadPoolExecutor executor;

    private StarCoordinator starCoordinator;

    public EDProxyIntentService()
    {
        this("EDProxyIntentService");
    }

    public EDProxyIntentService(String name)
    {
        super(name);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());

        executor = new ThreadPoolExecutor(40, 40, 15, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>());
        executor.allowCoreThreadTimeOut(true);

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        CmdrDbHelper.init(getApplicationContext());
        dbHelper = CmdrDbHelper.getInstance();

        starCoordinator = StarCoordinatorFactory.getInstance(getApplicationContext(),
                                                             StarCoordinatorFactory.StarCoordinatorType.EDSM);

        int ttl;

        try {
            ttl = Integer.valueOf(preferences.getString(PreferenceTag.DISCOVERY_TTL, "1"));
        } catch (Exception e) {
            String __tmp = preferences.getString(PreferenceTag.DISCOVERY_TTL, "1");
            Log.e(LOG_NAME, "Failed to convert TTL from preferences. [" + __tmp + "]", e);
            ttl = 1;
        }

        edDiscovery = new DiscoveryService("239.45.99.98", 45551, ttl);
        edDiscovery.addListener(new DiscoveryListener()
        {
            @Override
            public void discoveryAnnounce(AnnounceMessage message)
            {
                Log.d(LOG_NAME, "Discovery got message: " + message);
                if (message.getServiceName().equals("edproxy")) {
                    synchronized (discoveryCond) {
                        addrInfo = new EDProxyAddrInfo(message.getHostname(),
                                                       message.getPort());
                        discoveryCond.notify();
                    }
                }
            }

            @Override
            public void discoveryQuery(QueryMessage message)
            {

            }
        });
    }

    @Override
    public void onDestroy()
    {
        running.set(false);

        synchronized (discoveryCond) {
            discoveryCond.notifyAll();
        }

        synchronized (queueCond) {
            queueCond.notifyAll();
        }

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return binder;
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        running.set(true);

        int backoff_pos = 0;

        while (running.get()) {
            if (!isNetworkAvailable()) {
                synchronized (discoveryCond) {
                    try { discoveryCond.wait(10000); }
                    catch (InterruptedException ignored) {}
                }

                continue;
            }

            if (preferences.getBoolean(PreferenceTag.DISCOVERY_ENABLED, true)) {
                discover();
            } else if (!preferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
                addrInfo = new EDProxyAddrInfo(preferences.getString(PreferenceTag.IPADDR, ""),
                                               DEFAULT_EDPROXY_PORT);
            } else {
                // Bail out. We will be started later once someone puts in an IP.
                return;
            }

            if (running.get() && (addrInfo != null)) {
                try {
                    try {
                        Log.d(LOG_NAME, "backoff pos: " + backoff_pos);
                        Thread.sleep(backoff[backoff_pos]);
                    } catch (InterruptedException ignored) {
                    }

                    client = new ProxyClient(addrInfo.hostname, addrInfo.port);
                    client.addListener(new ProxyListener()
                    {
                        @Override
                        public void messageReceived(ProxyMessage message)
                        {
                            synchronized (queueCond) {
                                queue.add(message);
                                queueCond.notify();
                            }
                        }
                    });

                    Log.d(LOG_NAME, "reset backoff pos");
                    backoff_pos = 0;

                    sendProxyInit(client);
                    sendHostIntent(BROADCAST_CONNECTED, addrInfo.hostname, addrInfo.port);

                    while (running.get() && client.isRunning()) {
                        synchronized (queueCond) {
                            while (running.get() &&
                                   client.isRunning() &&
                                   queue.isEmpty()) {
                                try { queueCond.wait(1000); }
                                catch (InterruptedException ignored) {}
                            }

                            if (!running.get() || !client.isRunning()) {
                                continue;
                            }
                        }

                        ArrayList<Future<CmdrSystemInfo>> systemFutureList = new ArrayList<Future<CmdrSystemInfo>>();
                        ArrayList<Future<CmdrDistanceInfo>> distanceFutureList = new ArrayList<Future<CmdrDistanceInfo>>();

                        SystemMessage lastSystemMessage = null;
                        SQLiteDatabase db = dbHelper.getWritableDatabase();

                        try {
                            while (running.get() && client.isRunning() && !queue.isEmpty()) {
                                ProxyMessage message = queue.poll();

                                if (null != message) {
                                    switch (message.getType()) {
                                        case Init:
                                            break;
                                        case System: {
                                            if (!db.inTransaction()) {
                                                db.beginTransaction();
                                            }

                                            SystemMessage msg = handleSystemMessage((SystemMessage) message,
                                                                                    systemFutureList,
                                                                                    distanceFutureList);

                                            if (msg != null) {
                                                lastSystemMessage = msg;
                                            }

                                            break;
                                        }
                                        case Image:
                                            handleImageMessage((ImageMessage) message);
                                            break;
                                        case Import:
                                            break;
                                    }
                                }
                            }

                            if (db.inTransaction()) {
                                db.setTransactionSuccessful();
                            }
                        } finally {
                            if (db.inTransaction()) {
                                db.endTransaction();
                            }

                            for (Future<CmdrSystemInfo> future : systemFutureList) {
                                try {
                                    future.get();
                                } catch (InterruptedException | ExecutionException e) {
                                    Log.e(LOG_NAME, "Error while getting system future. ", e);
                                }
                            }

                            for (Future<CmdrDistanceInfo> future : distanceFutureList) {
                                try {
                                    future.get();
                                } catch (InterruptedException | ExecutionException e) {
                                    Log.e(LOG_NAME, "Error while getting distance future. ", e);
                                }
                            }

                            if (!systemFutureList.isEmpty()) {
                                try {
                                    CmdrSystemInfo info = systemFutureList.get(systemFutureList.size() - 1)
                                                                          .get();

                                    sendSystemUpdated(info);
                                } catch (Exception e) {
                                    Log.e(LOG_NAME, "Failed getting the last system entry.");
                                }
                            }

                            systemFutureList.clear();
                            distanceFutureList.clear();
                        }
                    }
                } catch (IOException e) {
                    Log.e(LOG_NAME, "ProxyClient failed to find the host.", e);

                    backoff_pos++;
                    if (backoff_pos == backoff.length) {
                        backoff_pos = backoff.length - 1;
                    }
                } finally {
                    Log.d(LOG_NAME, "Closing down connection");
                    if (client != null) {
                        client.close();
                    }

                    Log.d(LOG_NAME, "Event out disconnect.");
                    addrInfo = null;
                    sendClientStateChanged(BROADCAST_DISCONNECTED);
                }
            } else {
                sendDiscoveryFailedIntent();
            }
        }
    }

    public void resendState()
    {
        Intent intent = new Intent(lastStateBroadcast);

        if (lastStateBroadcast.equals(BROADCAST_DISCOVERY_SUCCESS) ||
            lastStateBroadcast.equals(BROADCAST_CONNECTED)) {
            intent.putExtra(EXTRA_HOSTNAME, addrInfo.hostname);
            intent.putExtra(EXTRA_PORT, addrInfo.port);
        }

        broadcastManager.sendBroadcast(intent);
    }

    private SystemMessage handleSystemMessage(SystemMessage message,
                                              ArrayList<Future<CmdrSystemInfo>> systemFutureList,
                                              ArrayList<Future<CmdrDistanceInfo>> distanceFutureList)
    {
        SystemMessage ret = null;

        if (!dbHelper.containsSystem(message.getSystem())) {
            systemFutureList.add(executor.submit(new SystemAsycCallable(dbHelper.createSystem(
                    message.getSystem(),
                    null))));
        }

        CmdrDistanceInfo distanceInfo = dbHelper.getLastDistance();
        if (distanceInfo == null) {
            dbHelper.createDistance(message.getSystem(), message.getDate());
            ret = message;
        } else if (!distanceInfo.getFirstSystem().equalsIgnoreCase(message.getSystem()) && (distanceInfo.getSecondSystem() == null)) {
            if (distanceInfo.getDistance() == 0.0) {
                distanceFutureList.add(executor.submit(new DistanceAsyncCallable(
                        distanceInfo,
                        message.getSystem())));
            }

            distanceInfo.updateSecondSystem(message.getSystem());
            dbHelper.createDistance(message.getSystem(), message.getDate());
            ret = message;
        }

        dbHelper.setLastProxyMessage(new Date(message.getDate().getTime() + 1000));

        return ret;
    }

    private void handleImageMessage(ImageMessage message)
    {
        Log.d(LOG_NAME, "Handle image message: " + message);
        if (preferences.getBoolean(PreferenceTag.ALLOW_IMAGES, false)) {
            Intent intent = new Intent(BROADCAST_IMAGE_EVENT);

            try {
                String path = message.getImageUrl().getPath();
                path = URLDecoder.decode(path, "UTF-8");

                String filename = new File(path).getName();
                filename = filename.substring(0, "yyyy-mm-dd_hh-mm-ss".length());

                Log.d(LOG_NAME, "system name: " + filename);

                CmdrSystemInfo info = dbHelper.getCurrentSystem();
                SystemImageInfo imageInfo = info.createImageInfo(path);
                intent.putExtra("image_info", imageInfo);
            } catch (UnsupportedEncodingException e) {
                Log.e(LOG_NAME, "Failed decoding the URL string name.", e);
            }

            intent.putExtra(EXTRA_IMAGE_URL, message.getImageUrl().toString());

            broadcastManager.sendBroadcast(intent);
        }
        Log.d(LOG_NAME, "Done with image.");
    }

    private boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return (activeNetworkInfo != null &&
                activeNetworkInfo.isConnected() &&
                ((activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) ||
                 (activeNetworkInfo.getType() == ConnectivityManager.TYPE_ETHERNET)));
    }

    private void discover()
    {
        boolean found = false;
        addrInfo = null;

        while (running.get() && isNetworkAvailable() && !found) {
            if (addrInfo == null) {
                synchronized (discoveryCond) {
                    int tryCount = 0;

                    WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                    WifiManager.MulticastLock multicastLock = wifi.createMulticastLock("EDProxyDiscovery");

                    sendDiscoveryStartedIntent();
                    multicastLock.acquire();

                    try {
                        edDiscovery.start();

                        while (running.get() &&
                               edDiscovery.isRunning() &&
                               (addrInfo == null) &&
                               (tryCount++ < 30)) {
                            try {
                                edDiscovery.send(new QueryMessage("edproxy"));
                                discoveryCond.wait(1000);
                            } catch (InterruptedException ignored) {
                            } catch (IOException ignored) {
                            }
                        }
                    } catch (IOException ignored) {
                        addrInfo = null;
                    }

                    edDiscovery.stop();
                    multicastLock.release();
                }
            }

            if ((addrInfo == null) ||
                (addrInfo.hostname == null) ||
                (addrInfo.hostname.isEmpty()) ||
                (addrInfo.port == -1)) {
                addrInfo = null;
                sendDiscoveryFailedIntent();

                if (preferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
                    synchronized (discoveryCond) {
                        try { discoveryCond.wait(10000); }
                        catch (InterruptedException ignored) {}
                    }
                } else {
                    found = true;
                    addrInfo = new EDProxyAddrInfo(preferences.getString(PreferenceTag.IPADDR,
                                                                         ""),
                                                   DEFAULT_EDPROXY_PORT);
                }
            } else {
                found = true;
                sendHostIntent(BROADCAST_DISCOVERY_SUCCESS, addrInfo.hostname, addrInfo.port);
            }
        }
    }

    private void sendDiscoveryStartedIntent()
    {
        broadcastManager.sendBroadcast(new Intent(BROADCAST_DISCOVERY_STARTED));
        lastStateBroadcast = BROADCAST_DISCOVERY_STARTED;
    }

    private void sendHostIntent(String action, String hostname, int port)
    {
        Intent intent = new Intent(action);
        intent.putExtra(EXTRA_HOSTNAME, hostname);
        intent.putExtra(EXTRA_PORT, port);

        broadcastManager.sendBroadcast(intent);

        lastStateBroadcast = action;
    }

    private void sendDiscoveryFailedIntent()
    {
        broadcastManager.sendBroadcast(new Intent(BROADCAST_DISCOVERY_FAILED));
        lastStateBroadcast = BROADCAST_DISCOVERY_FAILED;
    }

    private void sendClientStateChanged(String action)
    {
        broadcastManager.sendBroadcast(new Intent(action));
        lastStateBroadcast = action;
    }

    private void sendSystemUpdated(CmdrSystemInfo system)
    {
        Intent intent = new Intent(BROADCAST_SYSTEM_UPDATED);
        intent.putExtra("system_name", system.getSystem());
        intent.putExtra("system", system);

        broadcastManager.sendBroadcast(intent);
    }

    private void sendSystemUpdated(String system)
    {
        Intent intent = new Intent(BROADCAST_SYSTEM_UPDATED);
        intent.putExtra("system_name", system);

        broadcastManager.sendBroadcast(intent);
    }

    private void sendProxyInit(ProxyClient client)
    {
        ArrayList<ProxyMessageType> list = new ArrayList<ProxyMessageType>();
        list.add(ProxyMessageType.System);
        list.add(ProxyMessageType.Image);

        try {
            client.send(new InitMessage(new Date(), HEARTBEAT_TIMEOUT, dbHelper.getLastProxyMessage(), list));
            client.startHeartbeat(HEARTBEAT_TIMEOUT - 5, TimeUnit.SECONDS);
        } catch (IOException e) {
            Log.e(LOG_NAME, "Failed to send Init message to the proxy server.", e);
        }
    }

    public class EDProxyBinder extends Binder
    {
        public EDProxyIntentService getService()
        {
            return EDProxyIntentService.this;
        }
    }

    private static class EDProxyAddrInfo
    {
        public final String hostname;
        public final int port;

        EDProxyAddrInfo(String hostname, int port)
        {
            this.hostname = hostname;
            this.port = port;
        }

        @Override
        public String toString()
        {
            return "EDProxyAddrInfo{" +
                   "hostname='" + hostname + '\'' +
                   ", port=" + port +
                   '}';
        }
    }

    private class SystemAsycCallable implements Callable<CmdrSystemInfo>
    {
        private final CmdrSystemInfo info;

        SystemAsycCallable(CmdrSystemInfo info)
        {
            this.info = info;
        }

        @Override
        public CmdrSystemInfo call() throws Exception
        {
            SystemInfo systemInfo = starCoordinator.getSystem(info.getSystem());

            if ((systemInfo != null) && (systemInfo.getPosition() != null)) {
                info.setPosition(systemInfo.getPosition());
            }

            return info;
        }
    }

    private class DistanceAsyncCallable implements Callable<CmdrDistanceInfo>
    {
        private final CmdrDistanceInfo info;
        private final String system;

        DistanceAsyncCallable(CmdrDistanceInfo info, String system)
        {
            this.info = info;
            this.system = system;
        }

        @Override
        public CmdrDistanceInfo call() throws Exception
        {
            DistanceInfo di = starCoordinator.getDistance(info.getFirstSystem(), system);
            double dist = (di == null) ? 0.0 : di.getDistance();

            if (dist != 0.0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                try {
                    db.beginTransaction();
                    info.setDistance(dist);
                    info.setRemoteUpdated(true);
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            }

            return info;
        }
    }
}
