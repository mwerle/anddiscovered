package com.fussyware.AndDiscovered.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.dialog.CelestialStarEditDialogFragment;

/**
 * Created by wes on 11/17/15.
 */
public class CelestialStarDetailsFragment extends Fragment
{
    private static final String LOG_NAME = CelestialStarDetailsFragment.class.getSimpleName();

    private final Object mutex = new Object();

    private StarBody celestialBody;

    private View mainView;

    private TextView ageText;
    private TextView massText;
    private TextView radiusText;
    private TextView surfaceTempText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.cb_star_details_layout,
                                       container,
                                       false);

        createViews(mainView);

        if (savedInstanceState == null) {
            mainView.setVisibility(View.GONE);
        } else {
            StarBody body = savedInstanceState.getParcelable("celestial");
            setCelestialBody(body);
        }

        return mainView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial", celestialBody);
    }

    public void setCelestialBody(StarBody body)
    {
        synchronized (mutex) {
            if (body != celestialBody) {
                celestialBody = body;

                ageText.setText((body.getAge() == null) ?
                                "" :
                                String.format("%1.2f", body.getAge()));
                massText.setText((body.getAge() == null) ?
                                 "" :
                                 String.format("%1.4f", body.getMass()));
                radiusText.setText((body.getAge() == null) ?
                                   "" :
                                   String.format("%1.4f", body.getRadius()));
                surfaceTempText.setText((body.getAge() == null) ?
                                        "" :
                                        String.format("%1.2f", body.getSurfaceTemp()));

                mainView.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onDone(Bundle bundle)
    {
        for (String key : bundle.keySet()) {
            double dbl = bundle.getDouble(key);

            switch (key) {
                case CelestialStarEditDialogFragment.TAG_AGE:
                    celestialBody.setAge(dbl);
                    ageText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialStarEditDialogFragment.TAG_MASS:
                    celestialBody.setMass(dbl);
                    massText.setText(String.format("%1.4f", dbl));
                    break;
                case CelestialStarEditDialogFragment.TAG_RADIUS:
                    celestialBody.setRadius(dbl);
                    radiusText.setText(String.format("%1.4f", dbl));
                    break;
                case CelestialStarEditDialogFragment.TAG_SURFACETEMP:
                    celestialBody.setSurfaceTemp(dbl);
                    surfaceTempText.setText(String.format("%1.2f", dbl));
                    break;
                default:
                    break;
            }
        }
    }

    private void createViews(View layout)
    {
        mainView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CelestialStarEditDialogFragment df = CelestialStarEditDialogFragment.newInstance(celestialBody);
                df.show(getFragmentManager(), FragmentTag.celestial_info_fragment.celestial_star_dialog);
            }
        });

        ageText = (TextView) layout.findViewById(R.id.cb_star_age_value);
        massText = (TextView) layout.findViewById(R.id.cb_star_mass_value);
        radiusText = (TextView) layout.findViewById(R.id.cb_star_radius_value);
        surfaceTempText = (TextView) layout.findViewById(R.id.cb_star_surface_temp_value);
    }
}
