package com.fussyware.AndDiscovered;

import com.fussyware.AndDiscovered.edutils.Position;

/**
 * Created by wes on 9/2/15.
 */
public interface TrilatListener
{
    int MIN_SYSTEMS_NOT_MET = 1;
    int MIN_SYSTEMS_RAISED = 2;
    int CANCELLED = 3;

    void onSuccess(String system, Position position);
    void onFailure(String system, int reason);
    void onError(String system, Throwable thrown);
}
