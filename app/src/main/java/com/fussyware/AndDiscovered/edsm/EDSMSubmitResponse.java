package com.fussyware.AndDiscovered.edsm;

import com.fussyware.AndDiscovered.edutils.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wes on 6/28/15.
 */
public class EDSMSubmitResponse
{
    private static final String LOG_NAME = EDSMSubmitResponse.class.getSimpleName();

    private int systemsProcessed = 0;
    private boolean trilatPerformed = false;

    private TempleBaseSystemResponse baseResponse = null;
    private ArrayList<TempleDistanceResponse> distanceResponses = new ArrayList<>();

    // {"basesystem":{"msgnum":"110","msg":"base system name not found in submission - please check your API call"},"systems":null,"distances":null}
    // {"basesystem":{"msgnum":"108","name":"Teakoi JI-B d13-7","msg":"need 4 refs minimum","new":"true","refnum":"0"},"systems":[{"msgnum":"100","name":"Teakoi PV-C c26-0","msg":"system added"},{"msgnum":"308","name":"Teakoi PV-C c26-0","msg":"need 4 refs minimum","refnum":"0"}],"distances":[{"msgnum":"200","name":"Teakoi PV-C c26-0","dist":"31.76","msg":"distance saved"}]}
    public EDSMSubmitResponse(JSONObject json) throws JSONException
    {
        JSONObject basesystem = json.getJSONObject("basesystem");

        {
            EDSMResponseCode code = EDSMResponseCode.findResponseCode(basesystem.getInt("msgnum"));
            String msg = basesystem.getString("msg");
            String name = null;
            Boolean newAdd = null;
            Integer refnum = null;
            Position position = null;

            if (basesystem.has("name") && !basesystem.isNull("name")) {
                name = basesystem.getString("name");
            }

            if (basesystem.has("new") && !basesystem.isNull("new")) {
                newAdd = basesystem.getBoolean("new");
            }

            if (basesystem.has("refnum") && !basesystem.isNull("refnum")) {
                refnum = basesystem.getInt("refnum");
            }

            if (basesystem.has("coords") && !basesystem.isNull("coords")) {
                JSONObject object = basesystem.getJSONObject("coords");

                double x = object.getDouble("x");
                double y = object.getDouble("y");
                double z = object.getDouble("z");

                position = new Position(x, y, z);
            }

            trilatPerformed = (position != null);
            baseResponse = new TempleBaseSystemResponse(code, msg, name, newAdd, refnum, position);
        }

        if (!json.isNull("systems")) {
            JSONArray array = json.getJSONArray("systems");
            systemsProcessed = array.length();
        }

        if (!json.isNull("distances")) {
            JSONArray array = json.getJSONArray("distances");

            for (int i = 0; i < array.length(); i++) {
                JSONObject o = array.getJSONObject(i);

                EDSMResponseCode code = EDSMResponseCode.findResponseCode(o.getInt("msgnum"));
                String message = o.getString("msg");
                String system = null;
                Double dist = null;

                if (o.has("name") && !o.isNull("name")) {
                    system = o.getString("name");
                }

                if (o.has("dist") && !o.isNull("dist")){
                    dist = o.getDouble("dist");
                }

                distanceResponses.add(new TempleDistanceResponse(code, message, system, dist));
            }
        }
    }

    public TempleBaseSystemResponse getBaseResponse()
    {
        return baseResponse;
    }

    public int getSystemsProcessed()
    {
        return systemsProcessed;
    }

    public List<TempleDistanceResponse> getDistancesProcessed()
    {
        return distanceResponses;
    }

    public boolean isTrilatPerformed()
    {
        return trilatPerformed;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("EDSMSubmitResponse { Base Response Message [");
        sb.append(baseResponse.toString());
        sb.append("], Systems Processesed [");
        sb.append(systemsProcessed);
        sb.append("], Distances Processed [");
        sb.append(distanceResponses.toString());
        sb.append("], Trilat Performed [");
        sb.append(trilatPerformed);
        sb.append("]}");

        return sb.toString();
    }

    static final class TempleBaseSystemResponse
    {
        public final EDSMResponseCode code;
        public final String message;
        public final String systemName;
        public final Boolean newlyAdded;
        public final Integer refnum;
        public final Position position;

        TempleBaseSystemResponse(EDSMResponseCode code,
                                 String message,
                                 String systemName,
                                 Boolean newlyAdded,
                                 Integer refnum,
                                 Position position)
        {
            this.code = code;
            this.message = message;
            this.systemName = systemName;
            this.newlyAdded = newlyAdded;
            this.refnum = refnum;
            this.position = position;
        }

        @Override
        public String toString() {
            String sb = "";

            sb += "{ \"msgnum\": ";
            sb += Integer.toString(code.value());
            sb += ", \"msg\": \"";
            sb += message;
            sb += "\", \"name\": \"";
            sb += (systemName == null) ? "null" : systemName;
            sb += "\", \"new\": \"";
            sb += (newlyAdded == null) ? "null" : newlyAdded.toString();
            sb += "\", \"refnum\": ";
            sb += (refnum == null) ? "\"null\"" : refnum.toString();
            sb += ", \"coords\": {";

            if (position == null) {
                sb += "\"null\"";
            } else {
                sb += "\"x\": ";
                sb += Double.toString(position.x);
                sb += ", \"y\": ";
                sb += Double.toString(position.y);
                sb += ", \"z\": ";
                sb += Double.toString(position.z);
            }

            sb += "} }";

            return sb;
        }
    }

    static final class TempleDistanceResponse
    {
        public final EDSMResponseCode code;
        public final String systemName;
        public final Double distance;
        public final String message;

        TempleDistanceResponse(EDSMResponseCode code, String message, String name, Double distance)
        {
            this.code = code;
            this.systemName = name;
            this.distance = distance;
            this.message = message;
        }

        @Override
        public String toString() {
            String sb = "";

            sb += "{ \"msgnum\": ";
            sb += Integer.toString(code.value());
            sb += ", \"name\": \"";
            sb += (systemName == null) ? "null" : systemName;
            sb += "\", \"dist\": ";
            sb += (distance == null) ? "\"null\"" : distance.toString();
            sb += ", \"msg\": \"";
            sb += message;
            sb += "\" }";

            return sb;
        }
    }
}
