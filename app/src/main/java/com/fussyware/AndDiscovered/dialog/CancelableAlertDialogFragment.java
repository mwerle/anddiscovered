package com.fussyware.AndDiscovered.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 9/22/15.
 */
public class CancelableAlertDialogFragment extends DialogFragment
{
    private int title;

    private OnAlertListener listener;

    public interface OnAlertListener
    {
        void onPositive(String tag);
        void onNegative(String tag);
    }

    public static CancelableAlertDialogFragment newInstance(int titleId)
    {
        Bundle bundle = new Bundle();
        bundle.putInt("CancelableAlertDialogFragment.title", titleId);

        CancelableAlertDialogFragment fragment = new CancelableAlertDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("CancelableAlertDialogFragment.title", title);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof OnAlertListener) {
            listener = (OnAlertListener) activity;
        } else {
            throw new ClassCastException("Activity did not implement alert listener.");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        int iconId = android.R.drawable.ic_dialog_alert;

        if (getArguments() != null) {
            Bundle bundle = getArguments();

            title = bundle.getInt("CancelableAlertDialogFragment.title", 0);
        } else if (savedInstanceState != null) {
            title = savedInstanceState.getInt("CancelableAlertDialogFragment.title", 0);
        }

        return new AlertDialog
                .Builder(getActivity())
                .setIcon(iconId)
                .setTitle(title)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (listener != null) {
                            listener.onPositive(getTag());
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (listener != null) {
                            listener.onNegative(getTag());
                        }
                    }
                })
                .create();
    }
}
