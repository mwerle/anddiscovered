package com.fussyware.AndDiscovered.edsm;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.edsc.EDSCDbContract;

/**
 * Created by wboyd on 7/7/15.
 */
public class EDSMDistanceInfo extends DistanceInfo
{
    private final SQLiteDatabase db;
    private final long id;

    EDSMDistanceInfo(SQLiteDatabase db, long id, String first, String second, double distance)
    {
        super(first, second, distance);

        this.db = db;
        this.id = id;
    }

    @Override
    public void setDistance(double distance)
    {
        distance = (distance < 0) ? -distance : distance;

        if (this.getDistance() != distance) {
            super.setDistance(distance);

            String selection = EDSCDbContract.EDSCDistances._ID + "=?";
            String[] selectionArgs = {Long.toString(id)};

            ContentValues values = new ContentValues();
            values.put(EDSMDbContract.Distances.COLUMN_NAME_DISTANCE, distance);

            db.update(EDSMDbContract.Distances.TABLE_NAME,
                      values,
                      selection,
                      selectionArgs);
        }
    }
}
