package com.fussyware.AndDiscovered.edproxy;

/**
 * Created by wes on 6/15/15.
 */
public interface ProxyListener
{
    public void messageReceived(ProxyMessage message);
}
