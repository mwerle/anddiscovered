package com.fussyware.AndDiscovered.activity;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.CancelableAlertDialogFragment;
import com.fussyware.AndDiscovered.dialog.TextPickerDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrStarBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.fragment.CelestialBodyListFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.StarIconGridFragment;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.Stack;

/**
 * Created by wes on 11/10/15.
 */
public class CelestialListActivity
        extends BaseActivity
        implements CelestialBodyListFragment.CelestialBodyListener,
                   StarIconGridFragment.OnStarClickListeners,
                   TextPickerDialogFragment.OnTextChangedListener,
                   CancelableAlertDialogFragment.OnAlertListener

{
    private static final String LOG_NAME = CelestialListActivity.class.getSimpleName();
    private static final String BACKSTACK_HOLDER_FRAGMENT = "celestial_list_activity.back_stack_fragment";

    private CmdrSystemInfo systemInfo;

    private BackStackHolderFragment backStackFragment;
    private CelestialBodyListFragment celestialFragment;
    private StarIconGridFragment starFragment = new StarIconGridFragment();

    private SlidingUpPanelLayout slidePanel;

    private ActionMode actionMode;
    private final LongClickActionBar actionCallback = new LongClickActionBar();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(getString(R.string.celestial_list_activity_title));
        setContentView(R.layout.celestial_list_activity_layout);
        setDrawerIndicatorEnabled(false);

        TitleInfoFragment titleFragment = (TitleInfoFragment) getFragmentManager().findFragmentById(R.id.title_info_fragment);
        celestialFragment = (CelestialBodyListFragment) getFragmentManager().findFragmentById(R.id.celestial_list_fragment);

        backStackFragment = (BackStackHolderFragment) getFragmentManager().findFragmentByTag(BACKSTACK_HOLDER_FRAGMENT);

        if (backStackFragment == null) {
            backStackFragment = new BackStackHolderFragment();
            backStackFragment.setRetainInstance(true);
            getFragmentManager()
                    .beginTransaction()
                    .add(backStackFragment, BACKSTACK_HOLDER_FRAGMENT)
                    .commit();
        }

        setupSlideUpMenu();

        if (savedInstanceState == null) {
            systemInfo = getIntent().getParcelableExtra("system");

            celestialFragment.setSystem(systemInfo);
            titleFragment.setSystem(systemInfo);

            backStackFragment.backStack.clear();
        } else {
            systemInfo = savedInstanceState.getParcelable("system");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("system", systemInfo);
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            synchronized (backStackFragment.backStack) {
                if (!backStackFragment.backStack.isEmpty()) {
                    CelestialBody body = null;

                    backStackFragment.backStack.pop();

                    if (!backStackFragment.backStack.isEmpty()) {
                        body = backStackFragment.backStack.peek();
                    }

                    celestialFragment.setCelestialRoot(body);
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        super.onBackPressed();
        return true;
    }

    @Override
    protected void onNewSystem(SystemInfo system)
    {
        Intent intent = new Intent(getApplicationContext(), SystemInfoActivity.class);
        intent.putExtra("system", system);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    @Override
    public void onStarIconClick(int position, View view, StarType type)
    {
        CelestialBody root;

        synchronized (backStackFragment.backStack) {
            root = (backStackFragment.backStack.isEmpty() ?
                    null :
                    backStackFragment.backStack.peek());
        }

        celestialFragment.add(CmdrStarBody.create(systemInfo, type, root));
    }

    @Override
    public void onStarLongClick(int position, View view, StarType type)
    {
        Intent intent = new Intent("star_selection_icon_drag");
        intent.putExtra("type", type.value);

        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_INTENT};
        ClipData.Item item = new ClipData.Item(intent);
        ClipData clipData = new ClipData(type.toString(), mimeTypes, item);

        View.DragShadowBuilder shadow = new View.DragShadowBuilder(view);

        if (slidePanel != null) {
            slidePanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }

        view.startDrag(clipData, shadow, null, 0);
    }

    @Override
    public void onCelestialBodyItemClick(int position, View view, CelestialBody body)
    {
        if (actionMode != null) {
            actionMode.finish();
        }

        if (body.getSatellites().isEmpty()) {
            Intent intent = new Intent(getApplicationContext(), CelestialInfoActivity.class);
            intent.putExtra("celestial", body);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            startActivity(intent);
        } else {
            synchronized (backStackFragment.backStack) {
                backStackFragment.backStack.push(body);
            }

            celestialFragment.setCelestialRoot(body);
        }
    }

    @Override
    public boolean onCelestialBodyItemLongClick(int position, View view, CelestialBody body)
    {
        if (actionMode == null) {
            actionCallback.setSelected(position, view, body);
            actionMode = startActionMode(actionCallback);
        }
        return true;
    }

    @Override
    public boolean onCelestialBodyDrag(View view, DragEvent event, CelestialBody body)
    {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                return event.getClipDescription()
                            .hasMimeType(ClipDescription.MIMETYPE_TEXT_INTENT);
            case DragEvent.ACTION_DROP: {
                Intent intent = event.getClipData().getItemAt(0).getIntent();

                switch (intent.getAction()) {
                    case "star_selection_icon_drag":
                        StarType type = StarType.getStarType(intent.getIntExtra("type",
                                                                                StarType.Unknown.value));

                        StarBody starBody = CmdrStarBody.create(systemInfo, type, body);

                        if (isBodySelected(body)) {
                            celestialFragment.add(starBody);
                        }

                        return true;
                    default:
                        return false;
                }
            }
            default:
                return false;
        }
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.celestial_body_list_fragment.duplicate_dialog:
                int value = Integer.valueOf(text);

                for (int i = 0; i < value; i++) {
                    CelestialBody parent = actionCallback.body.getParent();

                    if (actionCallback.body instanceof StarBody) {
                        celestialFragment.add(CmdrStarBody.create(systemInfo,
                                                                  ((StarBody) actionCallback.body).getType(),
                                                                  parent));
                    }
                }

                actionMode.finish();
                break;
            default:
                super.onDone(tag, text);
                break;
        }
    }

    @Override
    public void onPositive(String tag)
    {
        switch (tag) {
            case FragmentTag.celestial_body_list_fragment.delete_dialog:
                celestialFragment.remove(actionCallback.position);
                actionCallback.body.delete();
                actionMode.finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onNegative(String tag)
    {

    }

    public static class BackStackHolderFragment extends Fragment
    {
        public final Stack<CelestialBody> backStack = new Stack<>();
    }

    private boolean isBodySelected(CelestialBody body)
    {
        CelestialBody root;

        synchronized (backStackFragment.backStack) {
            root = (backStackFragment.backStack.isEmpty() ?
                    null :
                    backStackFragment.backStack.peek());
        }

        return (root == body);
    }

    private void setupSlideUpMenu()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.icon_selection_frame,
               starFragment,
               FragmentTag.star_icon_grid_fragment.tag);
        ft.commit();

        /** The sliding up panel is only available on
         * mobile platforms when in portrait mode.
         */
        final View slideHandlebar = findViewById(R.id.slide_handle_bar);
        if (slideHandlebar != null) {
            /** Make the sliding handle bar disappear when we
             * have the sliding menu visible. This way it does not
             * stick out and take up unnecessary space.
             */
            slidePanel = (SlidingUpPanelLayout) findViewById(R.id.slide_up_panel);
            slidePanel.setPanelSlideListener(new SlidingUpPanelLayout.SimplePanelSlideListener()
            {
                @Override
                public void onPanelSlide(View panel, float slideOffset)
                {
                    if (slideOffset >= 0.5) {
                        if (slideHandlebar.getVisibility() != View.GONE) {
                            slideHandlebar.setVisibility(View.GONE);
                        }
                    } else {
                        if (slideHandlebar.getVisibility() != View.VISIBLE) {
                            slideHandlebar.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }
    }

    private class LongClickActionBar implements ActionMode.Callback
    {
        public int position;
        public View view;
        public CelestialBody body;

        private void setSelected(int position, View view, CelestialBody body)
        {
            this.position = position;
            this.view = view;
            this.body = body;

            view.setSelected(true);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu)
        {
            mode.getMenuInflater().inflate(R.menu.celestial_body_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu)
        {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item)
        {
            switch (item.getItemId()) {
                case R.id.cb_delete_menu: {
                    DialogFragment df = CancelableAlertDialogFragment.newInstance(R.string.cb_delete_menu_title);
                    df.show(getFragmentManager(), FragmentTag.celestial_body_list_fragment.delete_dialog);

                    return true;
                }
                case R.id.cb_duplicate_menu: {
                    String[] values = new String[255];

                    for (int i = 0; i < 255; i++) {
                        values[i] = String.format("%d", i + 1);
                    }

                    TextPickerDialogFragment df = TextPickerDialogFragment.newInstance(getString(R.string.cb_duplicate_menu_title),
                                                                                       getString(R.string.cb_duplicate_menu_summary),
                                                                                       values,
                                                                                       "1");
                    df.show(getFragmentManager(), FragmentTag.celestial_body_list_fragment.duplicate_dialog);

                    return true;
                }
                case R.id.cb_edit_menu: {
                    Intent intent = new Intent(getApplicationContext(), CelestialInfoActivity.class);
                    intent.putExtra("celestial", body);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    actionMode.finish();

                    startActivity(intent);
                    return true;
                }
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode)
        {
            view.setSelected(false);

            actionMode = null;
            view = null;
            body = null;
        }
    }
}
