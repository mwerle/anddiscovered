package com.fussyware.AndDiscovered.edsm;

import android.util.Log;

import com.fussyware.AndDiscovered.json.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.concurrent.Callable;

/**
 * Created by wes on 8/21/15.
 */
class AsyncSubmitDistanceCallable implements Callable<EDSMSubmitResponse>
{
    private static final String LOG_NAME = AsyncSubmitDistanceCallable.class.getSimpleName();

    private EDSMSubmitDistances distances;

    public AsyncSubmitDistanceCallable(EDSMSubmitDistances distances)
    {
        this.distances = distances;
    }

    @Override
    public EDSMSubmitResponse call() throws Exception
    {
        HttpURLConnection conn = null;

        try {
            conn = EDSMConnection.sendRequest("submit-distances", distances);

//            Log.d(LOG_NAME, "Waiting for response.");
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
//                Log.d(LOG_NAME, "Response was good!");
                try {
                    JsonParser parser = new JsonParser(new InputStreamReader(conn.getInputStream()));

                    JSONObject o = parser.getBaseObject();
//                    Log.d(LOG_NAME, "JSON Response: " + o.toString());
                    if (null != o) {
                        return new EDSMSubmitResponse(o);
                    } else {
                        Log.e(LOG_NAME, "Failed to parse JSON distance.");
                    }
                } catch (IOException e) {
                    Log.e(LOG_NAME, "[IO Error] Failed to parse the submit distance JSON. ", e);
                } catch (JSONException e) {
                    Log.e(LOG_NAME, "[JSON Error] Failed to parse the submit distance JSON. ", e);
                }
            } else {
                Log.e(LOG_NAME, "Failed attempting submitting distances. Code: " + Integer.toString(conn.getResponseCode()));
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_NAME, "Failed connecting to Temple due to bad URL. ", e);
        } catch (IOException e) {
            Log.e(LOG_NAME, "Failed connecting to Temple due to unknown IO error. ", e);
        } catch (Exception e) {
            Log.e(LOG_NAME, "Failed with unknown error when talking with EDSM server.", e);
        } finally {
            if (null != conn) {
                conn.disconnect();
            }
        }

        return null;
    }
}
