package com.fussyware.AndDiscovered.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.AsteroidBody;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.dialog.TextDialogFragment;

import java.security.InvalidParameterException;

/**
 * Created by wes on 11/17/15.
 */
public class CelestialInfoHeaderFragment extends Fragment
{
    private static final String LOG_NAME = CelestialInfoHeaderFragment.class.getSimpleName();

    private final Object mutex = new Object();

    private CelestialBody celestialBody;

    private TextView nameText;
    private TextView typeText;
    private TextView descriptionText;
    private Button satButton;
    private ImageView scoopableImage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.cb_info_header_layout, container, false);

        createViews(layout);

        if (savedInstanceState != null) {
            CelestialBody body = savedInstanceState.getParcelable("celestial");
            setCelestialBody(body);
        }

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial", celestialBody);
    }

    public void setCelestialBody(@NonNull CelestialBody body)
    {
        synchronized (mutex) {
            if (body != celestialBody) {
                celestialBody = body;

                populateName(body.getName());
                populateHeader();
                populateSatButton();

                if (body.getSatelliteCategory() == SatelliteCategory.Star) {
                    StarBody sb = (StarBody) body;

                    switch (sb.getType()) {
                        case A:
                        case B:
                        case F:
                        case G:
                        case K:
                        case M:
                        case O:
                            scoopableImage.setVisibility(View.VISIBLE);
                            break;
                        default:
                            scoopableImage.setVisibility(View.GONE);
                            break;
                    }
                }
            }
        }
    }

    public void onDialogDone(@NonNull String text)
    {
        celestialBody.setName(text);
        populateName(text);
    }

    private void createViews(View layout)
    {
        nameText = (TextView) layout.findViewById(R.id.cb_body_name_title);

        typeText = (TextView) layout.findViewById(R.id.cb_body_type_title);
        descriptionText = (TextView) layout.findViewById(R.id.cb_description);

        satButton = (Button) layout.findViewById(R.id.cb_satellite_button);
        satButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO: Do something usefull here.
                Log.d(LOG_NAME, "button clicked. " + satButton.getText().toString());
            }
        });

        scoopableImage = (ImageView) layout.findViewById(R.id.cb_scoopable_image);
        scoopableImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String summary;

                int value = ((StarBody) celestialBody).getType().value - 1;
                String[] types;

                types = getResources().getStringArray(R.array.star_types);
                summary = String.format(getResources().getString(R.string.cb_star_is_scoopable),
                                        types[value]);

                Toast.makeText(getActivity(),
                               summary,
                               Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void populateName(String name)
    {
        if (name.isEmpty()) {
            nameText.setText(R.string.cb_name_set_text);
            nameText.setTextColor(getActivity().getResources().getColor(R.color.ed_cyan));
            nameText.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    String content = celestialBody.getName();

                    if (content.isEmpty()) {
                        content = celestialBody.getSystem().getSystem();
                    }

                    String title = getResources().getString(R.string.cb_name_dialog_title);
                    String summary = getResources().getString(R.string.cb_name_dialog_summary);
                    
                    TextDialogFragment df = TextDialogFragment.newInstance(title,
                                                                           summary,
                                                                           content,
                                                                           InputType.TYPE_CLASS_TEXT |
                                                                           InputType.TYPE_TEXT_FLAG_CAP_WORDS);

                    df.show(getFragmentManager(), FragmentTag.celestial_info_fragment.celestial_name_dialog);
                }
            });
        } else {
            nameText.setText(name);
            nameText.setTextColor(getActivity().getResources().getColor(R.color.ed_primary_orange));
            nameText.setOnClickListener(null);
        }
    }

    private void populateHeader()
    {
        String typeName = "";
        String description = "";

        switch (celestialBody.getSatelliteCategory()) {
            case Star: {
                int value = ((StarBody) celestialBody).getType().value - 1;
                String[] types;

                types = getResources().getStringArray(R.array.star_types);
                typeName = types[value];

                types = getResources().getStringArray(R.array.star_descriptions);
                description = types[value];

                break;
            }
            case Planet: {
                int value = ((PlanetBody) celestialBody).getType().value - 1;
                String[] types;

                types = getResources().getStringArray(R.array.planet_types);
                typeName = types[value];

                types = getResources().getStringArray(R.array.planet_descriptions);
                description = types[value];

                break;
            }
            case Asteroid: {
                int value = ((AsteroidBody) celestialBody).getType().value;
                String[] types;

                types = getResources().getStringArray(R.array.mineral_types);
                typeName = types[value];

                types = getResources().getStringArray(R.array.mineral_descriptions);
                description = types[value];

                break;
            }
            case Unknown:
                throw new InvalidParameterException("A valid Satellite category must be provided.");
        }

        typeText.setText(typeName);
        descriptionText.setText(description);
    }

    private void populateSatButton()
    {
        String value;

        if (celestialBody.hasSatellites()) {
            value = getResources().getString(R.string.cb_button_view_sats);
        } else {
            value = getResources().getString(R.string.cb_button_add_sats);
        }

        satButton.setText(value);
    }
}
