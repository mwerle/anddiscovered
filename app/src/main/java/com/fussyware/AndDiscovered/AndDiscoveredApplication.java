package com.fussyware.AndDiscovered;

import android.app.Application;
import android.content.Intent;

import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;

/**
 * Created by wes on 9/28/15.
 */
public class AndDiscoveredApplication extends Application
{
    private Intent proxyIntent;
    private CmdrDbHelper dbHelper;
    private ProxyConnectionState proxyState;
    private StarCoordinator starCoordinator;

    @Override
    public void onCreate()
    {
        if (dbHelper == null) {
            // Init and hold a reference so it does not get cleaned up.
            CmdrDbHelper.init(this);
            dbHelper = CmdrDbHelper.getInstance();
        }

        if (proxyState == null) {
            ProxyConnectionState.init(this);
            proxyState = ProxyConnectionState.getInstance();
        }

        if (starCoordinator == null) {
            starCoordinator = StarCoordinatorFactory.getInstance(this,
                                                                 StarCoordinatorFactory.StarCoordinatorType.EDSM);
        }

        if (proxyIntent == null) {
            proxyIntent = new Intent(this, EDProxyIntentService.class);

            startService(proxyIntent);
            startService(starCoordinator.getIntentService(this));
        }

        super.onCreate();
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();

        stopService(proxyIntent);
        stopService(starCoordinator.getIntentService(this));

        dbHelper = null;
        proxyState = null;
        starCoordinator = null;
    }
}
