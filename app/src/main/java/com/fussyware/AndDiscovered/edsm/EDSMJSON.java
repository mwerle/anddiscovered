package com.fussyware.AndDiscovered.edsm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by wes on 6/25/15.
 */
public abstract class EDSMJSON
{
    public static final int VERSION = 2;
    public static final boolean DEFAULT_TEST = false;

    protected final Object mutex = new Object();

    private AtomicBoolean test = new AtomicBoolean(DEFAULT_TEST);

    public int getVersion()
    {
        return VERSION;
    }

    public boolean isTest()
    {
        return test.get();
    }

    public void setTest(boolean test)
    {
        this.test.set(test);
    }

    protected abstract void fillJSON(JSONObject baseObject) throws JSONException;

    public String getJSON()
    {
        try {
            JSONObject baseObject = new JSONObject();

            synchronized (mutex) {
                baseObject.put("ver", getVersion());

                if (DEFAULT_TEST != isTest()) {
                    baseObject.put("test", isTest());
                }

                fillJSON(baseObject);
            }

            JSONObject object = new JSONObject();
            object.put("data", baseObject);

            return object.toString();
        } catch (JSONException e) {
            return "";
        }
    }
}
