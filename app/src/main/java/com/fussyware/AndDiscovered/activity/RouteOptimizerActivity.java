package com.fussyware.AndDiscovered.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.preference.PreferenceTag;

/**
 * Created by wes on 11/2/15.
 */
public class RouteOptimizerActivity extends BaseActivity
{
    private static final Position SAGITTARIUS_A = new Position(25.21875,
                                                               -20.90625,
                                                               25899.96875);

    private int jumpDistance;
    private int sagaDistance;
    private int targetDist;
    private int routeRange;

    private EditText sagaDistText;
    private TextView targetText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.route_optimizer_activity_layout);
        setTitle(R.string.route_opt_title);

        final EditText jumpText = (EditText) findViewById(R.id.max_range_edit);
        Button calculateButton = (Button) findViewById(R.id.calculate_button);

        sagaDistText = (EditText) findViewById(R.id.saga_distance_edit);
        targetText = (TextView) findViewById(R.id.route_distance_text);

        calculateButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String jump, saga;

                jump = jumpText.getText().toString();
                saga = sagaDistText.getText().toString();

                if (!jump.isEmpty() && !saga.isEmpty()) {
                    jumpDistance = Integer.valueOf(jump);
                    sagaDistance = Integer.valueOf(saga);

                    if (jumpDistance > 0) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt(PreferenceTag.MAX_JUMP_DISTANCE, jumpDistance);
                        editor.apply();

                        int N = routeRange / jumpDistance;
                        int M = jumpDistance * N;

                        targetDist = M - ((N / 4) + (sagaDistance * 2));

                        String text = getResources().getString(R.string.route_opt_target_dist);
                        targetText.setText(String.format(text, targetDist));
                    }
                }
            }
        });

        if (savedInstanceState == null) {
            jumpDistance = preferences.getInt(PreferenceTag.MAX_JUMP_DISTANCE, 0);
            targetDist = 0;
            routeRange = 1000;
        } else {
            jumpDistance = savedInstanceState.getInt("jump_distance");
            targetDist = savedInstanceState.getInt("target_jump");
            routeRange = savedInstanceState.getInt("route_range");
        }

        jumpText.setText((jumpDistance == 0) ? "" : Integer.toString(jumpDistance));

        String text = getResources().getString(R.string.route_opt_target_dist);
        targetText.setText(String.format(text, targetDist));
    }

    @Override
    protected void onStart()
    {
        Position position = CmdrDbHelper.getInstance().getCurrentSystem().getPosition();

        if (position == null) {
            sagaDistance = 0;
        } else {
            double x, y, z;

            // Get the vector for (x, y, z)
            x = SAGITTARIUS_A.x - position.x;
            y = SAGITTARIUS_A.y - position.y;
            z = SAGITTARIUS_A.z - position.z;

            // Now perform the Pythagorean Theorem
            x *= x;
            y *= y;
            z *= z;

            sagaDistance = Double.valueOf(Math.floor(Math.sqrt(x + y + z))).intValue();
        }

        sagaDistText.setText((sagaDistance == 0) ? "" : Integer.toString(sagaDistance / routeRange));

        super.onStart();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putInt("jump_distance", jumpDistance);
        outState.putInt("target_jump", targetDist);
        outState.putInt("route_range", routeRange);
    }
}
