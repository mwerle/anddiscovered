package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wboyd on 7/7/15.
 */
public class CmdrSystemInfo extends SystemInfo
{
    public static final Parcelable.Creator<CmdrSystemInfo> CREATOR = new Creator<CmdrSystemInfo>()
    {
        @Override
        public CmdrSystemInfo createFromParcel(Parcel source)
        {
            return new CmdrSystemInfo(source);
        }

        @Override
        public CmdrSystemInfo[] newArray(int size)
        {
            return new CmdrSystemInfo[size];
        }
    };

    private final SQLiteDatabase db;
    private final long id;
    private final AtomicInteger trilat_req;
    private String note;
    private StarType mainStar;
    private int stellarBodies;
    private int planetaryBodies;

    private final Object mutex = new Object();

    CmdrSystemInfo(SQLiteDatabase db,
                   long id,
                   String system,
                   Position xyz,
                   int trilat_systems_required,
                   String note)
    {
        super(system, xyz);

        this.db = db;
        this.id = id;
        this.trilat_req = new AtomicInteger(trilat_systems_required);
        this.note = note;

        mainStar = StarType.Unknown;
        stellarBodies = 0;
        planetaryBodies = 0;
    }

    CmdrSystemInfo(SQLiteDatabase db,
                   long id,
                   String system,
                   Position xyz,
                   int trilat_systems_required,
                   String note,
                   StarType mainStar,
                   int stellarBodies,
                   int planetaryBodies,
                   SystemImageInfo[] images)
    {
        super(system, xyz);

        this.db = db;
        this.id = id;
        this.trilat_req = new AtomicInteger(trilat_systems_required);
        this.note = note;
        this.mainStar = mainStar;
        this.stellarBodies = stellarBodies;
        this.planetaryBodies = planetaryBodies;
    }

    private CmdrSystemInfo(Parcel parcel)
    {
        super(parcel);

        db = CmdrDbHelper.getInstance().getWritableDatabase();
        id = parcel.readLong();
        trilat_req = new AtomicInteger(parcel.readInt());
        note = parcel.readString();
        mainStar = StarType.getStarType(parcel.readInt());
        stellarBodies = parcel.readInt();
        planetaryBodies = parcel.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeLong(id);
        dest.writeInt(trilat_req.get());
        dest.writeString(note);
        dest.writeInt(mainStar.value());
        dest.writeInt(stellarBodies);
        dest.writeInt(planetaryBodies);
    }

    public long getRowId()
    {
        return id;
    }

    @Override
    public void setPosition(Position xyz)
    {
        if ((xyz != null) && (getPosition() != xyz)) {
            super.setPosition(xyz);

            String selection = CmdrDbContract.CmdrSystems._ID + "=?";
            String[] selectionArgs = {Long.toString(id)};

            ContentValues values = new ContentValues();
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD, xyz.x);
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD, xyz.y);
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD, xyz.z);

            db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                      values,
                      selection,
                      selectionArgs);
        }
    }

    public int getMinSystemsRequiredForTrilat()
    {
        return trilat_req.get();
    }

    public void setMinSystemsRequiredForTrilat(int required)
    {
        int req = trilat_req.get();

        if (req != required) {
            trilat_req.set(required);
            updateTrilatDb();
        }
    }

    public void incrementMinSystemsRequiredForTrilat()
    {
        trilat_req.incrementAndGet();
        updateTrilatDb();
    }

    public String getNote()
    {
        synchronized (mutex) {
            return note;
        }
    }

    public void setNote(String note)
    {
        if (note == null) note = "";

        synchronized (mutex) {
            if (this.note.equals(note)) return;

            this.note = note;
        }

        String selection = CmdrDbContract.CmdrSystems._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_NOTE, note);

        db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                  values,
                  selection,
                  selectionArgs);
    }

    @Override
    public List<CelestialBody> getSatellites()
    {
        String[] columns = {
                CmdrDbContract.Satellites.COLUMN_NAME_BODYID,
                CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
        };

        String selection;

        selection  = CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_PARENTID + " IS NULL";

        String[] selectionArgs = { Long.toString(id) };

        ArrayList<CelestialBody> rootList = new ArrayList<>();

        Cursor cursor = db.query(CmdrDbContract.Satellites.TABLE_NAME,
                                    columns,
                                    selection,
                                    selectionArgs,
                                    null,
                                    null,
                                    null);
        try {
            while (cursor.moveToNext()) {
                CelestialBody body = null;
                SatelliteCategory satCat = SatelliteCategory.getSatelliteCategory(cursor.getInt(1));
                CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();

                switch (satCat) {
                    case Star:
                        body = dbHelper.getStarSatellite(this, null, cursor.getLong(0));
                        break;
                    case Planet:
                        body = dbHelper.getPlanetSatellite(this, null, cursor.getLong(0));
                        break;
                    case Asteroid:
                        body = dbHelper.getAsteroidSatellite(this, null, cursor.getLong(0));
                        break;
                    case Unknown:
                        break;
                }

                if (body != null) {
                    rootList.add(body);
                }
            }
        } finally {
            cursor.close();
        }

        return rootList;
    }

    public StarType getMainStar()
    {
        synchronized (mutex) {
            return mainStar;
        }
    }

    public void setMainStar(StarType starType)
    {
        synchronized (mutex) {
            if (starType != this.mainStar) {
                String selection = CmdrDbContract.CmdrSystems._ID + "=?";
                String[] selectionArgs = { Long.toString(id) };

                ContentValues values = new ContentValues();
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR, starType.value());

                db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                          values,
                          selection,
                          selectionArgs);

                mainStar = starType;
            }
        }
    }

    public int getStellarBodies()
    {
        synchronized (mutex) {
            return stellarBodies;
        }
    }

    public void setStellarBodies(int stellarBodies)
    {
        synchronized (mutex) {
            if (stellarBodies != this.stellarBodies) {
                String selection = CmdrDbContract.CmdrSystems._ID + "=?";
                String[] selectionArgs = { Long.toString(id) };

                ContentValues values = new ContentValues();
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_STELLAR_BODIES, stellarBodies);

                db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                          values,
                          selection,
                          selectionArgs);

                this.stellarBodies = stellarBodies;
            }
        }
    }

    public int getPlanetaryBodies()
    {
        synchronized (mutex) {
            return planetaryBodies;
        }
    }

    public void setPlanetaryBodies(int planetaryBodies)
    {
        synchronized (mutex) {
            if (planetaryBodies != this.planetaryBodies) {
                String selection = CmdrDbContract.CmdrSystems._ID + "=?";
                String[] selectionArgs = { Long.toString(id) };

                ContentValues values = new ContentValues();
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_PLANETARY_BODIES, planetaryBodies);

                db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                          values,
                          selection,
                          selectionArgs);

                this.planetaryBodies = planetaryBodies;
            }
        }
    }

    // TODO: Remove old code
//    public StarType[] getStars()
//    {
//        synchronized (mutex) {
//            return starList.toArray(new StarType[starList.size()]);
//        }
//    }
//
//    public void setStars(StarType[] stars)
//    {
//        synchronized (mutex) {
//            ArrayList<StarType> list = new ArrayList<>(stars.length);
//            Collections.addAll(list, stars);
//
//            if (!starList.equals(list)) {
//                db.beginTransaction();
//
//                try {
//                    String where = CmdrDbContract.Stars.COLUMN_NAME_SYSTEMID + "=?";
//                    String[] whereArgs = new String[] {Long.toString(id)};
//                    db.delete(CmdrDbContract.Stars.TABLE_NAME, where, whereArgs);
//
//                    for (StarType type : list) {
//                        ContentValues values = new ContentValues();
//                        values.put(CmdrDbContract.Stars.COLUMN_NAME_SYSTEMID, id);
//                        values.put(CmdrDbContract.Stars.COLUMN_NAME_TYPE, type.value());
//
//                        db.insert(CmdrDbContract.Stars.TABLE_NAME, null, values);
//                    }
//
//                    db.setTransactionSuccessful();
//                } finally {
//                    db.endTransaction();
//                }
//
//                starList.clear();
//                starList.addAll(list);
//            }
//        }
//    }
//
//    public PlanetType[] getPlanets()
//    {
//        synchronized (mutex) {
//            return planetList.toArray(new PlanetType[planetList.size()]);
//        }
//    }
//
//    public void setPlanets(PlanetType[] planets)
//    {
//        synchronized (mutex) {
//            ArrayList<PlanetType> list = new ArrayList<>(planets.length);
//            Collections.addAll(list, planets);
//
//            if (!planetList.equals(list)) {
//                db.beginTransaction();
//
//                try {
//                    String where = CmdrDbContract.Planets.COLUMN_NAME_SYSTEMID + "=?";
//                    String[] whereArgs = new String[] {Long.toString(id)};
//                    db.delete(CmdrDbContract.Planets.TABLE_NAME, where, whereArgs);
//
//                    for (PlanetType type : list) {
//                        ContentValues values = new ContentValues();
//                        values.put(CmdrDbContract.Planets.COLUMN_NAME_SYSTEMID, id);
//                        values.put(CmdrDbContract.Planets.COLUMN_NAME_TYPE, type.value());
//
//                        db.insert(CmdrDbContract.Planets.TABLE_NAME, null, values);
//                    }
//
//                    db.setTransactionSuccessful();
//                } finally {
//                    db.endTransaction();
//                }
//
//                planetList.clear();
//                planetList.addAll(list);
//            }
//        }
//    }

    public SystemImageInfo[] getSystemImages()
    {
        synchronized (mutex) {
            return CmdrDbHelper.getInstance().getSystemImages(id, getSystem());
        }
    }

    public SystemImageInfo getSystemImage(String celestialName)
    {
        if (celestialName == null) {
            throw new NullPointerException("Celestial name may not be null");
        }

        synchronized (mutex) {
            SystemImageInfo[] imageList = CmdrDbHelper.getInstance().getSystemImages(id, getSystem());

            for (SystemImageInfo info : imageList) {
                if (celestialName.equalsIgnoreCase(info.getCelestialName())) {
                    return info;
                }
            }
        }

        return null;
    }

    public SystemImageInfo createImageInfo(String urlPath)
    {
        if ((urlPath == null) || urlPath.isEmpty()) {
            throw new IllegalArgumentException("Invalid image path name specified.");
        }

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Images.COLUMN_NAME_SYSTEMID, this.id);
        values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_NAME, "");
        values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_TYPE, "");
        values.put(CmdrDbContract.Images.COLUMN_NAME_DISTANCE_FROM_MAINSTAR, 0.0);
        values.put(CmdrDbContract.Images.COLUMN_NAME_URL_PATH, urlPath);

        long id = db.insert(CmdrDbContract.Images.TABLE_NAME,
                            null,
                            values);

        SystemImageInfo info = new SystemImageInfo(db,
                                                   id,
                                                   this.id,
                                                   getSystem(),
                                                   "",
                                                   "",
                                                   0.0,
                                                   urlPath);

        return info;
    }

    public boolean deleteImageInfo(SystemImageInfo info)
    {
        synchronized (mutex) {
            info.delete();
        }

        return true;
    }

    private void updateTrilatDb()
    {
        String selection = CmdrDbContract.CmdrSystems._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_TRILAT_SYSTEMS, trilat_req.get());

        db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }
}
