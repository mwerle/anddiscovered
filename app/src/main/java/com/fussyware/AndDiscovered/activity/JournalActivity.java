package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.NotesFragment;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;

/**
 * Created by wes on 9/30/15.
 */
public class JournalActivity
        extends BaseActivity
        implements NotesFragment.OnDoneListener
{
    private CmdrSystemInfo systemInfo;
    private boolean modified;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //TODO: Put title here
        setTitle(R.string.journal_activity_title);
        setContentView(R.layout.notes_activity_layout);
        setDrawerIndicatorEnabled(false);

        TitleInfoFragment titleFragment = (TitleInfoFragment) getFragmentManager().findFragmentById(R.id.title_info_fragment);
        NotesFragment notesFragment = (NotesFragment) getFragmentManager().findFragmentById(R.id.notes_fragment);

        if (savedInstanceState == null) {
            systemInfo = CmdrDbHelper
                    .getInstance()
                    .getSystem(getIntent().getStringExtra("system"));
        } else {
            systemInfo = CmdrDbHelper
                    .getInstance()
                    .getSystem(savedInstanceState.getString("system"));
            modified = savedInstanceState.getBoolean("modified");
        }

        titleFragment.setSystem(systemInfo);
        notesFragment.setSystem(systemInfo);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("system", systemInfo.getSystem());
        outState.putBoolean("modified", modified);
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            if (modified) {
                Intent intent = new Intent();
                intent.putExtra("modified", modified);

                setResult(Activity.RESULT_OK, intent);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        onBackPressed();
        return true;
    }

    @Override
    public void onDone(String tag, String text)
    {
        if (tag.equals(FragmentTag.notes_fragment.tag)) {
            modified = true;
        }
    }
}
