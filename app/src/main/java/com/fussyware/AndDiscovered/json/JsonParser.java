package com.fussyware.AndDiscovered.json;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;

/**
 * Created by wes on 6/16/15.
 */
public class JsonParser
{
    private JsonReader json;

    public JsonParser(Reader reader) throws IOException
    {
        this.json = new JsonReader(reader);
        this.json.setLenient(true);
    }

    public JSONObject getBaseObject() throws IOException
    {
        boolean running = true;

        while (running) {
            JsonToken token = json.peek();

            switch (token) {
                case BEGIN_ARRAY:
                    // EDProxy JSON will ALWAYS start with an object! Error here.
                    Log.d("JsonParser", "Got an BEGIN_ARRAY from main thread");
                    break;
                case BEGIN_OBJECT:
                    json.beginObject();
                    return readObject(json);
                case END_ARRAY:
                    // We should never end on an array!
                    Log.d("JsonParser", "Got an END_ARRAY from main thread");
                    break;
                case END_OBJECT:
                    // We should never have an END_OBJECT as this is absorbed by the routine.
                    Log.d("JsonParser", "Got an END_OBJECT from main thread");
                    break;
                case END_DOCUMENT:
                    running = false;

                    continue;
                default:
                    break;
            }

            if (json.hasNext()) {
                json.skipValue();
            }
        }

        return null;
    }

    private JSONObject readObject(JsonReader json) throws IOException {
        JSONObject object = new JSONObject();

        String name = null;
        boolean running = true;

        while (running) {
            JsonToken token = json.peek();

            switch (token) {
                case BEGIN_ARRAY:
                    json.beginArray();

                    JSONArray list = readArray(json);
                    if (null != list) {
                        try {
                            object.put(name, list);
                        } catch (JSONException e) {
                            running = false;
                        }
                    }

                    continue;
                case BEGIN_OBJECT:
                    json.beginObject();

                    JSONObject o = readObject(json);
                    if (null != o) {
                        try {
                            object.put(name, o);
                        } catch (JSONException e) {
                            running = false;
                        }
                    }

                    continue;
                case END_OBJECT:
                    json.endObject();
                    return object;
                case END_DOCUMENT:
                    return object;
                case NAME:
                    name = json.nextName();
                    continue;
                default:
                    break;
            }

            if (json.hasNext()) {
                try {
                    if (token == JsonToken.NULL) {
                        json.nextNull();
                        object.put(name, JSONObject.NULL);
                    } else if (token == JsonToken.BOOLEAN) {
                        object.put(name, json.nextBoolean());
                    } else if (token == JsonToken.NUMBER) {
                        /* There is no distinction between double, int, and long
                         * in the token. However, the reader only allows
                         * reading of double, int, or long. Thus the best
                         * we can do is read a string and then try and
                         * convert it from smallest size to largest.
                         */
                        String item = json.nextString();
                        BigDecimal decimal = new BigDecimal(item);

                        try {
                            object.put(name, decimal.intValueExact());
                        } catch (ArithmeticException e) {
                            try {
                                object.put(name, decimal.longValueExact());
                            } catch (ArithmeticException eprime) {
                                object.put(name, decimal.doubleValue());
                            }
                        }
                    } else if (token == JsonToken.STRING){
                        object.put(name, json.nextString());
                    }
                } catch (JSONException e) {
                    running = false;
                }
            }
        }

        return null;
    }

    private JSONArray readArray(JsonReader json) throws IOException
    {
        JSONArray object = new JSONArray();
        boolean running = true;

        while (running) {
            JsonToken token = json.peek();

            switch (token) {
                case BEGIN_ARRAY:
                    json.beginArray();

                    JSONArray list = readArray(json);
                    if (null != list) {
                        object.put(list);
                    }

                    continue;
                case BEGIN_OBJECT:
                    json.beginObject();

                   JSONObject o = readObject(json);
                    if (null != o) {
                        object.put(o);
                    }

                    continue;
                case END_ARRAY:
                    json.endArray();
                    return object;
                case END_DOCUMENT:
                    return object;
                case NULL:
                    json.nextNull();
                    object.put(JSONObject.NULL);
                    continue;
                default:
                    break;
            }

            if (json.hasNext()) {
                object.put(json.nextString());
            }
        }

        return null;
    }
}
