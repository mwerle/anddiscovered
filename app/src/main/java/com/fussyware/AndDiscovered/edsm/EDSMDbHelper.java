package com.fussyware.AndDiscovered.edsm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.fussyware.AndDiscovered.edutils.Position;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wes on 9/28/15.
 */
public class EDSMDbHelper extends SQLiteAssetHelper
{
    private static final String LOG_NAME = EDSMDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "edsm.db";

    private Date lastDistanceCheck;
    private Date lastSystemCheck;

    public final Object mutex = new Object();

    private static EDSMDbHelper singleton;

    public EDSMDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();
    }

    public static synchronized void init(Context context)
    {
        if (singleton == null) {
            singleton = new EDSMDbHelper(context);
        }
    }

    public static synchronized EDSMDbHelper getInstance()
    {
        return singleton;
    }

    public boolean containsSystem(String system)
    {
        String[] columns = {
                EDSMDbContract.Systems.COLUMN_NAME_SYSTEM,
        };

        String selection = EDSMDbContract.Systems.COLUMN_NAME_SYSTEM + "=?";
        String[] selectionArgs = { system };

        Cursor cursor = getReadableDatabase().query(EDSMDbContract.Systems.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            cursor.moveToFirst();
            return !cursor.isAfterLast();
        } finally {
            cursor.close();
        }
    }

    public EDSMSystemInfo createSystem(String system, Position xyz)
    {
        if ((system == null) || system.isEmpty()) {
            throw new IllegalArgumentException("Invalid system name specified.");
        }

        EDSMSystemInfo systemInfo = getSystem(system);
        if (systemInfo == null) {
            ContentValues values = new ContentValues();
            values.put(EDSMDbContract.Systems.COLUMN_NAME_SYSTEM, system);

            if (xyz == null) {
                values.putNull(EDSMDbContract.Systems.COLUMN_NAME_XCOORD);
                values.putNull(EDSMDbContract.Systems.COLUMN_NAME_YCOORD);
                values.putNull(EDSMDbContract.Systems.COLUMN_NAME_ZCOORD);
            } else {
                values.put(EDSMDbContract.Systems.COLUMN_NAME_XCOORD, xyz.x);
                values.put(EDSMDbContract.Systems.COLUMN_NAME_YCOORD, xyz.y);
                values.put(EDSMDbContract.Systems.COLUMN_NAME_ZCOORD, xyz.z);
            }

            long id = getWritableDatabase().insert(EDSMDbContract.Systems.TABLE_NAME,
                                                   null,
                                                   values);

            systemInfo = new EDSMSystemInfo(getWritableDatabase(), id, system, xyz);
        }

        return systemInfo;
    }

    public EDSMSystemInfo getSystem(String system)
    {
        String[] columns = {
                EDSMDbContract.Systems._ID,
                EDSMDbContract.Systems.COLUMN_NAME_SYSTEM,
                EDSMDbContract.Systems.COLUMN_NAME_XCOORD,
                EDSMDbContract.Systems.COLUMN_NAME_YCOORD,
                EDSMDbContract.Systems.COLUMN_NAME_ZCOORD,
        };

        String selection = EDSMDbContract.Systems.COLUMN_NAME_SYSTEM + "=?";
        String[] selectionArgs = { system };

        Cursor cursor = getReadableDatabase().query(EDSMDbContract.Systems.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            EDSMSystemInfo info = null;

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                Position xyz = null;

                if (!cursor.isNull(2) && !cursor.isNull(3) && !cursor.isNull(4)) {
                    xyz = new Position(cursor.getDouble(2), cursor.getDouble(3), cursor.getDouble(4));
                }

                info = new EDSMSystemInfo(getWritableDatabase(),
                                          cursor.getLong(0),
                                          cursor.getString(1),
                                          xyz);
            }

            return info;
        } finally {
            cursor.close();
        }
    }

    public EDSMDistanceInfo createDistance(String from, String to, Double distance)
    {
        if ((from == null) || from.isEmpty()) {
            throw new IllegalArgumentException("Invalid beginning system specified.");
        }

        if ((to == null) || to.isEmpty()){
            throw new IllegalArgumentException("Invalid end system specified.");
        }

        if (distance == null) {
            distance = 0.0;
        }

        distance = (distance < 0.0) ? -distance : distance;

        ContentValues values = new ContentValues();
        values.put(EDSMDbContract.Distances.COLUMN_NAME_FROM, from);
        values.put(EDSMDbContract.Distances.COLUMN_NAME_DISTANCE, distance);
        values.put(EDSMDbContract.Distances.COLUMN_NAME_TO, to);

        long id = getWritableDatabase().insert(EDSMDbContract.Distances.TABLE_NAME,
                                               null,
                                               values);

        return new EDSMDistanceInfo(getWritableDatabase(),
                                    id,
                                    from,
                                    to,
                                    distance);
    }

    public EDSMDistanceInfo getDistance(String s1, String s2)
    {
        if ((s1 == null) || (s2 == null)) {
            return null;
        }

        String[] columns = {
                EDSMDbContract.Distances._ID,
                EDSMDbContract.Distances.COLUMN_NAME_FROM,
                EDSMDbContract.Distances.COLUMN_NAME_TO,
                EDSMDbContract.Distances.COLUMN_NAME_DISTANCE,
        };

        String selection = EDSMDbContract.Distances.COLUMN_NAME_FROM + " IN (?,?) AND " +
                           EDSMDbContract.Distances.COLUMN_NAME_TO + " IN (?,?)";
        String[] selectionArgs = { s1, s2, s1, s2 };

        Cursor cursor = getReadableDatabase().query(EDSMDbContract.Distances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            cursor.moveToFirst();
            return cursor.isAfterLast() ? null : new EDSMDistanceInfo(getWritableDatabase(),
                                                                      cursor.getLong(0),
                                                                      cursor.getString(1),
                                                                      cursor.getString(2),
                                                                      cursor.getDouble(3));
        } finally {
            cursor.close();
        }
    }

    public List<EDSMDistanceInfo> getDistances(String system, boolean duplicatesAllowed)
    {
        ArrayList<EDSMDistanceInfo> list = new ArrayList<EDSMDistanceInfo>();

        String[] columns = {
                EDSMDbContract.Distances._ID,
                EDSMDbContract.Distances.COLUMN_NAME_FROM,
                EDSMDbContract.Distances.COLUMN_NAME_TO,
                EDSMDbContract.Distances.COLUMN_NAME_DISTANCE,
        };

        String selection = EDSMDbContract.Distances.COLUMN_NAME_FROM + " =? OR " +
                           EDSMDbContract.Distances.COLUMN_NAME_TO + " =?";
        String[] selectionArgs = { system, system };

        Date t0 = new Date();
        Cursor cursor = getReadableDatabase().query(EDSMDbContract.Distances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);
        Log.d(LOG_NAME, "1: " + (new Date().getTime() - t0.getTime()));

        try {
            t0 = new Date();
            while (cursor.moveToNext()) {
                String s1 = cursor.getString(1);
                String s2 = cursor.getString(2);

                EDSMDistanceInfo info;

                if (system.equalsIgnoreCase(s1)) {
                    info = new EDSMDistanceInfo(getWritableDatabase(),
                                                cursor.getLong(0),
                                                s1,
                                                s2,
                                                cursor.getDouble(3));
                } else {
                    info = new EDSMDistanceInfo(getWritableDatabase(),
                                                cursor.getLong(0),
                                                s2,
                                                s1,
                                                cursor.getDouble(3));
                }

                if (duplicatesAllowed) {
                    list.add(info);
                } else if (!list.contains(info)) {
                    list.add(info);
                }
            }
            Log.d(LOG_NAME, "3: " + (new Date().getTime() - t0.getTime()));
        } finally {
            cursor.close();
        }

        return list;
    }

    public Date getLastDistanceCheck()
    {
        synchronized (mutex) {
            if (lastDistanceCheck == null) {
                String[] columns = {
                        EDSMDbContract.DbInfo.COLUMN_NAME_CONFIG,
                        EDSMDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = EDSMDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {EDSMDbContract.DbInfo.CONFIG_NAME_LAST_EDSM_DISTANCE_CHECK};

                Cursor c = getReadableDatabase().query(EDSMDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();
                    lastDistanceCheck = new Date(Long.valueOf(c.getString(1)));
                } catch (Exception e) {
                    lastDistanceCheck = new Date(0);
                } finally {
                    c.close();
                }
            }

            return lastDistanceCheck;
        }
    }

    public void setLastDistanceCheck(Date newTime)
    {
        String selection = EDSMDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
        String[] selectionArgs = {EDSMDbContract.DbInfo.CONFIG_NAME_LAST_EDSM_DISTANCE_CHECK};

        ContentValues values = new ContentValues();
        values.put(EDSMDbContract.DbInfo.COLUMN_NAME_VALUE, Long.toString(newTime.getTime()));

        getWritableDatabase().update(EDSMDbContract.DbInfo.TABLE_NAME,
                                     values,
                                     selection,
                                     selectionArgs);

        synchronized (mutex) {
            lastDistanceCheck = newTime;
        }
    }

    public Date getLastSystemCheck()
    {
        synchronized (mutex) {
            if (lastSystemCheck == null) {
                String[] columns = {
                        EDSMDbContract.DbInfo.COLUMN_NAME_CONFIG,
                        EDSMDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = EDSMDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {EDSMDbContract.DbInfo.CONFIG_NAME_LAST_EDSM_SYSTEM_CHECK};

                Cursor c = getReadableDatabase().query(EDSMDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();
                    lastSystemCheck = new Date(Long.valueOf(c.getString(1)));
                } catch (Exception e) {
                    lastSystemCheck = new Date(0);
                } finally {
                    c.close();
                }
            }

            return lastSystemCheck;
        }
    }

    public void setLastSystemCheck(Date newTime)
    {
        String selection = EDSMDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
        String[] selectionArgs = {EDSMDbContract.DbInfo.CONFIG_NAME_LAST_EDSM_SYSTEM_CHECK};

        ContentValues values = new ContentValues();
        values.put(EDSMDbContract.DbInfo.COLUMN_NAME_VALUE, Long.toString(newTime.getTime()));

        getWritableDatabase().update(EDSMDbContract.DbInfo.TABLE_NAME,
                                     values,
                                     selection,
                                     selectionArgs);

        synchronized (mutex) {
            lastSystemCheck = newTime;
        }
    }

    public List<EDSMSystemInfo> getSystemSphere(Position origin, double radius, int sortBy)
    {
        Position prime1 = new Position((origin.x + radius),
                                       (origin.y + radius),
                                       (origin.z + radius));
        Position prime2 = new Position((origin.x - radius),
                                       (origin.y - radius),
                                       (origin.z - radius));

        String[] columns = {
                EDSMDbContract.Systems._ID,
                EDSMDbContract.Systems.COLUMN_NAME_SYSTEM,
                EDSMDbContract.Systems.COLUMN_NAME_XCOORD,
                EDSMDbContract.Systems.COLUMN_NAME_YCOORD,
                EDSMDbContract.Systems.COLUMN_NAME_ZCOORD,
        };

        String selection = "";

        selection += EDSMDbContract.Systems.COLUMN_NAME_XCOORD + " >=? AND ";
        selection += EDSMDbContract.Systems.COLUMN_NAME_XCOORD + " <=? AND ";

        selection += EDSMDbContract.Systems.COLUMN_NAME_YCOORD + " >=? AND ";
        selection += EDSMDbContract.Systems.COLUMN_NAME_YCOORD + " <=? AND ";

        selection += EDSMDbContract.Systems.COLUMN_NAME_ZCOORD + " >=? AND ";
        selection += EDSMDbContract.Systems.COLUMN_NAME_ZCOORD + " <=?";

        String[] selectionArgs = {
                Double.toString(prime2.x),
                Double.toString(prime1.x),

                Double.toString(prime2.y),
                Double.toString(prime1.y),

                Double.toString(prime2.z),
                Double.toString(prime1.z),
        };

        String orderBy;

        switch (sortBy) {
            case EDSMStarCoordinator.SORT_BY_X:
                orderBy = EDSMDbContract.Systems.COLUMN_NAME_XCOORD + " DESC";
                break;
            case EDSMStarCoordinator.SORT_BY_Y:
                orderBy = EDSMDbContract.Systems.COLUMN_NAME_YCOORD + " DESC";
                break;
            case EDSMStarCoordinator.SORT_BY_Z:
                orderBy = EDSMDbContract.Systems.COLUMN_NAME_ZCOORD + " DESC";
                break;
            default:
                orderBy = null;
                break;
        }

        Cursor cursor = getReadableDatabase().query(EDSMDbContract.Systems.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    orderBy);

        ArrayList<EDSMSystemInfo> list = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                Position xyz = null;

                if (!cursor.isNull(2) && !cursor.isNull(3) && !cursor.isNull(4)) {
                    xyz = new Position(cursor.getDouble(2), cursor.getDouble(3), cursor.getDouble(4));
                }

                EDSMSystemInfo info = new EDSMSystemInfo(getWritableDatabase(),
                                                         cursor.getLong(0),
                                                         cursor.getString(1),
                                                         xyz);

                list.add(info);
            }
        } finally {
            cursor.close();
        }

        return list;
    }
}
