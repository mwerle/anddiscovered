package com.fussyware.AndDiscovered.edsm;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

/**
 * Created by wboyd on 7/7/15.
 */
public class EDSMSystemInfo extends SystemInfo
{
    private final SQLiteDatabase db;
    private final long id;

    EDSMSystemInfo(SQLiteDatabase db, long id, String system, Position xyz)
    {
        super(system, xyz);

        this.db = db;
        this.id = id;
    }

    @Override
    public void setPosition(Position xyz)
    {
        if ((xyz != null) && (getPosition() != xyz)) {
            super.setPosition(xyz);

            String selection = EDSMDbContract.Systems._ID + "=?";
            String[] selectionArgs = {Long.toString(id)};

            ContentValues values = new ContentValues();

            values.put(EDSMDbContract.Systems.COLUMN_NAME_XCOORD, xyz.x);
            values.put(EDSMDbContract.Systems.COLUMN_NAME_YCOORD, xyz.y);
            values.put(EDSMDbContract.Systems.COLUMN_NAME_ZCOORD, xyz.z);

            db.update(EDSMDbContract.Systems.TABLE_NAME,
                      values,
                      selection,
                      selectionArgs);
        }
    }
}
