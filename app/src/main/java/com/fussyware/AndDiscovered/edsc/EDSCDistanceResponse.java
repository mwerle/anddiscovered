package com.fussyware.AndDiscovered.edsc;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wes on 6/16/15.
 */
public class EDSCDistanceResponse
{
    private static final String LOG_NAME = EDSCDistanceResponse.class.getSimpleName();

    private String system;
    private ArrayList<EDSCDistance> distanceList = new ArrayList<EDSCDistance>();

    EDSCDistanceResponse(JSONObject object) throws JSONException
    {
        this("", object);
    }

    EDSCDistanceResponse(String system, JSONObject object) throws JSONException
    {
        Date t0 = new Date();

        JSONObject json = object.getJSONObject("d");

        if (null == system) {
            this.system = "";
        } else {
            this.system = system;
        }

        JSONArray edscList = json.getJSONArray("distances");

        for (int i = 0; i < edscList.length(); i++) {
            JSONObject o = edscList.getJSONObject(i);
            String name = o.getString("name");
            JSONArray refs = o.getJSONArray("refs");

            if (!this.system.isEmpty()) {
                if (name.equals(this.system)) {
                    // All refs are distances to System. There may be multiple
                    for (int j = 0; j < refs.length(); j++) {
                        JSONObject o2 = refs.getJSONObject(j);
                        String to = o2.getString("name");
                        double distance = o2.getDouble("dist");

                        distanceList.add(new EDSCDistance(this.system, to, distance));
                    }
                } else {
                    // All refs are *the* System. There will be one or none.
                    if (refs.length() > 0) {
                        JSONObject o2 = refs.getJSONObject(0);
                        double distance = o2.getDouble("dist");

                        distanceList.add(new EDSCDistance(this.system, name, distance));
                    }
                }
            } else {
                for (int j = 0; j < refs.length(); j++) {
                    JSONObject o2 = refs.getJSONObject(j);
                    String to = o2.getString("name");
                    double distance = o2.getDouble("dist");

                    distanceList.add(new EDSCDistance(name, to, distance));
                }
            }
        }

        Log.d(LOG_NAME, "JSON distance object parse time. ["  + (new Date().getTime() - t0.getTime()) + "]");
    }

    public String getSystem()
    {
        return system;
    }

    public List<EDSCDistance> getDistanceList()
    {
        return distanceList;
    }

    public double getDistanceTo(String to)
    {
        for (EDSCDistance distance : distanceList) {
            if (distance.getToSystem().equals(to)) {
                return distance.getDistance();
            }
        }

        return 0.0;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("EDSCDistanceResponse { System [");
        sb.append(system);
        sb.append("], Distances [");
        sb.append(distanceList.toString());
        sb.append("]}");

        return sb.toString();
    }
}
