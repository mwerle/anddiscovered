package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.AsteroidBody;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.CelestialAsteroidEditDialogFragment;
import com.fussyware.AndDiscovered.dialog.CelestialCommonEditDialogFragment;
import com.fussyware.AndDiscovered.dialog.CelestialPlanetEditDialogFragment;
import com.fussyware.AndDiscovered.dialog.CelestialStarEditDialogFragment;
import com.fussyware.AndDiscovered.fragment.CelestialAsteroidDetailsFragment;
import com.fussyware.AndDiscovered.fragment.CelestialInfoCommonDetailsFragment;
import com.fussyware.AndDiscovered.fragment.CelestialInfoHeaderFragment;
import com.fussyware.AndDiscovered.fragment.CelestialPlanetDetailsFragment;
import com.fussyware.AndDiscovered.fragment.CelestialStarDetailsFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;

import java.security.InvalidParameterException;

/**
 * Created by wes on 9/30/15.
 */
public class CelestialInfoActivity
        extends BaseActivity
        implements CelestialCommonEditDialogFragment.OnDoneListener,
                   CelestialStarEditDialogFragment.OnDoneListener,
                   CelestialPlanetEditDialogFragment.OnDoneListener,
                   CelestialAsteroidEditDialogFragment.OnDoneListener
{
    private CelestialBody celestialBody;

    private TitleInfoFragment titleFragment;
    private CelestialInfoHeaderFragment headerFragment;
    private CelestialInfoCommonDetailsFragment commonFragment;
    private CelestialPlanetDetailsFragment planetFragment;
    private CelestialStarDetailsFragment starFragment;
    private CelestialAsteroidDetailsFragment asteroidFragment;

    private boolean modified;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.celestial_info_activity_title);
        setContentView(R.layout.celestial_info_activity_layout);
        setDrawerIndicatorEnabled(false);

        titleFragment    = (TitleInfoFragment) getFragmentManager().findFragmentById(R.id.title_info_fragment);
        headerFragment   = (CelestialInfoHeaderFragment) getFragmentManager().findFragmentById(R.id.celestial_info_header_fragment);
        commonFragment   = (CelestialInfoCommonDetailsFragment) getFragmentManager().findFragmentById(R.id.cb_common_details_fragment);
        planetFragment   = (CelestialPlanetDetailsFragment) getFragmentManager().findFragmentById(R.id.cb_planet_details_fragment);
        starFragment     = (CelestialStarDetailsFragment) getFragmentManager().findFragmentById(R.id.cb_star_details_fragment);
        asteroidFragment = (CelestialAsteroidDetailsFragment) getFragmentManager().findFragmentById(R.id.cb_asteroid_details_fragment);

        if (savedInstanceState == null) {
            celestialBody = getIntent().getParcelableExtra("celestial");

            titleFragment.setSystem(celestialBody.getSystem());
            headerFragment.setCelestialBody(celestialBody);
            commonFragment.setCelestialBody(celestialBody);

            switch (celestialBody.getSatelliteCategory()) {
                case Star:
                    starFragment.setCelestialBody((StarBody) celestialBody);
                    break;
                case Planet:
                    planetFragment.setCelestialBody((PlanetBody) celestialBody);
                    break;
                case Asteroid:
                    asteroidFragment.setCelestialBody((AsteroidBody) celestialBody);
                    break;
                case Unknown:
                    throw new InvalidParameterException("A valid Satellite category must be provided.");
            }
        } else {
            celestialBody = savedInstanceState.getParcelable("celestial");
            modified = savedInstanceState.getBoolean("modified");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial", celestialBody);
        outState.putBoolean("modified", modified);
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            if (modified) {
                Intent intent = new Intent();
                intent.putExtra("modified", modified);

                setResult(Activity.RESULT_OK, intent);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        Intent intent = new Intent(getApplicationContext(), SystemInfoActivity.class);
        intent.putExtra("system", celestialBody.getSystem());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);

        return true;
    }

    @Override
    protected void onNewSystem(SystemInfo system)
    {
        Intent intent = new Intent(getApplicationContext(), SystemInfoActivity.class);
        intent.putExtra("system", system);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.celestial_info_fragment.celestial_name_dialog:
                if (headerFragment != null) {
                    headerFragment.onDialogDone(text);
                    modified = true;
                }
                break;
            default:
                super.onDone(tag, text);
                break;
        }
    }

    @Override
    public void onDone(String tag, Bundle changedItems)
    {
        switch (tag) {
            case FragmentTag.celestial_info_fragment.celestial_common_dialog:
                commonFragment.onDone(changedItems);
                break;
            case FragmentTag.celestial_info_fragment.celestial_star_dialog:
                starFragment.onDone(changedItems);
                break;
            case FragmentTag.celestial_info_fragment.celestial_planet_dialog:
                planetFragment.onDone(changedItems);
                break;
            case FragmentTag.celestial_info_fragment.celestial_asteroid_dialog:
                asteroidFragment.onDone(changedItems);
                break;
            default:
                break;
        }
    }
}
