package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 8/30/15.
 */
public enum PlanetType
{
    Metal_Rich(1, "Metal Rich"),
    High_Metallic(2, "High Metallic"),
    Water_World(3, "Water World"),
    Earth_Like(4, "Earth Like"),
    Ammonia(5, "Ammonia"),
    Gas_Giant_I(6, "Gas Giant I"),
    Gas_Giant_II(7, "Gas Giant II"),
    Gas_Giant_III(8, "Gas Giant III"),
    Gas_Giant_IV(9, "Gas Giant IV"),
    Gas_Giant_V(10, "Gas Giant V"),
    Gas_Giant_Ammonia(14, "Gas Giant with Ammonia"),
    Gas_Giant_Water(15, "Gas Giant with Water"),
    Rocky(11, "Rocky"),
    Ice(12, "Ice"),
    // TODO: Convert Unknown from 13 to 0xFFFFFFFF.
    Unknown(13, "NA");

    public int value;
    // TODO: Get rid of name and make the application use R.string.
    public String name;

    PlanetType(int value, String name)
    {
        this.value = value;
        this.name = name;
    }

    public static PlanetType getPlanetType(String value)
    {
        for (PlanetType t : PlanetType.values()) {
            if (t.name.equals(value)) {
                return t;
            }
        }

        return PlanetType.Unknown;
    }

    public static PlanetType getPlanetType(int value)
    {
        for (PlanetType t : PlanetType.values()) {
            if (t.value == value) {
                return t;
            }
        }

        return PlanetType.Unknown;
    }

    public int value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return name;
    }

}
