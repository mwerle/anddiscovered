package com.fussyware.AndDiscovered.edsc;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;

import com.fussyware.AndDiscovered.StarCoordinator;
import com.fussyware.AndDiscovered.eddatabase.CmdrDistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by wboyd on 7/6/15.
 */
public class EDSCStarCoordinator extends StarCoordinator
{
    private static final String LOG_NAME = EDSCStarCoordinator.class.getSimpleName();

    private EDSCDbHelper edscDbHelper;

    private static EDSCStarCoordinator singleton = null;

    public EDSCStarCoordinator(Context context)
    {
        super(context);

        edscDbHelper = EDSCDbHelper.getInstance(context.getApplicationContext());
    }

    public static EDSCStarCoordinator getInstance(Context context)
    {
        if (singleton == null) {
            singleton = new EDSCStarCoordinator(context);
        }

        return singleton;
    }

    @Override
    public Intent getIntentService(Context context)
    {
        return new Intent(context, EDSCStarCoordinator.EDSCIntentService.class);
    }

    @Override
    public SystemInfo getSystem(String system)
    {
        return edscDbHelper.getSystem(system);
    }

    @Override
    public boolean containsSystem(String system)
    {
        return edscDbHelper.containsSystem(system);
    }

    @Override
    public DistanceInfo getDistance(String s1, String s2)
    {
        return edscDbHelper.getDistance(s1, s2);
    }

    @Override
    public List<? extends DistanceInfo> getDistances(String system, boolean duplicatesAllowed)
    {
        return edscDbHelper.getDistances(system, duplicatesAllowed);
    }

    public static class EDSCIntentService extends IntentService
    {
        private AtomicBoolean running = new AtomicBoolean(false);

        private final Lock mutex = new ReentrantLock();
        private final Condition condition = mutex.newCondition();

        private EDSCStarCoordinator starCoordinator;

        public EDSCIntentService()
        {
            super("EDSCIntentService");
        }

        @Override
        public void onCreate()
        {
            super.onCreate();
            starCoordinator = EDSCStarCoordinator.getInstance(this);
        }

        @Override
        protected void onHandleIntent(Intent intent)
        {
            running.set(true);

            while (running.get()) {
                Log.d(LOG_NAME, "1");
                uploadCmdrDistances();
                Log.d(LOG_NAME, "2");

                if (downloadDistances()) {
                    Log.d(LOG_NAME, "3");
                    starCoordinator.sendDistanceUpdatedIntent();
                    Log.d(LOG_NAME, "4");
                }
                Log.d(LOG_NAME, "5");

                trilatCmdrSystems();
                Log.d(LOG_NAME, "6");

                if (downloadSystems()) {
                    Log.d(LOG_NAME, "7");
                    starCoordinator.sendSystemUpdatedIntent();
                    Log.d(LOG_NAME, "8");
                }

                Log.d(LOG_NAME, "9");

                try {
                    mutex.lock();
                    condition.await(15, TimeUnit.MINUTES);
                } catch (InterruptedException ignored) {
                } finally {
                    mutex.unlock();
                }
            }
        }

        @Override
        public void onDestroy()
        {
            try {
                mutex.lock();
                running.set(false);
                condition.signal();
            } finally {
                mutex.unlock();
            }

            super.onDestroy();
        }

        private void trilatCmdrSystems()
        {
            List<CmdrSystemInfo> list = starCoordinator.cmdrDbHelper.getSystemsWithNoPosition();
            HashMap<CmdrSystemInfo, ArrayList<Pair<String, Double>>> map = new HashMap<CmdrSystemInfo, ArrayList<Pair<String, Double>>>();
            HashMap<CmdrSystemInfo, Future<EDSCSubmitResponse>> futureMap = new HashMap<CmdrSystemInfo, Future<EDSCSubmitResponse>>();

            for (CmdrSystemInfo info : list) {
                ArrayList<Pair<String, Double>> al = new ArrayList<Pair<String, Double>>();
                List<? extends DistanceInfo> li;

                li = starCoordinator.edscDbHelper.getDistances(info.getSystem(), false);
                for (DistanceInfo cdi : li) {
                    if (cdi.getDistance() != 0.0) {
                        al.add(new Pair<String, Double>(cdi.getSecondSystem(), cdi.getDistance()));
                    }
                }

                li = starCoordinator.cmdrDbHelper.getDistances(info.getSystem(), false);
                for (DistanceInfo cdi : li) {
                    if (cdi.getSecondSystem() != null) {
                        Pair<String, Double> pair = new Pair<String, Double>(cdi.getSecondSystem(),
                                                                             cdi.getDistance());
                        if ((cdi.getDistance() != 0.0) && !al.contains(pair)) {
                            al.add(pair);
                        }
                    }
                }

                map.put(info, al);
            }

            String cmdrName = starCoordinator.cmdrDbHelper.getCmdrName();

            for (CmdrSystemInfo key : map.keySet()) {
                int numSystems = key.getMinSystemsRequiredForTrilat();

                if (map.get(key).size() >= numSystems) {
                    EDSCSubmitDistances distances = new EDSCSubmitDistances(key.getSystem(),
                                                                            map.get(key));

                    distances.setTest(true);
                    distances.setCommander(cmdrName);

                    Log.d(LOG_NAME, "Submitting: " + key.getSystem() + ", num distances: " + Integer.toString(numSystems));
                    Future<EDSCSubmitResponse> future = EDSCConnection.submitDistances(distances);
                    if (future != null) {
                        futureMap.put(key, future);
                    }
                }
            }

            for (CmdrSystemInfo key : futureMap.keySet()) {
                EDSCSubmitResponse response;
                try {
                    response = futureMap.get(key).get();

                    if (null != response) Log.d(LOG_NAME, response.toString());
                    if ((null != response) && !response.isTrilatPerformed()) {
                        // We only really care about the fact we did NOT update the trilat. If we did
                        // update it then the pull from the systems master database will udpate us so
                        // all is good.

                        key.setMinSystemsRequiredForTrilat(response.getDistancesProcessed() + 1);
                    }
                } catch (InterruptedException e) {
                    Log.e(LOG_NAME, "Distance submission was interrupted by the VM.");
                } catch (ExecutionException e) {
                    Log.e(LOG_NAME, "Distance submission failed executing. " + e.getMessage());
                }
            }
        }

        private void uploadCmdrDistances()
        {
            List<CmdrDistanceInfo> list = starCoordinator.cmdrDbHelper.getUploadRequiredDistances();
            HashMap<String, ArrayList<Pair<String, Double>>> map = new HashMap<String, ArrayList<Pair<String, Double>>>();
            HashMap<String, Future<EDSCSubmitResponse>> futureMap = new HashMap<String, Future<EDSCSubmitResponse>>();

            for (CmdrDistanceInfo info : list) {
                if (map.containsKey(info.getFirstSystem())) {
                    ArrayList<Pair<String, Double>> al = map.get(info.getFirstSystem());
                    al.add(new Pair<String, Double>(info.getSecondSystem(), info.getDistance()));
                } else {
                    ArrayList<Pair<String, Double>> al = new ArrayList<Pair<String, Double>>();
                    al.add(new Pair<String, Double>(info.getSecondSystem(), info.getDistance()));
                    map.put(info.getFirstSystem(), al);
                }
            }

            String cmdrName = starCoordinator.cmdrDbHelper.getCmdrName();

            for (String key : map.keySet()) {
                EDSCSubmitDistances distances = new EDSCSubmitDistances(key, map.get(key));

                //distances.setTest(true);
                distances.setCommander(cmdrName);

                Future<EDSCSubmitResponse> future = EDSCConnection.submitDistances(distances);
                if (future != null) {
                    futureMap.put(key, future);
                }
            }

            for (Future<EDSCSubmitResponse> future : futureMap.values()) {
                try {
                    future.get();
                } catch (InterruptedException e) {
                    Log.e(LOG_NAME, "Distance submission was interrupted by the VM.");
                } catch (ExecutionException e) {
                    Log.e(LOG_NAME, "Distance submission failed executing. " + e.getMessage());
                }
            }

            try {
                starCoordinator.cmdrDbHelper.getWritableDatabase().beginTransaction();
                for (CmdrDistanceInfo info : list) {
                    info.setRemoteUpdated(true);
                }
                starCoordinator.cmdrDbHelper.getWritableDatabase().setTransactionSuccessful();
            } finally {
                starCoordinator.cmdrDbHelper.getWritableDatabase().endTransaction();
            }
        }

        private boolean downloadSystems()
        {
            boolean updated = false;

            EDSCRequest req = new EDSCRequest();
            //req.setTest(true);
            req.setCr(0);
            req.setKnownStatus(0);
            req.setOutputMode(2);
            req.setDate(starCoordinator.edscDbHelper.getLastSystemCheck());

            Log.d(LOG_NAME, "Start getting systems from Timer.");
            List<EDSCSystem> response = EDSCConnection.getSystems(req);
            Log.d(LOG_NAME, "Got " + Integer.toString(response.size()) + " systems.");

            starCoordinator.edscDbHelper.setLastSystemCheck(new Date());

            for (EDSCSystem system : response) {
                EDSCSystemInfo systemInfo = starCoordinator.edscDbHelper.getSystem(system.getSystem());

                if (systemInfo == null) {
                    starCoordinator.edscDbHelper.createSystem(system.getSystem(), system.getPosition());
                } else {
                    systemInfo.setPosition(system.getPosition());
                }

                updated = true;
            }

            return updated;
        }

        private boolean downloadDistances()
        {
            boolean updated = false;

            EDSCRequest req = new EDSCRequest();
            //req.setTest(true);
            req.setCr(0);
            req.setKnownStatus(0);
            req.setOutputMode(1);
            req.setDate(starCoordinator.edscDbHelper.getLastDistanceCheck());

            Log.d(LOG_NAME, "Start getting distances from Timer.");
            EDSCDistanceResponse response = EDSCConnection.getDistances(req);
            Log.d(LOG_NAME, "Finished getting distances from Timer.");

            starCoordinator.edscDbHelper.setLastDistanceCheck(new Date());

            if (null != response) {
                List<EDSCDistance> distanceList = response.getDistanceList();

                Log.d(LOG_NAME, "Got " + Integer.toString(distanceList.size()) + " distances.");
                try {
                    starCoordinator.edscDbHelper.getWritableDatabase().beginTransaction();
                    for (EDSCDistance distance : distanceList) {
                        String from = distance.getFromSystem();
                        String to = distance.getToSystem();
                        double dist = distance.getDistance();

                        EDSCDistanceInfo distanceInfo = starCoordinator.edscDbHelper.getDistance(from, to);

                        if (distanceInfo == null) {
                            starCoordinator.edscDbHelper.createDistance(from, to, dist);
                            updated = true;
                        } else if (distanceInfo.getDistance() != dist) {
                            distanceInfo.setDistance(dist);
                            updated = true;
                        }
                    }

                    Log.d(LOG_NAME, "Successfully updated database with all known distances.");
                    starCoordinator.edscDbHelper.getWritableDatabase().setTransactionSuccessful();
                } finally {
                    starCoordinator.edscDbHelper.getWritableDatabase().endTransaction();
                }
            }

            Log.d(LOG_NAME, "Update distances: " + Boolean.toString(updated));
            if (updated) {
                updated = false;

                List<CmdrDistanceInfo> list = starCoordinator.cmdrDbHelper.getEmptyDistances();

                try {
                    starCoordinator.cmdrDbHelper.getWritableDatabase().beginTransaction();
                    for (CmdrDistanceInfo info : list) {
                        if (info.getSecondSystem() != null) {
                            EDSCDistanceInfo distanceInfo = starCoordinator.edscDbHelper.getDistance(
                                    info.getFirstSystem(),
                                    info.getSecondSystem());

                            if (distanceInfo != null) {
                                info.setDistance(distanceInfo.getDistance());
                                info.setRemoteUpdated(true);

                                updated = true;
                            }
                        }
                    }
                    starCoordinator.cmdrDbHelper.getWritableDatabase().setTransactionSuccessful();
                } finally {
                    starCoordinator.cmdrDbHelper.getWritableDatabase().endTransaction();
                }
            }

            Log.d(LOG_NAME, "Finished with distance update.");

            return updated;
        }
    }
}
