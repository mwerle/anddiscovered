package com.fussyware.AndDiscovered.eddiscovery;

/**
 * Created by wes on 6/14/15.
 */
public interface DiscoveryListener
{
    public void discoveryAnnounce(AnnounceMessage message);
    public void discoveryQuery(QueryMessage message);
}
