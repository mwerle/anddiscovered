package com.fussyware.AndDiscovered.edsm;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.util.Pair;

import com.fussyware.AndDiscovered.StarCoordinator;
import com.fussyware.AndDiscovered.TrilatResponse;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrDistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by wboyd on 7/6/15.
 */
public class EDSMStarCoordinator extends StarCoordinator
{
    private static final String LOG_NAME = EDSMStarCoordinator.class.getSimpleName();
    private static final int SUBMIT_DEBOUNCE_SECONDS = 30;
    private static final HashMap<String, EDSMFuture> trilatMap = new HashMap<>();
    private static final ScheduledThreadPoolExecutor executorService = new ScheduledThreadPoolExecutor(20);

    private static EDSMStarCoordinator singleton;
    private final EDSMDbHelper dbHelper;

    static {
        executorService.setKeepAliveTime(60, TimeUnit.SECONDS);
        executorService.allowCoreThreadTimeOut(true);
        //executorService.setRemoveOnCancelPolicy(true);
    }

    public EDSMStarCoordinator(Context context)
    {
        super(context);

        EDSMDbHelper.init(context);
        dbHelper = EDSMDbHelper.getInstance();
    }

    public static EDSMStarCoordinator getInstance(Context context)
    {
        if (singleton == null) {
            singleton = new EDSMStarCoordinator(context);
        }

        return singleton;
    }

    @Override
    public Intent getIntentService(Context context)
    {
        return new Intent(context, EDSMStarCoordinator.TempleIntentService.class);
    }

    @Override
    public SystemInfo getSystem(String system)
    {
        return dbHelper.getSystem(system);
    }

    @Override
    public boolean containsSystem(String system)
    {
        return dbHelper.containsSystem(system);
    }

    @Override
    public DistanceInfo getDistance(String s1, String s2)
    {
        return dbHelper.getDistance(s1, s2);
    }

    @Override
    public List<? extends SystemInfo> getSystemSphere(Position origin, double radius, int sortBy)
    {
        return dbHelper.getSystemSphere(origin, radius, sortBy);
    }

    @Override
    public Future<TrilatResponse> trilatSystem(String system, List<? extends DistanceInfo> distanceInfos, boolean debounce) {

        CmdrSystemInfo systemInfo = cmdrDbHelper.getSystem(system);
        int numSystems = systemInfo.getMinSystemsRequiredForTrilat();

        if (systemInfo.getPosition() != null) return new StaticTrilatResponseFuture(systemInfo.getPosition(), -1);
        if (distanceInfos.size() < numSystems) return new StaticTrilatResponseFuture();

        ArrayList<Pair<String, Double>> al = new ArrayList<Pair<String, Double>>();

        String cmdrName = cmdrDbHelper.getCmdrName();

        for (DistanceInfo info : distanceInfos) {
            al.add(new Pair<>(info.getSecondSystem(), info.getDistance()));
        }

        EDSMSubmitDistances distances = new EDSMSubmitDistances(system, al);

        distances.setCommander(cmdrName);

        Log.d(LOG_NAME, "Submitting: " + system + ", Num distances required: " + Integer.toString(numSystems) + ", Num distances submitted: " + Integer.toString(al.size()));

        synchronized (trilatMap) {
            EDSMFuture future = trilatMap.get(system);

            if (debounce) {
                if ((future != null) && !future.isDone()) {
                    future.cancel(false);
                }

                future = new EDSMFuture(executorService.schedule(new AsyncSubmitDistanceCallable(distances),
                                                                   SUBMIT_DEBOUNCE_SECONDS,
                                                                   TimeUnit.SECONDS));
                trilatMap.put(system, future);
            } else {
                if ((future == null) || future.isDone()) {
                    future = new EDSMFuture(executorService.submit(new AsyncSubmitDistanceCallable(distances)));
                    trilatMap.put(system, future);
                }
            }

            return future;
        }
    }

    @Override
    public List<? extends DistanceInfo> getDistances(String system, boolean duplicatesAllowed)
    {
        return dbHelper.getDistances(system, duplicatesAllowed);
    }

    public static class TempleIntentService extends IntentService
    {
        private static final long TWO_WEEKS = (2 * 7 * 24 * 60 * 60 * 1000);

        private final AtomicBoolean running = new AtomicBoolean(false);

        private final Lock mutex = new ReentrantLock();
        private final Condition condition = mutex.newCondition();

        private EDSMStarCoordinator starCoordinator;
        private NetworkChangeReceiver networkReceiver;
        private EDSMDbHelper dbHelper;

        public TempleIntentService()
        {
            super("TempleIntentService");
        }

        @Override
        public void onCreate()
        {
            super.onCreate();

            dbHelper = EDSMDbHelper.getInstance();
            starCoordinator = EDSMStarCoordinator.getInstance(getApplicationContext());
            networkReceiver = new NetworkChangeReceiver();

            registerReceiver(networkReceiver,
                             new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

        @Override
        protected void onHandleIntent(Intent intent)
        {
            running.set(true);

            while (running.get()) {
                if (!isNetworkAvailable()) {
                    Log.d(LOG_NAME, "No network available to perform temple server update.");

                    try {
                        mutex.lock();
                        condition.await(10, TimeUnit.SECONDS);
                    } catch (InterruptedException ignored) {
                    } finally {
                        mutex.unlock();
                    }

                    continue;
                }

                Log.d(LOG_NAME, "Upload cmdr distances.");
                uploadCmdrDistances();
                Log.d(LOG_NAME, "Update EDSM Database");
                updateEDSMDb();

                Log.d(LOG_NAME, "Update cmdr systems");
                if (updateSystems()) {
                    starCoordinator.sendSystemUpdatedIntent();
                }

                Log.d(LOG_NAME, "Update cmdr distances.");
                if (updateDistances()) {
                    starCoordinator.sendDistanceUpdatedIntent();
                }

                Log.d(LOG_NAME, "Yield for 15 minutes.");
                try {
                    mutex.lock();
                    if (isNetworkAvailable()) {
                        condition.await(15, TimeUnit.MINUTES);
                    }
                } catch (InterruptedException ignored) {
                } finally {
                    mutex.unlock();
                }
            }
        }

        @Override
        public void onDestroy()
        {
            try {
                mutex.lock();
                running.set(false);
                condition.signal();
            } finally {
                mutex.unlock();
            }

            unregisterReceiver(networkReceiver);
            super.onDestroy();
        }

        private boolean isNetworkAvailable()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

            return (activeNetworkInfo != null &&
                    activeNetworkInfo.isConnected() &&
                    ((activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) ||
                     (activeNetworkInfo.getType() == ConnectivityManager.TYPE_ETHERNET)));
        }

        private void uploadCmdrDistances()
        {
            List<CmdrDistanceInfo> list = starCoordinator.cmdrDbHelper.getUploadRequiredDistances();
            HashMap<String, ArrayList<Pair<String, Double>>> map = new HashMap<String, ArrayList<Pair<String, Double>>>();
            HashMap<String, Future<EDSMSubmitResponse>> futureMap = new HashMap<String, Future<EDSMSubmitResponse>>();

            for (CmdrDistanceInfo info : list) {
                if (info.getSecondSystem() != null) {
                    if (map.containsKey(info.getFirstSystem())) {
                        ArrayList<Pair<String, Double>> al = map.get(info.getFirstSystem());
                        al.add(new Pair<String, Double>(info.getSecondSystem(), info.getDistance()));
                    } else {
                        ArrayList<Pair<String, Double>> al = new ArrayList<Pair<String, Double>>();
                        al.add(new Pair<String, Double>(info.getSecondSystem(), info.getDistance()));
                        map.put(info.getFirstSystem(), al);
                    }
                }
            }

            String cmdrName = starCoordinator.cmdrDbHelper.getCmdrName();

            for (String key : map.keySet()) {
                EDSMSubmitDistances distances = new EDSMSubmitDistances(key, map.get(key));

                distances.setCommander(cmdrName);

                Future<EDSMSubmitResponse> future = executorService.submit(new AsyncSubmitDistanceCallable(distances));
                futureMap.put(key, future);
            }

            for (String key : futureMap.keySet()) {
                try {
                    Future<EDSMSubmitResponse> future = futureMap.get(key);
                    EDSMSubmitResponse response = future.get();

                    if (response != null) {
                        Log.d(LOG_NAME, response.toString());

                        List<EDSMSubmitResponse.TempleDistanceResponse> distanceResponses = response.getDistancesProcessed();

                        for (EDSMSubmitResponse.TempleDistanceResponse dist : distanceResponses) {
                            switch (dist.code) {
                                case SYSTEM_ADDED:
                                case SYSTEM_ADDED_WITH_COORDS:
                                case SYSTEM_COORDS_FOUND:
                                case DISTANCE_EXIST_CR_INC:
                                case DISTANCE_EXIST_CR_SAVED:
                                case DISTANCE_SAVED:
                                case SYSTEM_COORDS_FOUND2:
                                    for (CmdrDistanceInfo info : list) {
                                        if (info.getFirstSystem().equals(key) &&
                                            info.getSecondSystem().equals(dist.systemName)) {
                                            Log.d(LOG_NAME,
                                                  "Mark [" + key + " --> " + dist.systemName + "] as updated.");
                                            info.setRemoteUpdated(true);
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    Log.e(LOG_NAME, "Distance submission was interrupted by the VM.", e);
                } catch (ExecutionException e) {
                    Log.e(LOG_NAME, "Distance submission failed executing.", e);
                }
            }
        }

        private void updateEDSMDb()
        {
            Date startTime = (dbHelper.getLastSystemCheck().compareTo(dbHelper.getLastDistanceCheck()) < 0) ? dbHelper.getLastSystemCheck() : dbHelper.getLastDistanceCheck();
            Date endTime;
            Date now = new Date();
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            do {
                endTime = new Date(startTime.getTime() + TWO_WEEKS);
                if (endTime.compareTo(now) > 0) {
                    endTime = now;
                }

                List<EDSMSystem> systems = EDSMConnection.getTimeSpan(startTime, endTime);
                Log.d(LOG_NAME, "System list: " + systems.size());

                startTime = endTime;

                try {
                    int i = 0;
                    db.beginTransaction();

                    Log.d(LOG_NAME, "Loop through systems");
                    for (EDSMSystem system : systems) {
                        EDSMSystemInfo systemInfo = dbHelper.createSystem(system.getName(), system.getPosition());

                        if ((system.getPosition() != null) &&
                            (!system.getPosition().equals(systemInfo.getPosition()))) {
                            systemInfo.setPosition(system.getPosition());
                        }

                        for (EDSMSystem.SystemDistance distance : system.getDistances()) {
                            EDSMDistanceInfo distanceInfo = dbHelper.getDistance(system.getName(),
                                                                                 distance.name);

                            if (distanceInfo != null) {
                                if (distance.distance != distanceInfo.getDistance()) {
                                    distanceInfo.setDistance(distance.distance);
                                }
                            } else {
                                dbHelper.createDistance(system.getName(),
                                                        distance.name,
                                                        distance.distance);
                            }
                        }

                        if (i++ == 1000) {
                            i = 0;
                            db.setTransactionSuccessful();
                            db.endTransaction();
                            db.beginTransaction();
                        }
                    }

                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            } while (!endTime.equals(now));

            Log.d(LOG_NAME, "Update last checks.");
            dbHelper.setLastDistanceCheck(now);
            dbHelper.setLastSystemCheck(now);
        }

        private boolean updateSystems()
        {
            boolean updated = false;
            List<CmdrSystemInfo> list = CmdrDbHelper.getInstance().getSystemsWithNoPosition();

            for (CmdrSystemInfo info : list) {
                EDSMSystemInfo systemInfo = dbHelper.getSystem(info.getSystem());

                if ((systemInfo != null) && (systemInfo.getPosition() != null)) {
                    info.setPosition(systemInfo.getPosition());
                    updated = true;
                }
            }

            return updated;
        }

        private boolean updateDistances()
        {
            boolean updated = false;
            List<CmdrDistanceInfo> list = CmdrDbHelper.getInstance().getEmptyDistances();

            for (CmdrDistanceInfo info : list) {
                EDSMDistanceInfo distanceInfo = dbHelper.getDistance(info.getFirstSystem(), info.getSecondSystem());

                if (distanceInfo != null) {
                    info.setDistance(distanceInfo.getDistance());
                    updated = true;
                }
            }

            return updated;
        }

        private class NetworkChangeReceiver extends BroadcastReceiver
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                switch (intent.getAction()) {
                    case ConnectivityManager.CONNECTIVITY_ACTION:
                        try {
                            mutex.lock();
                            condition.signalAll();
                        } finally {
                            mutex.unlock();
                        }

                        break;
                    default:
                        break;
                }
            }
        }
    }
}
