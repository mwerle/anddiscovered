package com.fussyware.AndDiscovered.adapter;

import android.widget.BaseAdapter;

import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by wes on 6/2/15.
 */
public abstract class BaseCursorAdapter extends BaseAdapter
{
    private static final String LOG_NAME = BaseCursorAdapter.class.getSimpleName();

    protected CmdrDbHelper dbHelper;
    protected ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
//    protected Activity activity;

    public BaseCursorAdapter()
    {
        dbHelper = CmdrDbHelper.getInstance();
    }
//    public BaseCursorAdapter(Activity activity)
//    {
//        this.activity = activity;
//        dbHelper = CmdrDbHelper.getInstance(activity.getApplicationContext());
//
//        DistanceUpdatedIntentReceiver recv = new DistanceUpdatedIntentReceiver();
//        LocalBroadcastManager
//                .getInstance(activity)
//                .registerReceiver(recv,
//                                  new IntentFilter(EDProxyIntentService.BROADCAST_SYSTEM_UPDATED));
//        LocalBroadcastManager
//                .getInstance(activity)
//                .registerReceiver(recv,
//                                  new IntentFilter(StarCoordinator.BROADCAST_DISTANCE_UPDATED));
//    }
//
//    protected SQLiteDatabase getReadableDatabase()
//    {
//        return dbHelper.getReadableDatabase();
//    }

    public abstract void notifyDatabaseChanged();

//    private class DistanceUpdatedIntentReceiver extends BroadcastReceiver
//    {
//        @Override
//        public void onReceive(Context context, Intent intent)
//        {
//            String action = intent.getAction();
//
//            switch (action) {
//                case EDProxyIntentService.BROADCAST_SYSTEM_UPDATED:
//                    Log.d(LOG_NAME, "Got edproxy system update.");
//                    notifyDatabaseChanged();
//                    break;
//                case StarCoordinator.BROADCAST_SYSTEM_UPDATED:
//                    Log.d(LOG_NAME, "Got star coordinator system updated.");
//                    notifyDatabaseChanged();
//                    break;
//                case StarCoordinator.BROADCAST_DISTANCE_UPDATED:
//                    Log.d(LOG_NAME, "Got star coordinator distance updated.");
//                    notifyDatabaseChanged();
//                    break;
//                default:
//                    break;
//            }
//        }
//    }
}
