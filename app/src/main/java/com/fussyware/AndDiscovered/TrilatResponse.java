package com.fussyware.AndDiscovered;

import com.fussyware.AndDiscovered.edutils.Position;

/**
 * Created by wes on 8/21/15.
 */
public interface TrilatResponse
{
    Position getPosition();
    int getDistancesProcessed();
    boolean isTrilatPerformed();
}
