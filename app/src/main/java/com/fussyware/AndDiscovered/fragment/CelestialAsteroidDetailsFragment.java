package com.fussyware.AndDiscovered.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.AsteroidBody;
import com.fussyware.AndDiscovered.dialog.CelestialAsteroidEditDialogFragment;

/**
 * Created by wes on 11/17/15.
 */
public class CelestialAsteroidDetailsFragment extends Fragment
{
    private static final String LOG_NAME = CelestialAsteroidDetailsFragment.class.getSimpleName();

    private final Object mutex = new Object();

    private AsteroidBody celestialBody;

    private View mainView;
    private TextView massText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.cb_asteroid_details_layout,
                                    container,
                                    false);

        createViews(mainView);

        if (savedInstanceState == null) {
            mainView.setVisibility(View.GONE);
        } else {
            AsteroidBody body = savedInstanceState.getParcelable("celestial");
            setCelestialBody(body);
        }

        return mainView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial", celestialBody);
    }

    public void setCelestialBody(AsteroidBody body)
    {
        synchronized (mutex) {
            if (body != celestialBody) {
                celestialBody = body;

                if (body.getMoonMass() != null) {
                    massText.setText(String.format("%1.4f", body.getMoonMass()));
                }

                mainView.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onDone(Bundle bundle)
    {
        for (String key : bundle.keySet()) {
            double dbl = bundle.getDouble(key, 0.0);

            switch (key) {
                case CelestialAsteroidEditDialogFragment.TAG_MOONMASSES:
                    celestialBody.setMoonMass(dbl);
                    massText.setText(String.format("%1.4f", dbl));
                    break;
                default:
                    break;
            }
        }
    }

    private void createViews(View layout)
    {
        mainView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CelestialAsteroidEditDialogFragment df = CelestialAsteroidEditDialogFragment.newInstance(celestialBody);
                df.show(getFragmentManager(), FragmentTag.celestial_info_fragment.celestial_asteroid_dialog);
            }
        });

        massText = (TextView) layout.findViewById(R.id.cb_asteroid_moonmasses_value);
    }
}
