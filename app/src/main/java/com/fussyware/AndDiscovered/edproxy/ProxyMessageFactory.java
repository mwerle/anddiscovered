package com.fussyware.AndDiscovered.edproxy;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by wes on 6/15/15.
 */
public class ProxyMessageFactory
{
    private static final String LOG_NAME = ProxyMessageFactory.class.getSimpleName();

    static ProxyMessage getMessage(JSONObject object)
    {
        ProxyMessage value = null;

        try {
            ProxyMessageType messageType = ProxyMessageType.getMessageType(object.getString("Type"));
            Date date = getDate(object.getString("Date"));

            switch (messageType) {
                case Init:
                    break;
                case Pong:
                    value = PongMessage.getMessage(messageType, date, object);
                    break;
                case System:
                    value = SystemMessage.getMessage(messageType, date, object);
                    break;
                case Image:
                    value = ImageMessage.getMessage(messageType, date, object);
                    break;
            }
        } catch (JSONException e) {
            Log.e("ProxyMessageFactory", "Failed to retreive Type from JSON.");
        }

        return value;
    }

    private static Date getDate(String value)
    {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(value);
        } catch (ParseException e) {
            return new Date();
        }
    }
}
