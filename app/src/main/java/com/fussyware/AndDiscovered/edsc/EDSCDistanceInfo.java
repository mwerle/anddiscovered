package com.fussyware.AndDiscovered.edsc;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.fussyware.AndDiscovered.celestial.DistanceInfo;

/**
 * Created by wboyd on 7/7/15.
 */
public class EDSCDistanceInfo extends DistanceInfo
{
    private SQLiteDatabase db;
    private long id;

    EDSCDistanceInfo(SQLiteDatabase db, long id, String first, String second, double distance)
    {
        super(first, second, distance);

        this.db = db;
        this.id = id;
    }

    @Override
    public void setDistance(double distance)
    {
        distance = (distance < 0) ? -distance : distance;

        if (this.getDistance() != distance) {
            super.setDistance(distance);

            String selection = EDSCDbContract.EDSCDistances._ID + "=?";
            String[] selectionArgs = {Long.toString(id)};

            ContentValues values = new ContentValues();
            values.put(EDSCDbContract.EDSCDistances.COLUMN_NAME_DISTANCE, distance);

            db.update(EDSCDbContract.EDSCDistances.TABLE_NAME,
                      values,
                      selection,
                      selectionArgs);
        }
    }
}
