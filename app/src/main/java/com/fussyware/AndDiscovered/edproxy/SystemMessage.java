package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;
import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.edutils.ShipStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

/**
 * Created by wes on 6/14/15.
 */
public class SystemMessage extends ProxyMessage
{
    private String system;
    private int bodies;
    private ShipStatus status;
    private Position position;

    public SystemMessage(Date date, String system, int numBodies, ShipStatus status, Position position)
    {
        super(ProxyMessageType.System, date);

        this.system = system;
        this.bodies = numBodies;
        this.status = status;
        this.position = position;
    }

    public String getSystem()
    {
        return system;
    }

    public int getNumBodies()
    {
        return bodies;
    }

    public ShipStatus getShipStatus()
    {
        return status;
    }

    public Position getPosition()
    {
        return position;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("SystemMessage {");
        sb.append(super.toString());
        sb.append(", System [");
        sb.append(system);
        sb.append("], Bodies [");
        sb.append(bodies);
        sb.append("], Position [");
        sb.append(position.toString());
        sb.append("], Ship Status [");
        sb.append(status.toString());
        sb.append("]}");

        return sb.toString();
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {
        json.name("System").value(system);
        json.name("Bodies").value(bodies);
        json.name("Status").value(status.toString());

        json.name("Position");
        json.beginArray();
        json.value(Double.toString(position.x));
        json.value(Double.toString(position.y));
        json.value(Double.toString(position.z));
        json.endArray();
    }

    static SystemMessage getMessage(ProxyMessageType messageType, Date date, JSONObject object)
    {
        if (messageType != ProxyMessageType.System) {
            return null;
        }

        try {
            int _bodies = object.getInt("Bodies");
            String _system = object.getString("System");

            ShipStatus _status = getStatus(object.getString("Status"));

            Position _position = null;
            {
                JSONArray list = object.getJSONArray("Position");

                double x = list.getDouble(0);
                double y = list.getDouble(1);
                double z = list.getDouble(2);

                _position = new Position(x, y, z);
            }

            return new SystemMessage(date, _system, _bodies, _status, _position);
        } catch (JSONException e) {
            return null;
        }
    }

    private static ShipStatus getStatus(String value)
    {
        for (ShipStatus status : ShipStatus.values()) {
            if (status.toString().equals(value)) {
                return status;
            }
        }

        return ShipStatus.Unknown;
    }
}
