package com.fussyware.AndDiscovered;

import android.view.View;

/**
 * Created by wes on 8/22/15.
 */
interface TopRowChangeListener
{
    void topRowChanged(String oldSystem, String newSystem, View newView);
}
