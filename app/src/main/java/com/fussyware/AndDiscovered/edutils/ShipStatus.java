package com.fussyware.AndDiscovered.edutils;

/**
 * Created by wes on 6/14/15.
 */
public enum ShipStatus
{
    Unknown("unknown"),
    Cruising("cruising");

    private String value;

    ShipStatus(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value;
    }
}
