package com.fussyware.AndDiscovered.edsc;

/**
 * Created by wes on 6/18/15.
 */
public class EDSCDistance
{
    private String from;
    private String to;
    private double distance;

    EDSCDistance(String from, String to, double distance)
    {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    public String getFromSystem()
    {
        return from;
    }

    public String getToSystem()
    {
        return to;
    }

    public double getDistance()
    {
        return distance;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("EDSCDistance { From [");
        sb.append(from);
        sb.append("], To [");
        sb.append(to);
        sb.append("], Distance [");
        sb.append(distance);
        sb.append("]}");

        return sb.toString();
    }
}
