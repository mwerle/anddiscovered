package com.fussyware.AndDiscovered.edsm;

import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;

/**
 * Created by wes on 9/28/15.
 */
class EDSMSystem
{
    private final String name;

    private Position position;
    private ArrayList<SystemDistance> distances = new ArrayList<>();

    public EDSMSystem(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public Position getPosition()
    {
        return position;
    }

    public void setPosition(Position position)
    {
        this.position = position;
    }

    public ArrayList<SystemDistance> getDistances()
    {
        return distances;
    }

    public void setDistances(ArrayList<SystemDistance> distances)
    {
        this.distances = distances;
    }

    public void add(SystemDistance distance)
    {
        distances.add(distance);
    }

    @Override
    public String toString()
    {
        return "EDSMSystem{" +
               "name='" + name + '\'' +
               ", position=" + position +
               ", distances=" + distances +
               '}';
    }

    static class SystemDistance
    {
        final String name;
        final Double distance;

        SystemDistance(String name, Double distance)
        {
            this.name = name;
            this.distance = distance;
        }

        @Override
        public String toString()
        {
            return "SystemDistance{" +
                   "name='" + name + '\'' +
                   ", distance=" + distance +
                   '}';
        }
    }
}
