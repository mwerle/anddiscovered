package com.fussyware.AndDiscovered.edsc;

import android.provider.BaseColumns;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by wes on 6/15/15.
 */
public final class EDSCDbContract
{
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    static {
        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static abstract class EDSCSystems implements BaseColumns
    {
        public static final String TABLE_NAME = "EDSCSystems";
        public static final String COLUMN_NAME_SYSTEM = "System";
        public static final String COLUMN_NAME_XCOORD = "xCoord";
        public static final String COLUMN_NAME_YCOORD = "yCoord";
        public static final String COLUMN_NAME_ZCOORD = "zCoord";
//        public static final String COLUMN_NAME_ = "";
    }

    public static abstract class EDSCDistances implements BaseColumns
    {
        public static final String TABLE_NAME = "EDSCDistances";
        public static final String COLUMN_NAME_FROM = "FromSystem";
        public static final String COLUMN_NAME_TO = "ToSystem";
        public static final String COLUMN_NAME_DISTANCE = "Distance";
        public static final String COLUMN_NAME_EDSC_UPDATED = "EDSCUpdated";
//        public static final String COLUMN_NAME_ = "";
    }

    public static abstract class DbInfo implements BaseColumns
    {
        public static final String TABLE_NAME = "DbInfo";
        public static final String COLUMN_NAME_CONFIG = "ConfigName";
        public static final String COLUMN_NAME_VALUE = "Value";

        public static final String CONFIG_NAME_DBVERSION = "DbVersion";
        public static final String CONFIG_NAME_LAST_EDSC_UPLOAD = "LastEDSCUpload";
        public static final String CONFIG_NAME_LAST_EDSC_DISTANCE_CHECK = "LastEDSCDistanceCheck";
        public static final String CONFIG_NAME_LAST_EDSC_SYSTEM_CHECK = "LastEDSCSystemCheck";
//        public static final String COLUMN_NAME_ = "";
    }
}
