package com.fussyware.AndDiscovered.edsc;

import com.fussyware.AndDiscovered.edutils.Position;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by wes on 6/23/15.
 */
public class EDSCSystem
{
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    static {
        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private static final String LOG_NAME = EDSCSystem.class.getSimpleName();

    private String system;
    private Position position = null;
    private int id;
    private int cr;
    private String CmdrCreate;
    private String CmdrUpdate;
    private Date CmdrCreateDate;
    private Date CmdrUpdateDate;

    EDSCSystem(JSONObject json) throws JSONException
    {
        system = json.getString("name");
        id = json.getInt("id");
        cr = json.getInt("cr");
        CmdrCreate = json.getString("commandercreate");
        CmdrUpdate = json.getString("commanderupdate");

        try {
            CmdrCreateDate = DATE_FORMATTER.parse(json.getString("createdate"));
        } catch (ParseException e) {
            CmdrCreateDate = new Date(0);
        }
        try {
            CmdrUpdateDate = DATE_FORMATTER.parse(json.getString("updatedate"));
        } catch (ParseException e) {
            CmdrUpdateDate = new Date(0);
        }

        JSONArray array = json.getJSONArray("coord");
        if (array.length() == 3) {
            if (!array.isNull(0) && !array.isNull(1) && !array.isNull(2)) {
                position = new Position(array.getDouble(0),
                                        array.getDouble(1),
                                        array.getDouble(2));
            }
        }
    }

    public String getSystem()
    {
        return system;
    }

    public Position getPosition()
    {
        return position;
    }

    public int getId()
    {
        return id;
    }

    public int getCr()
    {
        return cr;
    }

    public String getCmdrCreate()
    {
        return CmdrCreate;
    }

    public String getCmdrUpdate()
    {
        return CmdrUpdate;
    }

    public Date getCmdrCreateDate()
    {
        return CmdrCreateDate;
    }

    public Date getCmdrUpdateDate()
    {
        return CmdrUpdateDate;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EDSCSystem that = (EDSCSystem) o;

        if (system != null ? !system.equalsIgnoreCase(that.system) : that.system != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return system != null ? system.hashCode() : 0;
    }
}
