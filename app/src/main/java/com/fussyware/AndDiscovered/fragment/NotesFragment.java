package com.fussyware.AndDiscovered.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.DebouncedTextWatcher;

/**
 * Created by wes on 9/21/15.
 */
public class NotesFragment extends BaseInfoFragment
{
    private EditText notesText;
    private CmdrSystemInfo selectedSystem;
    private OnDoneListener listener;

    private final Object mutex = new Object();

    public interface OnDoneListener
    {
        void onDone(String tag, String text);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof OnDoneListener) {
            listener = (OnDoneListener) activity;
        } else {
            throw new ClassCastException("Activity does not implement OnDoneListener.");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.notes_layout, container, false);

        if (savedInstanceState != null) {
            selectedSystem = CmdrDbHelper
                    .getInstance()
                    .getSystem(savedInstanceState.getString("NotesFragment.system"));
        }

        createNotes(layout, savedInstanceState);

        return layout;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        selectedSystem = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString("NotesFragment.system", selectedSystem.getSystem());
        outState.putString("NotesFragment.notes", notesText.getText().toString());
    }

    @Override
    protected void onNewSystem(SystemInfo system)
    {
        if (system instanceof CmdrSystemInfo) {
            setSystem((CmdrSystemInfo) system);
        }
    }

    public void setSystem(CmdrSystemInfo system)
    {
        synchronized (mutex) {
            if (!system.equals(selectedSystem)) {
                selectedSystem = system;
                notesText.setText(system.getNote());
            }
        }
    }

    private void createNotes(View layout, Bundle savedInstanceState)
    {
        notesText = (EditText) layout.findViewById(R.id.NotesEditText);

        if (savedInstanceState != null) {
            notesText.setText(savedInstanceState.getString("NotesFragment.notes"));
        }

        notesText.addTextChangedListener(new DebouncedTextWatcher(3000)
        {
            @Override
            public void debouncedTextChanged(Editable s)
            {
                if (selectedSystem != null) {
                    new AsyncTask<String, Void, String>()
                    {
                        @Override
                        protected void onPostExecute(String result)
                        {
                            if ((listener != null) && (!result.isEmpty())) {
                                listener.onDone(FragmentTag.notes_fragment.tag, result);
                            }
                        }

                        @Override
                        protected String doInBackground(String... strings)
                        {
                            CmdrSystemInfo info = CmdrDbHelper.getInstance().getSystem(strings[0]);
                            String ret = "";

                            if (info != null) {
                                info.setNote(strings[1]);
                                ret = strings[1];
                            }

                            return ret;
                        }
                    }.execute(selectedSystem.getSystem(), s.toString());
                }
            }
        });
    }
}
