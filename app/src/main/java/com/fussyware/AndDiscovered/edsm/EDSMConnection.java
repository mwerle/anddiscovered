package com.fussyware.AndDiscovered.edsm;

import android.util.Log;

import com.fussyware.AndDiscovered.edutils.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by wes on 8/8/15.
 */
abstract class EDSMConnection
{
    private static final String LOG_NAME = EDSMConnection.class.getSimpleName();
    private static final String baseUrl = "http://www.edsm.net/api-v1/";
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ZZZZZ",
                                                                                Locale.US);
    private final static ScheduledThreadPoolExecutor executorService = new ScheduledThreadPoolExecutor(20);

    static {
        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));

        executorService.setKeepAliveTime(60, TimeUnit.SECONDS);
        executorService.allowCoreThreadTimeOut(true);
    }

    static HttpURLConnection sendRequest(String url, EDSMJSON request) throws  IOException
    {
        HttpURLConnection conn = (HttpURLConnection) new URL(baseUrl + url).openConnection();

        byte[] b = request.getJSON().getBytes("UTF-8");

        Log.d(LOG_NAME, "JSON: " + request.getJSON());

        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("charset", "utf-8");
        conn.setRequestProperty("Content-Length", Integer.toString(b.length));
        conn.setRequestProperty("Accept-Encoding", "");
        conn.setFixedLengthStreamingMode(b.length);
        conn.setUseCaches(false);
        conn.connect();

        OutputStream os = conn.getOutputStream();
        os.write(b);
        os.flush();
        os.close();

        return conn;
    }

    static List<EDSMSystem> getTimeSpan(Date startTime, Date endTime)
    {
        ArrayList<EDSMSystem> systems = new ArrayList<>();

        try {
            Date now = new Date();

            if ((endTime == null) || (endTime.compareTo(now) > 0)) {
                endTime = now;
            }

            String st = "&startdatetime=" + URLEncoder.encode(DATE_FORMATTER.format(startTime), "UTF-8");
            String et = "&enddatetime=" + URLEncoder.encode(DATE_FORMATTER.format(endTime), "UTF-8");
            String url  = baseUrl + "systems?coords=1&distances=1" + st + et;

            Log.d(LOG_NAME, "URL: " + url);
            Log.d(LOG_NAME, "Start time: " + DATE_FORMATTER.format(startTime) + ", End time: " + DATE_FORMATTER.format(endTime));

            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestProperty("Accept-Encoding", "");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line = bufferedReader.readLine();

                if ((line != null) && line.startsWith("[")) {
                    JSONArray json_array = new JSONArray(line);

                    for (int i = 0; i < json_array.length(); i++) {
                        JSONObject json = json_array.getJSONObject(i);
                        EDSMSystem system = new EDSMSystem(json.getString("name"));

                        if (json.has("coords")) {
                            JSONObject object = json.getJSONObject("coords");

                            if (object.has("x") && object.has("y") && object.has("z")) {
                                system.setPosition(new Position(object.getDouble("x"),
                                                                object.getDouble("y"),
                                                                object.getDouble("z")));
                            }
                        }

                        if (json.has("distances")) {
                            JSONArray distance_array = json.getJSONArray("distances");

                            for (int j = 0; j < distance_array.length(); j++) {
                                JSONObject json_distance = distance_array.getJSONObject(j);

                                system.add(new EDSMSystem.SystemDistance(json_distance.getString("name"),
                                                                         json_distance.getDouble("distance")));
                            }
                        }

                        systems.add(system);
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(LOG_NAME, "Failed generating URL.", e);
        } catch (IOException e) {
            Log.e(LOG_NAME, "Failed getting data from EDSM.", e);
        } catch (JSONException e) {
            Log.e(LOG_NAME, "Failed parsing JSON.", e);
        }

        return systems;
    }
}
