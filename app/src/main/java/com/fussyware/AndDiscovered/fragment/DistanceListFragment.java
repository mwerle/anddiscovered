package com.fussyware.AndDiscovered.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.StarCoordinator;
import com.fussyware.AndDiscovered.StarCoordinatorFactory;
import com.fussyware.AndDiscovered.SystemTrilatExecutor;
import com.fussyware.AndDiscovered.TrilatListener;
import com.fussyware.AndDiscovered.adapter.DistanceListAdapter;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.TextDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrDistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

/**
 * Created by wes on 9/20/15.
 */
public class DistanceListFragment extends BaseInfoFragment
{
    private static final String LOG_NAME = DistanceListFragment.class.getSimpleName();

    private static final int TRILAT_STATE_HAVE_POSITION = 1;
    private static final int TRILAT_STATE_READY = 2;
    private static final int TRILAT_STATE_CALCULATING = 3;
    private static final int TRILAT_STATE_CALCULATION_NOT_ENOUGH = 4;
    private static final int TRILAT_STATE_CALCULATION_ERROR = 5;

    private SystemTrilatExecutor trilatExecutor;
    private final OnTrilatListener trilatListener = new OnTrilatListener();
    private int trilatState;

    private CmdrDbHelper dbHelper;
    private CmdrSystemInfo selectedSystem;

    private DistanceListAdapter distanceAdapter;
    private DistanceUpdateTask updateTask;

    private LocalBroadcastManager broadcastManager;
    private BroadcastReceiver broadcastReceiver;

    private OnSystemLongClickListener longClickListener;
    private OnPositionTrilaterationListener positionListener;

    private final Object mutex = new Object();

    public interface OnSystemLongClickListener
    {
        void onSystemLongClick(String system);
    }

    public interface OnPositionTrilaterationListener
    {
        void onPositionCalculation(String text);
        void onPositionFound(Position position);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof OnSystemLongClickListener) {
            longClickListener = (OnSystemLongClickListener) activity;
        } else {
            throw new ClassCastException("Activity does not implement OnSystemLongClickListener.");
        }

        if (activity instanceof OnPositionTrilaterationListener) {
            positionListener = (OnPositionTrilaterationListener) activity;
        } else {
            throw new ClassCastException("Activity does not implement OnPositionTrilaterationListener.");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        longClickListener = null;
        positionListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.distance_list_layout, container, false);

        dbHelper = CmdrDbHelper.getInstance();

        if (savedInstanceState != null) {
            String system = savedInstanceState.getString("DistanceListFragment.system");

            if (system != null) {
                selectedSystem = dbHelper.getSystem(system);
            }

            trilatState = savedInstanceState.getInt("DistanceListFragment.trilatState",
                                                    TRILAT_STATE_READY);
        }

        trilatExecutor = new SystemTrilatExecutor(dbHelper,
                                                  StarCoordinatorFactory.getInstance(getActivity(),
                                                                                     StarCoordinatorFactory.StarCoordinatorType.EDSM));
        trilatExecutor.registerTrilatListener(trilatListener);

        broadcastManager = LocalBroadcastManager.getInstance(getActivity().getApplicationContext());
        broadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String action = intent.getAction();

                switch (action) {
                    case StarCoordinator.BROADCAST_DISTANCE_UPDATED:
                        distanceAdapter.notifyDatabaseChanged();
                        break;
                    default:
                        break;
                }
            }
        };

        createDistanceListView(layout, savedInstanceState);

        IntentFilter filter = new IntentFilter();
        filter.addAction(StarCoordinator.BROADCAST_DISTANCE_UPDATED);
        broadcastManager.registerReceiver(broadcastReceiver, filter);

        return layout;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        trilatExecutor.unregisterTrilatListener(trilatListener);
        broadcastManager.unregisterReceiver(broadcastReceiver);

        broadcastManager = null;
        broadcastReceiver = null;
        selectedSystem = null;
        trilatExecutor = null;
        distanceAdapter = null;
        dbHelper = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString("DistanceListFragment.system", selectedSystem.getSystem());
        outState.putInt("DistanceListFragment.trilatState", trilatState);

        distanceAdapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onNewSystem(SystemInfo system)
    {
        if (system instanceof CmdrSystemInfo) {
            setSystem((CmdrSystemInfo) system);
        }
    }

    public void setSystem(@NonNull String system)
    {
        setSystem(dbHelper.getSystem(system));
    }

    public void setSystem(@NonNull CmdrSystemInfo system)
    {
        synchronized (mutex) {
            if (!system.equals(selectedSystem)) {
                selectedSystem = system;
                distanceAdapter.setSystemName(system.getSystem());

                if (selectedSystem.getPosition() == null) {
                    trilatState = TRILAT_STATE_READY;
                } else {
                    trilatState = TRILAT_STATE_HAVE_POSITION;
                }
            }
        }
    }

    public void onDistanceUpdateDialogDone(String text)
    {
        if (updateTask != null) {
            try {
                Double dist = Double.valueOf(text);

                if (dist >= 0.0) {
                    updateTask.execute(dist);
                }
            } catch (NumberFormatException e) {
                Log.e(LOG_NAME, "Failed to convert new distance: " + text, e);
            }
        }
    }

    private void createDistanceListView(View layout, Bundle savedInstanceState)
    {
        distanceAdapter = new DistanceListAdapter(getActivity(),
                                                  StarCoordinatorFactory.getInstance(getActivity(),
                                                                                           StarCoordinatorFactory.StarCoordinatorType.EDSM));

        if (savedInstanceState != null) {
            distanceAdapter.onViewStateRestored(savedInstanceState);
        }

        ListView distanceListView = (ListView) layout.findViewById(R.id.DistanceListView);
        distanceListView.setAdapter(distanceAdapter);
        distanceListView.setOnItemClickListener(new DistanceListItemClickListener());
        distanceListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                DistanceListAdapter.SystemDistanceInfo info = distanceAdapter.getItem(position);

                if (longClickListener != null) {
                    longClickListener.onSystemLongClick(info.toSystem);
                }

//                DistancePopupMenu distancePopupMenu = new DistancePopupMenu(info,
//                                                                            getActivity(),
//                                                                            view);
//                distancePopupMenu.show();

                return true;
            }
        });
    }

    private class DistancePopupMenu extends PopupMenu implements PopupMenu.OnMenuItemClickListener
    {
        private final DistanceListAdapter.SystemDistanceInfo info;

        /**
         * Construct a new PopupMenu.
         *
         * @param context Context for the PopupMenu.
         * @param anchor  Anchor view for this popup. The popup will appear below the anchor if there
         */
        public DistancePopupMenu(DistanceListAdapter.SystemDistanceInfo info, Context context, View anchor)
        {
            super(context, anchor);

            this.info = info;

            getActivity().getMenuInflater().inflate(R.menu.distance_popup_menu, getMenu());
            setOnMenuItemClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item)
        {
            switch (item.getItemId()) {
                case R.id.distance_jump_item:
                    if (longClickListener != null) {
                        longClickListener.onSystemLongClick(info.toSystem);
                    }

                    return true;
                case R.id.distance_hide_item:
                    return true;
                default:
                    return false;
            }
        }
    }

    private class DistanceListItemClickListener implements AdapterView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            DistanceListAdapter.SystemDistanceInfo info = distanceAdapter.getItem(position);

            String summary = "";
            summary += "Update the distance from system [";
            summary += info.fromSystem;
            summary += "] to the system [";
            summary += info.toSystem;
            summary += "]. As soon as the distance is updated ";
            summary += "position information will be attempted ";
            summary += "in the background.";

            updateTask = new DistanceUpdateTask(info);

            TextDialogFragment.newInstance("Update Distance",
                                           summary,
                                           info.formatDistance(),
                                           (InputType.TYPE_CLASS_NUMBER |
                                            InputType.TYPE_NUMBER_FLAG_DECIMAL))
                              .show(getFragmentManager(),
                                    FragmentTag.distance_list_fragment.distance_update_dialog);
        }
    }

    private class DistanceUpdateTask extends AsyncTask<Double, Void, Boolean>
    {
        private DistanceListAdapter.SystemDistanceInfo info;

        DistanceUpdateTask(DistanceListAdapter.SystemDistanceInfo info)
        {
            this.info = info;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean)
        {
            if (aBoolean) {
                if (positionListener != null) {
                    positionListener.onPositionCalculation(getResources().getString(R.string.CalculatingPosition));
                }

                trilatState = TRILAT_STATE_CALCULATING;
                trilatExecutor.submit(info.fromSystem);
            }

            distanceAdapter.notifyDatabaseChanged();
            updateTask = null;
        }

        @Override
        protected Boolean doInBackground(Double... params)
        {
            boolean performTrilat = false;
            double dist = params[0];

            if (info.toSystem.equals(DistanceListAdapter.NEXT_SYSTEM_IN_ROUTE)) {
                CmdrDistanceInfo info = dbHelper.getLastDistance();

                if ((info != null) &&
                    info.getFirstSystem().equals(this.info.fromSystem) &&
                    (info.getSecondSystem() == null)) {
                    info.setDistance(dist);
                }
            } else if (info.isReferenceSystem) {
                dbHelper.createReferenceDistance(info.fromSystem,
                                                 info.toSystem,
                                                 dist);
                performTrilat = true;
            } else {
                CmdrDistanceInfo info = dbHelper.getDistance(this.info.fromSystem,
                                                             this.info.toSystem);

                if (info != null) {
                    info.setDistance(dist);
                } else {
                    info = dbHelper.getReferenceDistance(this.info.fromSystem,
                                                         this.info.toSystem);
                    if (info != null) {
                        info.setDistance(dist);
                    }
                }
            }

            return performTrilat;
        }
    }

    private class OnTrilatListener implements TrilatListener
    {
        @Override
        public void onSuccess(String system, Position position)
        {
            if (system.equals(selectedSystem.getSystem())) {
                if (positionListener != null) {
                    positionListener.onPositionFound(position);
                }

                trilatState = TRILAT_STATE_HAVE_POSITION;
                distanceAdapter.notifyDatabaseChanged();
            }
        }

        @Override
        public void onFailure(String system, int reason)
        {
            if (system.equals(selectedSystem.getSystem())) {
                switch (reason) {
                    case MIN_SYSTEMS_NOT_MET:
                    case MIN_SYSTEMS_RAISED:
                        if (positionListener != null) {
                            positionListener.onPositionCalculation(getResources().getString(R.string.NeedMorePosition));
                        }

                        trilatState = TRILAT_STATE_CALCULATION_NOT_ENOUGH;
                        break;
                    case CANCELLED:
                        // We are just going to ignore this as it means a new
                        // instance of the trilat is running.
                        break;
                    default:
                        break;
                }
            }
        }

        @Override
        public void onError(String system, Throwable thrown)
        {
            if (system.equals(selectedSystem.getSystem())) {
                if (positionListener != null) {
                    positionListener.onPositionCalculation(getResources().getString(R.string.ErrorPosition));
                }

                trilatState = TRILAT_STATE_CALCULATION_ERROR;
            }
        }
    }
}
