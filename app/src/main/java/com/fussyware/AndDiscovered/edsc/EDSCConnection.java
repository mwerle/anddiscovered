package com.fussyware.AndDiscovered.edsc;

import android.util.Log;

import com.fussyware.AndDiscovered.json.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by wes on 6/16/15.
 */
public abstract class EDSCConnection
{
    private static final String LOG_NAME = EDSCConnection.class.getSimpleName();
    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    private static HttpURLConnection sendRequest(String url, EDSCJSON request) throws  IOException
    {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

        byte[] b = request.getJSON().getBytes("UTF-8");

        Log.d(LOG_NAME, "JSON: " + request.getJSON());

        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("charset", "utf-8");
        conn.setRequestProperty("Content-Length", Integer.toString(b.length));
        conn.setUseCaches(false);
        conn.connect();

        conn.getOutputStream().write(b);
        conn.getOutputStream().flush();

        return conn;
    }

    public static EDSCDistanceResponse getDistances(EDSCRequest request)
    {
        HttpURLConnection conn = null;
        Date t0;

        try {

//            conn = (HttpURLConnection) new URL("http://edstarcoordinator.com/api.asmx/GetDistances").openConnection();
//
//            byte[] b = request.getJSON().getBytes(StandardCharsets.UTF_8);
//
//            Log.d(LOG_NAME, "JSON: " + request.getJSON());
//
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setInstanceFollowRedirects(false);
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("charset", "utf-8");
//            conn.setRequestProperty("Content-Length", Integer.toString(b.length));
//            conn.setUseCaches(false);
//            conn.connect();
//
//            conn.getOutputStream().write(b);
//            conn.getOutputStream().flush();

            t0 = new Date();
            conn = sendRequest("http://edstarcoordinator.com/api.asmx/GetDistances", request);
            Log.d(LOG_NAME, "Distance request time. [" + (new Date().getTime() - t0.getTime()) + "]");

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                try {
                    JsonParser parser = new JsonParser(new InputStreamReader(conn.getInputStream()));

                    t0 = new Date();
                    JSONObject o = parser.getBaseObject();
                    Log.d(LOG_NAME, "JSON distance parsing time. [" + (new Date().getTime() - t0.getTime()) + "]");
                    if (null != o) {
                        return new EDSCDistanceResponse(request.getSystem(), o);
                    } else {
                        Log.e(LOG_NAME, "Failed to parse JSON distance.");
                    }
                } catch (IOException e) {
                    Log.e(LOG_NAME, "Failed connection to the EDSC server.", e);
                } catch (JSONException e) {
                    Log.e(LOG_NAME, "Failed to parse the distance JSON.", e);
                }
            } else {
                Log.d(LOG_NAME, "Failed attempting get distance request." + Integer.toString(conn.getResponseCode()));
            }

            return null;
        } catch (MalformedURLException e) {
            Log.e(LOG_NAME, "Failed connecting to EDSC due to bad URL. " + e.getMessage());
        } catch (IOException e) {
            Log.e(LOG_NAME, "Failed connecting to EDSC due to unknown IO error. " + e.getMessage());
        } finally {
            if (null != conn) {
                conn.disconnect();
            }
        }

        return null;
    }

    public static List<EDSCSystem> getSystems(EDSCRequest request)
    {
        HttpURLConnection conn = null;
        ArrayList<EDSCSystem> systemList = new ArrayList<EDSCSystem>();
        Date t0;

        try {
            t0 = new Date();
            conn = sendRequest("http://edstarcoordinator.com/api.asmx/GetSystems", request);
            Log.d(LOG_NAME, "System request time. [" + (new Date().getTime() - t0.getTime()) + "]");

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                try {
                    JsonParser parser = new JsonParser(new InputStreamReader(conn.getInputStream()));

                    t0 = new Date();
                    JSONObject o = parser.getBaseObject();
                    Log.d(LOG_NAME, "JSON system parsing time. [" + (new Date().getTime() - t0.getTime()) + "]");
                    if (null != o) {
                        JSONObject json = o.getJSONObject("d");
                        JSONArray jsonArray = json.getJSONArray("systems");

                        t0 = new Date();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            EDSCSystem system = new EDSCSystem(jsonArray.getJSONObject(i));
                            if (!systemList.contains(system)) {
                                systemList.add(system);
                            }
                        }
                        Log.d(LOG_NAME, "Parsed all systems to the list. [" + (new Date().getTime() - t0.getTime()) + "]");
                    } else {
                        Log.e(LOG_NAME, "Failed to parse JSON system.");
                    }
                } catch (IOException e) {
                    Log.e(LOG_NAME, "Failed to communicate with the EDSC server.", e);
                } catch (JSONException e) {
                    Log.e(LOG_NAME, "Failed to parse the system JSON.", e);
                }
            } else {
                Log.d(LOG_NAME, "Failed attempting get system request." + Integer.toString(conn.getResponseCode()));
            }

            return systemList;
        } catch (MalformedURLException e) {
            Log.e(LOG_NAME, "Failed connecting to EDSC due to bad URL. " + e.getMessage());
        } catch (IOException e) {
            Log.e(LOG_NAME, "Failed connecting to EDSC due to unknown IO error. " + e.getMessage());
        } finally {
            if (null != conn) {
                conn.disconnect();
            }
        }

        return systemList;
    }

    public static Future<EDSCSubmitResponse> submitDistances(EDSCSubmitDistances distances)
    {
        return executorService.submit(new SubmitDistanceCallable(distances));
    }

    private static class SubmitDistanceCallable implements Callable<EDSCSubmitResponse>
    {
        private EDSCSubmitDistances distances;

        public SubmitDistanceCallable(EDSCSubmitDistances distances)
        {
            this.distances = distances;
        }

        @Override
        public EDSCSubmitResponse call() throws Exception
        {
            HttpURLConnection conn = null;

            try {
                conn = sendRequest("http://edstarcoordinator.com/api.asmx/SubmitDistances", distances);

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    try {
                        JsonParser parser = new JsonParser(new InputStreamReader(conn.getInputStream()));

                        JSONObject o = parser.getBaseObject();
                        Log.d(LOG_NAME, "JSON Response: " + o.toString());
                        if (null != o) {
                            return new EDSCSubmitResponse(o.getJSONObject("d"));
                        } else {
                            Log.e(LOG_NAME, "Failed to parse JSON distance.");
                        }
                    } catch (IOException e) {
                        Log.e(LOG_NAME, "[IO Error] Failed to parse the submit distance JSON. " + e.getMessage());
                    } catch (JSONException e) {
                        Log.e(LOG_NAME, "[JSON Error] Failed to parse the submit distance JSON. " + e.getMessage());
                    }
                } else {
                    Log.e(LOG_NAME, "Failed attempting submitting distances. Code: " + Integer.toString(conn.getResponseCode()));
                }
            } catch (MalformedURLException e) {
                Log.e(LOG_NAME, "Failed connecting to EDSC due to bad URL. " + e.getMessage());
            } catch (IOException e) {
                Log.e(LOG_NAME, "Failed connecting to EDSC due to unknown IO error. " + e.getMessage());
            } finally {
                if (null != conn) {
                    conn.disconnect();
                }
            }

            return null;
        }
    }
}
