package com.fussyware.AndDiscovered.fragment;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.StringListAdapter;
import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.CheckDialogFragment;
import com.fussyware.AndDiscovered.dialog.TextPickerDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wes on 9/30/15.
 */
public class CelestialInfoFragment extends BaseInfoFragment
{
    private TextView mainStarText;
    private TextView stellarCountText;
    private TextView planetCountText;

    private StringListAdapter starAdapter;
    private StringListAdapter planetAdapter;

    private CmdrSystemInfo selectedSystem;

    private final Object mutex = new Object();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.celestial_info_layout, container, false);

        createStarLayout(layout, savedInstanceState);
        createPlanetLayout(layout, savedInstanceState);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    @Override
    protected void onNewSystem(SystemInfo system)
    {
        if (system instanceof CmdrSystemInfo) {
            setSystem((CmdrSystemInfo) system);
        }
    }

    public void setSystem(CmdrSystemInfo systemInfo)
    {
        synchronized (mutex) {
            if (!systemInfo.equals(selectedSystem)) {
                selectedSystem = systemInfo;

                planetCountText.setText(Integer.toString(systemInfo.getPlanetaryBodies()));
                stellarCountText.setText(Integer.toString(systemInfo.getStellarBodies()));

                if (systemInfo.getMainStar() == StarType.Unknown) {
                    mainStarText.setText(getActivity().getResources()
                                                      .getString(R.string.star_select));
                } else {
                    mainStarText.setText(systemInfo.getMainStar().toString());
                }

                /*
                ArrayList<String> items = new ArrayList<>();

                StarType[] stars = systemInfo.getStars();
                for (StarType type : stars) {
                    items.add(type.toString());
                }

                if (items.isEmpty()) {
                    items.add(getActivity().getResources().getString(R.string.none_selected));
                }

                starAdapter.setItems(items);

                items = new ArrayList<>();
                PlanetType[] planets = systemInfo.getPlanets();
                for (PlanetType type : planets) {
                    items.add(type.toString());
                }

                if (items.isEmpty()) {
                    items.add(getActivity().getResources().getString(R.string.none_selected));
                }

                planetAdapter.setItems(items);
                */
            }
        }
    }

    public void onMainStarDialogDone(String text)
    {
        mainStarText.setText(text);
        selectedSystem.setMainStar(StarType.getStarType(text));
    }

    public void onStarDialogDone(int count, CheckDialogFragment.CheckListItem[] items)
    {
        stellarCountText.setText(Integer.toString(count));

        new AsyncTask<Integer, Void, Void>()
        {
            @Override
            protected Void doInBackground(Integer... params)
            {
                selectedSystem.setStellarBodies(params[0]);
                return null;
            }
        }.execute(count);

        ArrayList<String> list = new ArrayList<>();
        for (CheckDialogFragment.CheckListItem item : items) {
            if (item.isChecked()) {
                list.add(item.name);
            }
        }

        new AsyncTask<ArrayList<String>, Void, Void>()
        {
            @Override
            protected final Void doInBackground(ArrayList<String>... params)
            {
                StarType[] stars = new StarType[params[0].size()];

                int length = params[0].size();
                for (int i = 0; i < length; i++) {
                    stars[i] = StarType.getStarType(params[0].get(i));
                }

                //selectedSystem.setStars(stars);

                return null;
            }
        }.execute(list);

        if (list.isEmpty()) {
            list.add(getResources().getString(R.string.none_selected));
        }

        starAdapter.setItems(list);
    }

    public void onPlanetDialogDone(int count, CheckDialogFragment.CheckListItem[] items)
    {
        planetCountText.setText(Integer.toString(count));

        new AsyncTask<Integer, Void, Void>()
        {
            @Override
            protected Void doInBackground(Integer... params)
            {
                selectedSystem.setPlanetaryBodies(params[0]);
                return null;
            }
        }.execute(count);

        ArrayList<String> list = new ArrayList<>();
        for (CheckDialogFragment.CheckListItem item : items) {
            if (item.isChecked()) {
                list.add(item.name);
            }
        }

        new AsyncTask<ArrayList<String>, Void, Void>()
        {
            @Override
            protected final Void doInBackground(ArrayList<String>... params)
            {
                PlanetType[] planets = new PlanetType[params[0].size()];

                int length = params[0].size();
                for (int i = 0; i < length; i++) {
                    planets[i] = PlanetType.getPlanetType(params[0].get(i));
                }

                //selectedSystem.setPlanets(planets);

                return null;
            }
        }.execute(list);

        if (list.isEmpty()) {
            list.add(getResources().getString(R.string.none_selected));
        }

        planetAdapter.setItems(list);
    }

    private void createStarLayout(View layout, Bundle savedInstanceState)
    {
        View innerLayout = layout.findViewById(R.id.MainStellarLayout);
        innerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources res = getActivity().getResources();
                ArrayList<String> list = new ArrayList<>(StarType.values().length);

                for (StarType type : StarType.values()) {
                    list.add(type.toString());
                }

                TextPickerDialogFragment dialog = TextPickerDialogFragment.newInstance(res.getString(R.string.main_star_dialog_title),
                                                                                       res.getString(R.string.main_star_dialog_summary, selectedSystem.getSystem()),
                                                                                       list.toArray(new String[StarType.values().length]),
                                                                                       mainStarText.getText().toString());
                dialog.show(getFragmentManager(), FragmentTag.celestial_info_fragment.main_star_dialog);
            }
        });

        innerLayout = layout.findViewById(R.id.SecondaryStellarLayout);
        innerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CheckDialogFragment.CheckListItem[] items = new CheckDialogFragment.CheckListItem[StarType.values().length - 1];
                StarType[] values = StarType.values();
                List<String> selectedItems = starAdapter.getItems();

                for (int i = 0, j = 0; i < values.length; i++) {
                    if (values[i] != StarType.Unknown) {
                        String value = values[i].toString();
                        boolean checked = selectedItems.contains(value);

                        items[j++] = new CheckDialogFragment.CheckListItem(value, checked);
                    }
                }

                Resources res = getActivity().getResources();
                CheckDialogFragment fragment = CheckDialogFragment.newInstance(res.getString(R.string.secondary_star_dialog_title),
                                                                               res.getString(R.string.secondary_star_dialog_summary),
                                                                               selectedSystem.getStellarBodies(),
                                                                               items);
                fragment.show(getFragmentManager(), FragmentTag.celestial_info_fragment.star_check_list_dialog);
            }
        });

        stellarCountText = (TextView) layout.findViewById(R.id.StellarCountText);
        mainStarText = (TextView) layout.findViewById(R.id.MainStarText);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        starAdapter = new StringListAdapter(R.layout.string_horizontal_layout_item, new ArrayList<String>());

        RecyclerView starList = (RecyclerView) layout.findViewById(R.id.StellarBodyList);
        starList.setLayoutManager(layoutManager);
        starList.setHasFixedSize(true);
        starList.setAdapter(starAdapter);
        starList.addItemDecoration(new DividerItemDecoration(getActivity(),
                                                             LinearLayoutManager.HORIZONTAL));
    }

    private void createPlanetLayout(View layout, Bundle savedInstanceState)
    {
        planetCountText = (TextView) layout.findViewById(R.id.NumBodiesText);

        View innerLayout = layout.findViewById(R.id.PlanetLayout);
        innerLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CheckDialogFragment.CheckListItem[] items = new CheckDialogFragment.CheckListItem[
                        PlanetType.values().length - 1];
                PlanetType[] values = PlanetType.values();
                List<String> selectedItems = planetAdapter.getItems();

                for (int i = 0, j = 0; i < values.length; i++) {
                    if (values[i] != PlanetType.Unknown) {
                        items[j++] = new CheckDialogFragment.CheckListItem(values[i].toString(),
                                                                           selectedItems.contains(values[i].toString()));
                    }
                }

                Resources res = getActivity().getResources();
                CheckDialogFragment fragment = CheckDialogFragment.newInstance(
                        res.getString(R.string.planetary_dialog_title),
                        res.getString(R.string.planetary_dialog_summary),
                        selectedSystem.getPlanetaryBodies(),
                        items);
                fragment.show(getFragmentManager(),
                              FragmentTag.celestial_info_fragment.planet_check_list_dialog);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        planetAdapter = new StringListAdapter(R.layout.string_horizontal_layout_item, new ArrayList<String>());

        RecyclerView planetList = (RecyclerView) layout.findViewById(R.id.PlanetBodyList);
        planetList.setLayoutManager(layoutManager);
        planetList.setHasFixedSize(true);
        planetList.setAdapter(planetAdapter);
        planetList.addItemDecoration(new DividerItemDecoration(getActivity(),
                                                               LinearLayoutManager.HORIZONTAL));
    }
}
