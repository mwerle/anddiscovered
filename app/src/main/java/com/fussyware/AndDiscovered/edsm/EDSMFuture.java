package com.fussyware.AndDiscovered.edsm;

import com.fussyware.AndDiscovered.TrilatResponse;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by wes on 8/21/15.
 */
class EDSMFuture implements Future<TrilatResponse>
{
    private final Future<EDSMSubmitResponse> future;

    EDSMFuture(Future<EDSMSubmitResponse> future)
    {
        this.future = future;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning)
    {
        return future.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled()
    {
        return future.isCancelled();
    }

    @Override
    public boolean isDone()
    {
        return future.isDone();
    }

    @Override
    public TrilatResponse get() throws InterruptedException, ExecutionException
    {
        return new InnerTrilatResponse(future.get());
    }

    @Override
    public TrilatResponse get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException
    {
        return new InnerTrilatResponse(future.get(timeout, unit));
    }

    private class InnerTrilatResponse implements TrilatResponse
    {
        private EDSMSubmitResponse response;

        public InnerTrilatResponse(EDSMSubmitResponse response)
        {
            this.response = response;
        }

        @Override
        public Position getPosition()
        {
            return isTrilatPerformed() ? response.getBaseResponse().position : null;
        }

        @Override
        public int getDistancesProcessed()
        {
            return (response == null) ? -1 : response.getDistancesProcessed().size();
        }

        @Override
        public boolean isTrilatPerformed()
        {
            return (response != null) && response.isTrilatPerformed();
        }
    }
}
