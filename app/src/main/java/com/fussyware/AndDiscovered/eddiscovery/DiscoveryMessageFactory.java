package com.fussyware.AndDiscovered.eddiscovery;

/**
 * Created by wes on 6/14/15.
 */
public class DiscoveryMessageFactory
{
    public static DiscoveryMessage getMessage(String json)
    {
        DiscoveryMessage msg = null;

        msg = AnnounceMessage.getMessage(json);
        if (null != msg) return msg;

        msg = QueryMessage.getMessage(json);
        if (null != msg) return msg;

        return null;
    }
}
