package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;

import com.fussyware.AndDiscovered.AndDiscoveredActivity;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.CheckDialogFragment;
import com.fussyware.AndDiscovered.dialog.TextPickerDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.fragment.CelestialInfoFragment;
import com.fussyware.AndDiscovered.fragment.DistanceListFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.NotesFragment;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

/**
 * Created by wes on 9/29/15.
 */
public class SystemInfoActivity
        extends BaseActivity
        implements CheckDialogFragment.OnDoneListener,
                   TextPickerDialogFragment.OnTextChangedListener,
                   DistanceListFragment.OnSystemLongClickListener,
                   DistanceListFragment.OnPositionTrilaterationListener,
                   NotesFragment.OnDoneListener

{
    private static final String LOG_NAME = SystemInfoActivity.class.getSimpleName();

    private static final int RESULT_CELESTIAL = 1;
    private static final int RESULT_DISTANCE = 2;
    private static final int RESULT_NOTES = 3;
    private static final int RESULT_PHOTO = 4;

    private CmdrDbHelper dbHelper;

    private TitleInfoFragment titleFragment;
    private CelestialInfoFragment celestialFragment;
    private DistanceListFragment distanceFragment;
    private NotesFragment notesFragment;

    private final ArrayList<String> systemsModified = new ArrayList<>();
    private final Stack<CmdrSystemInfo> backStack = new Stack<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.system_info_activity_title);
        setContentView(R.layout.system_info_layout);
        setDrawerIndicatorEnabled(false);

        dbHelper = CmdrDbHelper.getInstance();

        titleFragment = (TitleInfoFragment) getFragmentManager().findFragmentById(R.id.title_info_fragment);
        distanceFragment = (DistanceListFragment) getFragmentManager().findFragmentById(R.id.distance_list_fragment);
        notesFragment = (NotesFragment) getFragmentManager().findFragmentById(R.id.notes_fragment);
        celestialFragment = (CelestialInfoFragment) getFragmentManager().findFragmentById(R.id.celestial_info_fragment);

        createCelestialButton();
        createDistanceButton();
        createNotesButton();
        createPhotoGalleryButton();

        if (savedInstanceState == null) {
            CmdrSystemInfo system = getIntent().getParcelableExtra("system");

            // TODO: OK this is stupid, but I need to change everything here. Punt, but convert later.
            setSystem(system.getSystem());
        } else {
            synchronized (backStack) {
                // TODO: Convert this whole thing to use a Data fragment.
                String[] values = savedInstanceState.getStringArray("back_stack");

                for (String item : values) {
                    backStack.push(dbHelper.getSystem(item));
                }

                if (!backStack.empty()) {
                    setSystem(backStack.peek());
                } else {
                    setSystem(getIntent().getStringExtra("system"));
                }
            }

            synchronized (systemsModified) {
                Collections.addAll(systemsModified,
                                   savedInstanceState.getStringArray("modified_list"));
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        CmdrSystemInfo system = intent.getParcelableExtra("system");
        // TODO: Yep, stupid. Redo.
        setSystem(system.getSystem());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RESULT_CELESTIAL:
                case RESULT_DISTANCE:
                case RESULT_NOTES:
                    if (data.getBooleanExtra("modified", false)) {
                        synchronized (systemsModified) {
                            synchronized (backStack) {
                                if (!systemsModified.contains(backStack.peek().getSystem())) {
                                    systemsModified.add(backStack.peek().getSystem());
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        ArrayList<String> list = new ArrayList<>();

        synchronized (backStack) {
            for (CmdrSystemInfo info : backStack) {
                list.add(0, info.getSystem());
            }
        }

        outState.putStringArray("back_stack", list.toArray(new String[list.size()]));

        synchronized (systemsModified) {
            outState.putStringArray("modified_list",
                                    systemsModified.toArray(new String[systemsModified.size()]));
        }
    }

    @Override
    protected void onDestroy()
    {
        backStack.clear();

        super.onDestroy();
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        Intent intent = new Intent(getApplicationContext(), AndDiscoveredActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        synchronized (systemsModified) {
            if (!systemsModified.isEmpty()) {
                intent.putExtra("modified_list",
                                systemsModified.toArray(new String[systemsModified.size()]));

                setResult(Activity.RESULT_OK, intent);
            }
        }

        startActivity(intent);

        return true;
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            synchronized (backStack) {
                backStack.pop();

                if (backStack.empty()) {
                    synchronized (systemsModified) {
                        if (systemsModified.isEmpty()) {
                            super.onBackPressed();
                        } else {
                            Intent intent = new Intent();
                            intent.putExtra("modified_list",
                                            systemsModified.toArray(new String[systemsModified.size()]));

                            setResult(Activity.RESULT_OK, intent);

                            finish();
                        }
                    }
                } else {
                    setSystem(backStack.peek());
                }
            }
        }
    }

    @Override
    protected void onNewSystem(SystemInfo system)
    {
        setSystem(system.getSystem());
    }

    @Override
    protected void onUpdateSystems()
    {
        CmdrSystemInfo info = dbHelper.getSystem(backStack.peek().getSystem());

        synchronized (backStack) {
            backStack.pop();
            backStack.push(info);
        }
    }

    @Override
    public void onDone(String tag, int count, CheckDialogFragment.CheckListItem[] items)
    {
        switch (tag) {
            case FragmentTag.celestial_info_fragment.star_check_list_dialog:
                if (celestialFragment != null) {
                    celestialFragment.onStarDialogDone(count, items);

                    synchronized (systemsModified) {
                        synchronized (backStack) {
                            if (!systemsModified.contains(backStack.peek().getSystem())) {
                                systemsModified.add(backStack.peek().getSystem());
                            }
                        }
                    }
                }
                break;
            case FragmentTag.celestial_info_fragment.planet_check_list_dialog:
                if (celestialFragment != null) {
                    celestialFragment.onPlanetDialogDone(count, items);

                    synchronized (systemsModified) {
                        synchronized (backStack) {
                            if (!systemsModified.contains(backStack.peek().getSystem())) {
                                systemsModified.add(backStack.peek().getSystem());
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.notes_fragment.tag:
                synchronized (systemsModified) {
                    synchronized (backStack) {
                        if (!systemsModified.contains(backStack.peek().getSystem())) {
                            systemsModified.add(backStack.peek().getSystem());
                        }
                    }
                }

                break;
            case FragmentTag.distance_list_fragment.distance_update_dialog:
                if (distanceFragment != null) {
                    distanceFragment.onDistanceUpdateDialogDone(text);

                    synchronized (systemsModified) {
                        synchronized (backStack) {
                            if (!systemsModified.contains(backStack.peek().getSystem())) {
                                systemsModified.add(backStack.peek().getSystem());
                            }
                        }
                    }
                }
                break;
            case FragmentTag.celestial_info_fragment.main_star_dialog:
                if (celestialFragment != null) {
                    celestialFragment.onMainStarDialogDone(text);

                    synchronized (systemsModified) {
                        synchronized (backStack) {
                            if (!systemsModified.contains(backStack.peek().getSystem())) {
                                systemsModified.add(backStack.peek().getSystem());
                            }
                        }
                    }
                }
                break;
            default:
                super.onDone(tag, text);
                break;
        }
    }

    @Override
    public void onPositionCalculation(String text)
    {
        titleFragment.onPositionCalculation(text);
    }

    @Override
    public void onPositionFound(Position position)
    {
        titleFragment.onPositionFound(position);

        synchronized (systemsModified) {
            if (!systemsModified.contains(backStack.peek().getSystem())) {
                systemsModified.add(backStack.peek().getSystem());
            }
        }
    }

    @Override
    public void onSystemLongClick(String system)
    {
        setSystem(system);
    }

    public void setSystem(@NonNull String system)
    {
        synchronized (backStack) {
            CmdrSystemInfo systemInfo = dbHelper.getSystem(system);

            if (systemInfo == null) {
                // TODO: This should throw up a dialog giving warning that we could not find the system.
            } else if (backStack.empty() || !systemInfo.equals(backStack.peek())) {
                backStack.push(systemInfo);
                setSystem(systemInfo);
            }
        }
    }

    private void setSystem(final CmdrSystemInfo systemInfo)
    {
        titleFragment.setSystem(systemInfo);

        if (distanceFragment != null) {
            distanceFragment.setSystem(systemInfo);
        }

        if (notesFragment != null) {
            notesFragment.setSystem(systemInfo);
        }

        if (celestialFragment != null) {
            celestialFragment.setSystem(systemInfo);
        }
    }

    private void createCelestialButton()
    {
        View view = findViewById(R.id.celestial_button);

        if (view != null) {
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(getApplicationContext(),
                                               CelestialListActivity.class);

                    synchronized (backStack) {
                        intent.putExtra("system", backStack.peek().getSystem());
                    }

                    startActivityForResult(intent, RESULT_CELESTIAL);
                }
            });
        }
    }

    private void createDistanceButton()
    {
        View view = findViewById(R.id.distance_button);

        if (view != null) {
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(getApplicationContext(), CoordinateCalculatorActivity.class);

                    synchronized (backStack) {
                        intent.putExtra("system", backStack.peek().getSystem());
                    }

                    startActivityForResult(intent, RESULT_DISTANCE);
                }
            });
        }
    }

    private void createNotesButton()
    {
        View view = findViewById(R.id.notes_button);

        if (view != null) {
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(getApplicationContext(), JournalActivity.class);

                    synchronized (backStack) {
                        intent.putExtra("system", backStack.peek().getSystem());
                    }

                    startActivityForResult(intent, RESULT_NOTES);
                }
            });
        }
    }

    private void createPhotoGalleryButton()
    {
        View view = findViewById(R.id.photo_gallery_button);

        if (view != null) {
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(getApplicationContext(), SystemPhotoGalleryActivity.class);

                    synchronized (backStack) {
                        intent.putExtra("system", backStack.peek().getSystem());
                    }

                    startActivityForResult(intent, RESULT_PHOTO);
                }
            });
        }
    }
}
