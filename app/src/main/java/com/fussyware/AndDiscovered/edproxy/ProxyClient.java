package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.fussyware.AndDiscovered.json.JsonParser;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by wes on 6/14/15.
 */
public class ProxyClient
{
    private static final String LOG_NAME = ProxyClient.class.getSimpleName();

    private final Socket sock;
    private final OutputStream sockOutput;
    private final ArrayList<ProxyListener> listeners = new ArrayList<ProxyListener>();

    private boolean running;

    private final Lock lock = new ReentrantLock();
    private final Condition heartbeatCond = lock.newCondition();

    private final Object mutex = new Object();

    public ProxyClient(String hostname, int port) throws IOException
    {
        sock = new Socket(hostname, port);
        sock.setReuseAddress(true);
        sock.setKeepAlive(true);

        sockOutput = sock.getOutputStream();

        running = true;

        new Thread(new ClientThread()).start();
    }

    public void addListener(ProxyListener listener)
    {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public void startHeartbeat(final int timeout, final TimeUnit units)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                Log.d(LOG_NAME, "Starting up heartbeat thread.");
                while (isRunning()) {
                    try {
                        lock.lock();
                        if (!heartbeatCond.await(timeout, units)) {
                            if (isRunning()) {
                                send(new HeartbeatMessage());

                                if (!heartbeatCond.await(5, TimeUnit.SECONDS)) {
                                    close();
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        Log.e(LOG_NAME, "Interrupted by the system in heartbeat sender.", e);
                        close();
                    } catch (IOException e) {
                        Log.e(LOG_NAME, "IOException when sending Heartbeat message.", e);
                        close();
                    } finally {
                        lock.unlock();
                    }
                }

                Log.d(LOG_NAME, "Leaving heartbeat thread.");
            }
        }).start();
    }

    public boolean isRunning()
    {
        synchronized (mutex) {
            return running;
        }
    }

    public void close()
    {
        synchronized (mutex) {
            if (running) {
                running = false;

                try {
                    lock.lock();
                    heartbeatCond.signalAll();
                } finally {
                    lock.unlock();
                }

                try {
                    sock.shutdownInput();
                } catch (IOException ignored) {
                }

                try {
                    sock.shutdownOutput();
                    sockOutput.close();
                } catch (IOException ignored) {
                }

                try {
                    sock.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    public void send(ProxyMessage message) throws IOException
    {
        sockOutput.write(message.getJSON().getBytes());
    }

    private class ClientThread implements Runnable {

        private ArrayList<Object> readArray(JsonReader json)
        {
            ArrayList<Object> object = new ArrayList<Object>();

            while (isRunning()) {
                try {
                    JsonToken token = json.peek();

                    switch (token) {
                        case BEGIN_ARRAY:
                            json.beginArray();
                            if (json.hasNext()) {
                                ArrayList<Object> list = readArray(json);
                                if (null != list) {
                                    object.add(list);
                                }
                            }

                            continue;
                        case BEGIN_OBJECT:
                            json.beginObject();
                            if (json.hasNext()) {
                                HashMap<String, Object> map = readObject(json);
                                if (null != map) {
                                    object.add(map);
                                }
                            }

                            continue;
                        case END_ARRAY:
                            json.endArray();
                            return object;
                        case END_DOCUMENT:
                            synchronized (mutex) {
                                running = false;
                            }

                            continue;
                        default:
                            break;
                    }

                    if (json.hasNext()) {
                        object.add(json.nextString());
                    }
                } catch (IOException e) {
                    Log.e(LOG_NAME, "Failed parsing JSON array.", e);
                    close();
                }
            }

            return null;
        }

        private HashMap<String, Object> readObject(JsonReader json) {
            HashMap<String, Object> object = new HashMap<String, Object>();

            String name = null;

            while (isRunning()) {
                try {
                    JsonToken token = json.peek();

                    switch (token) {
                        case BEGIN_ARRAY:
                            json.beginArray();
                            if (json.hasNext()) {
                                ArrayList<Object> list = readArray(json);
                                if (null != list) {
                                    object.put(name, list);
                                }
                            }

                            continue;
                        case BEGIN_OBJECT:
                            json.beginObject();
                            if (json.hasNext()) {
                                HashMap<String, Object> map = readObject(json);
                                if (null != map) {
                                    object.put(name, map);
                                }
                            }

                            continue;
                        case END_OBJECT:
                            json.endObject();
                            return object;
                        case END_DOCUMENT:
                            synchronized (mutex) {
                                running = false;
                            }

                            continue;
                        case NAME:
                            name = json.nextName();
                            continue;
                        default:
                            break;
                    }

                    if (json.hasNext()) {
                        String value = json.nextString();

                        switch (name) {
                            case "Type":
                                for (ProxyMessageType type : ProxyMessageType.values()) {
                                    if (type.toString().equals(value)) {
                                        object.put(name, type);
                                        break;
                                    }
                                }
                                break;
                            case "Date":
                                try {
                                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",
                                                                             Locale.US);
                                    object.put(name, format.parse(value));
                                } catch (ParseException e) {
                                    object.put(name, new Date());
                                }
                                break;
                            default:
                                object.put(name, value);
                                break;
                        }
                    }
                } catch (IOException e) {
                    Log.e(LOG_NAME, "Failed parsing JSON object.", e);
                    close();
                }
            }

            return null;
        }

        @Override
        public void run()
        {
            JsonParser parser = null;

            try {
                parser = new JsonParser(new InputStreamReader(sock.getInputStream()));
            } catch (IOException e) {
                Log.e(LOG_NAME, "Failed creating json reader.", e);
                close();
            }

            while (isRunning()) {
                try {
                    JSONObject object = parser.getBaseObject();

                    if (null != object) {
                        ProxyMessage msg = ProxyMessageFactory.getMessage(object);
                        if (null != msg) {
                            if (msg.getType() == ProxyMessageType.Pong) {
                                try {
                                    lock.lock();
                                    heartbeatCond.signal();
                                } finally {
                                    lock.unlock();
                                }
                            } else {
                                synchronized (listeners) {
                                    for (ProxyListener listener : listeners) {
                                        listener.messageReceived(msg);
                                    }
                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    Log.d("ProxyClient", "Something bad happened while reading from the Proxy Server.", e);

                    close();
                }
            }

            Log.d("ProxyClient", "Closing down proxy client.");
            close();
        }
    }
}
