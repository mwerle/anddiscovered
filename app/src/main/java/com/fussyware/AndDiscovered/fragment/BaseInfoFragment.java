package com.fussyware.AndDiscovered.fragment;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.fussyware.AndDiscovered.EDProxyIntentService;
import com.fussyware.AndDiscovered.StarCoordinator;
import com.fussyware.AndDiscovered.celestial.SystemInfo;

/**
 * Created by wes on 10/4/15.
 */
public abstract class BaseInfoFragment extends Fragment
{
    private LocalBroadcastManager broadcastManager;
    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        broadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String action = intent.getAction();

                switch (action) {
                    case EDProxyIntentService.BROADCAST_SYSTEM_UPDATED: {
                        SystemInfo info = intent.getParcelableExtra("system");
                        onNewSystem(info);
                        break;
                    }
                    case StarCoordinator.BROADCAST_SYSTEM_UPDATED: {
                        onUpdateSystems();
                        break;
                    }
                    default:
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(EDProxyIntentService.BROADCAST_SYSTEM_UPDATED);
        filter.addAction(StarCoordinator.BROADCAST_SYSTEM_UPDATED);

        broadcastManager = LocalBroadcastManager.getInstance(getActivity().getApplicationContext());
        broadcastManager.registerReceiver(broadcastReceiver, filter);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }

    protected void onNewSystem(SystemInfo system)
    {

    }

    protected void onUpdateSystems()
    {

    }
}
