package com.fussyware.AndDiscovered.edsc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wes on 6/28/15.
 */
public class EDSCSubmitResponse
{
    private static final String LOG_NAME = EDSCSubmitResponse.class.getSimpleName();

    private int systemsProcessed;
    private int distancesProcessed;
    private boolean trilatPerformed;

    public EDSCSubmitResponse(JSONObject json) throws JSONException
    {
        JSONObject object = json.getJSONObject("status");
        JSONArray array = object.getJSONArray("system");
        systemsProcessed = array.length();

        array = object.getJSONArray("dist");
        distancesProcessed = array.length();

        array = object.getJSONArray("trilat");
        trilatPerformed = (array.length() > 0);
    }

    public int getSystemsProcessed()
    {
        return systemsProcessed;
    }

    public int getDistancesProcessed()
    {
        return distancesProcessed;
    }

    public boolean isTrilatPerformed()
    {
        return trilatPerformed;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("EDSCSubmitResponse { Systems Processesed [");
        sb.append(systemsProcessed);
        sb.append("], Distances Processed [");
        sb.append(distancesProcessed);
        sb.append("], Trilat Performed [");
        sb.append(trilatPerformed);
        sb.append("]}");

        return sb.toString();
    }
}
