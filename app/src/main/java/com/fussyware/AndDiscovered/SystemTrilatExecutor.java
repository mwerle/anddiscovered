package com.fussyware.AndDiscovered;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrDistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by wes on 9/2/15.
 */
public class SystemTrilatExecutor
{
    private static final String LOG_NAME = SystemTrilatExecutor.class.getSimpleName();

    private final StarCoordinator starCoordinator;
    private final ThreadPoolExecutor executor;
    private final CmdrDbHelper dbHelper;

    private final ArrayList<TrilatListener> listeners = new ArrayList<>();

    public SystemTrilatExecutor(CmdrDbHelper dbHelper,
                                StarCoordinator starCoordinator)
    {
        this.starCoordinator = starCoordinator;
        this.dbHelper = dbHelper;

        executor = new ThreadPoolExecutor(10, 10, 15, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        executor.allowCoreThreadTimeOut(true);
    }

    public void registerTrilatListener(TrilatListener listener)
    {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public void unregisterTrilatListener(TrilatListener listener)
    {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    public void submit(String system)
    {
        executor.submit(new TrilatRunner(system, this));
    }

    private static class TrilatRunner implements Runnable
    {
        private final String system;
        private final SystemTrilatExecutor trilatExecutor;

        public TrilatRunner(String system,
                            SystemTrilatExecutor trilatExecutor)
        {
            this.system = system;
            this.trilatExecutor = trilatExecutor;
        }

        @Override
        public void run()
        {
            ArrayList<CmdrDistanceInfo> masterList = new ArrayList<CmdrDistanceInfo>();
            List<CmdrDistanceInfo> distances = trilatExecutor.dbHelper.getDistances(system, false);

            masterList.addAll(trilatExecutor.dbHelper.getReferenceDistances(system));

            for (CmdrDistanceInfo info : distances) {
                if ((info.getSecondSystem() != null) && (info.getDistance() != 0.0)) {
                    CmdrSystemInfo sysinfo = trilatExecutor.dbHelper.getSystem(info.getSecondSystem());

                    if ((sysinfo.getPosition() != null) && !masterList.contains(info)) {
                        masterList.add(info);
                    }
                }
            }

            TrilatPostRunner postRunner;

            try {
                Future<TrilatResponse> future = trilatExecutor.starCoordinator.trilatSystem(system, masterList, true);
                CmdrSystemInfo info = trilatExecutor.dbHelper.getSystem(system);

                final TrilatResponse response = future.get();
                if (response.isTrilatPerformed()) {
                    Log.d(LOG_NAME, "Trilat was successful for system [" + system + "]");
                    info.setPosition(response.getPosition());

                    postRunner = new TrilatPostRunner(trilatExecutor, system, response.getPosition());
                } else if (response.getDistancesProcessed() > 0) {
                    Log.d(LOG_NAME, "Trilat was NOT successful for system [" + system + "]. Raising min system to [" + Integer.toString(response.getDistancesProcessed() + 1) + "]");
                    info.setMinSystemsRequiredForTrilat(response.getDistancesProcessed());

                    postRunner = new TrilatPostRunner(trilatExecutor, system, TrilatListener.MIN_SYSTEMS_RAISED);
                } else {
                    Log.d(LOG_NAME, "Trilat was NOT successful for system [" + system + "] too few systems provided.");
                    postRunner = new TrilatPostRunner(trilatExecutor, system, TrilatListener.MIN_SYSTEMS_NOT_MET);
                }
            } catch (final CancellationException e) {
                Log.d(LOG_NAME, "Trilat of system [" + system + "] was cancelled.");
                postRunner = new TrilatPostRunner(trilatExecutor, system, TrilatListener.CANCELLED);
            } catch (final Exception e) {
                Log.e(LOG_NAME, "Failed trilat of system [" + system + "].", e);
                postRunner = new TrilatPostRunner(trilatExecutor, system, e);
            }

            new Handler(Looper.getMainLooper()).post(postRunner);
        }
    }

    private static class TrilatPostRunner implements Runnable
    {
        private final SystemTrilatExecutor executor;
        private final String system;
        private final int code;
        private final Position position;
        private final Throwable err;

        TrilatPostRunner(SystemTrilatExecutor executor, String system, int code)
        {
            this.executor = executor;
            this.system = system;

            this.code = code;
            this.position = null;
            this.err = null;
        }

        TrilatPostRunner(SystemTrilatExecutor executor, String system, Position position)
        {
            this.executor = executor;
            this.system = system;

            this.code = -1;
            this.position = position;
            this.err = null;
        }

        TrilatPostRunner(SystemTrilatExecutor executor, String system, Throwable err)
        {
            this.executor = executor;
            this.system = system;

            this.code = -1;
            this.position = null;
            this.err = err;
        }

        @Override
        public void run()
        {
            synchronized (executor.listeners) {
                for (TrilatListener listener : executor.listeners) {
                    if (code != -1) {
                        listener.onFailure(system, code);
                    } else if (position != null) {
                        listener.onSuccess(system, position);
                    } else if (err != null) {
                        listener.onError(system, err);
                    }
                }
            }
        }
    }
}
