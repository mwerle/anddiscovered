package com.fussyware.AndDiscovered.celestial;

import android.os.Parcel;
import android.os.Parcelable;

import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wboyd on 7/7/15.
 */
public abstract class SystemInfo implements Parcelable
{
    private final String system;
    private Position xyz;

    private final Object mutex = new Object();

    public SystemInfo(String system, Position xyz)
    {
        this.system = system;
        this.xyz = xyz;
    }

    protected SystemInfo(Parcel parcel)
    {
        system = parcel.readString();
        xyz = (Position) parcel.readValue(Position.class.getClassLoader());
    }

    public String getSystem()
    {
        return system;
    }

    public Position getPosition()
    {
        synchronized (mutex) {
            return xyz;
        }
    }

    public List<CelestialBody> getSatellites()
    {
        return new ArrayList<>();
    }

    protected void setPosition(Position xyz)
    {
        synchronized (mutex) {
            this.xyz = xyz;
        }
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(system);
        dest.writeValue(xyz);
    }
}
