package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.Atmosphere;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by wes on 11/17/15.
 */
public class CmdrPlanetBody extends PlanetBody
{
    public static final Creator<CmdrPlanetBody> CREATOR = new Creator<CmdrPlanetBody>()
    {
        @Override
        public CmdrPlanetBody createFromParcel(Parcel source)
        {
            return new CmdrPlanetBody(source);
        }

        @Override
        public CmdrPlanetBody[] newArray(int size)
        {
            return new CmdrPlanetBody[size];
        }
    };

    private static final Object createMutex = new Object();

    private final SQLiteDatabase db;
    private final Long id;

    public CmdrPlanetBody(@NonNull SQLiteDatabase db,
                          @NonNull CmdrSystemInfo system,
                          @NonNull PlanetType type,
                          @NonNull ScanLevel scanLevel,
                          long id,
                          CelestialBody parent,
                          String name,
                          Double distance,
                          Double mass,
                          Double radius,
                          Double surfaceTemp,
                          Double surfacePressure,
                          Boolean volcanism,
                          Atmosphere atmosphereType,
                          Double orbitalPeriod,
                          Double semiMajorAxis,
                          Double orbitalEccentricity,
                          Double orbitalInclination,
                          Double argPeriapsis,
                          Double rotationPeriod,
                          Boolean tidalLocked, Double axisTilt)
    {
        super(system,
              type,
              scanLevel,
              parent,
              name,
              distance,
              mass,
              radius,
              surfaceTemp,
              surfacePressure,
              volcanism,
              atmosphereType,
              orbitalPeriod,
              semiMajorAxis,
              orbitalEccentricity,
              orbitalInclination,
              argPeriapsis,
              rotationPeriod,
              tidalLocked,
              axisTilt);

        if (db.isReadOnly()) {
            throw new InvalidParameterException("A valid writable database must be provided.");
        }

        this.db = db;
        this.id = id;
    }

    private CmdrPlanetBody(@NonNull SQLiteDatabase db,
                           @NonNull CmdrSystemInfo system,
                           @NonNull PlanetType type,
                           long id,
                           CelestialBody parent)
    {
        super(system, type, parent);

        this.db = db;
        this.id = id;
    }

    private CmdrPlanetBody(Parcel parcel)
    {
        super(parcel);

        db = CmdrDbHelper.getInstance().getWritableDatabase();
        id = parcel.readLong();
    }

    public static CmdrPlanetBody create(@NonNull CmdrSystemInfo system,
                                        @NonNull PlanetType type)
    {
        return create(system, type, null);
    }

    public static CmdrPlanetBody create(@NonNull CmdrSystemInfo system,
                                        @NonNull PlanetType type,
                                        CelestialBody parent)
    {
        if (type == PlanetType.Unknown) {
            throw new InvalidParameterException("A valid planet type must be specified during creation.");
        }

        synchronized (createMutex) {
            SQLiteDatabase db = CmdrDbHelper.getInstance().getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(CmdrDbContract.Planets.COLUMN_NAME_TYPE, type.value());
            values.put(CmdrDbContract.Planets.COLUMN_NAME_SCANLEVEL, 2);

            long id = db.insert(CmdrDbContract.Planets.TABLE_NAME, null, values);

            values.clear();

            values.put(CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID, system.getRowId());
            values.put(CmdrDbContract.Satellites.COLUMN_NAME_BODYID, id);
            values.put(CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
                       SatelliteCategory.Planet.value);

            if (parent != null) {
                long _id = -1;

                if (parent.getId() instanceof Long) {
                    _id = (Long) parent.getId();
                }

                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTID, _id);
                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID,
                           parent.getSatelliteCategory().value);
            }

            db.insert(CmdrDbContract.Satellites.TABLE_NAME, null, values);

            return new CmdrPlanetBody(db, system, type, id, parent);
        }
    }

    @Override
    public Object getId()
    {
        return id;
    }

    @Override
    public List<CelestialBody> getSatellites()
    {
        return CmdrDbHelper.getInstance()
                           .getSatellitesOf((CmdrSystemInfo) getSystem(), this);
    }

    @Override
    public boolean hasSatellites()
    {
        return CmdrDbHelper.getInstance()
                           .hasSatellites(((CmdrSystemInfo) getSystem()).getRowId(),
                                          id,
                                          getSatelliteCategory());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeLong(id);
    }

    @Override
    public void delete()
    {

    }

    @Override
    protected void updateParent(CelestialBody body)
    {

    }

    @Override
    protected void updateMass(double mass)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_MASS, mass);

        write(values);
    }

    @Override
    protected void updateRadius(double radius)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_RADIUS, radius);

        write(values);
    }

    @Override
    protected void updateSurfaceTemp(double surfaceTemp)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_SURFACETEMP, surfaceTemp);

        write(values);
    }

    @Override
    protected void updateSurfacePressure(double surfacePressure)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_SURFACEPRESSURE, surfacePressure);

        write(values);
    }

    @Override
    protected void updateVolcanism(boolean volcanism)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_VOLCANISM, volcanism);

        write(values);
    }

    @Override
    protected void updateAtmosphereType(Atmosphere atmosphereType)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_ATMOSPHERETYPE, atmosphereType.value);

        write(values);
    }

    @Override
    protected void updateRotationPeriod(double rotationPeriod)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_ROTATIONPERIOD, rotationPeriod);

        write(values);
    }

    @Override
    protected void updateTidalLocked(boolean tidalLocked)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_TIDALLOCKED, tidalLocked);

        write(values);
    }

    @Override
    protected void updateAxisTilt(double axisTilt)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_AXISTILT, axisTilt);

        write(values);
    }

    @Override
    protected void updateName(String name)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_NAME, name);

        write(values);
    }

    @Override
    protected void updateDistance(double distance)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_DISTANCE, distance);

        write(values);
    }

    @Override
    protected void updateScanLevel(ScanLevel scanLevel)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_SCANLEVEL, scanLevel.value);

        write(values);
    }

    @Override
    protected void updateSemiMajorAxis(double semiMajorAxis)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_SEMIMAJORAXIS, semiMajorAxis);

        write(values);
    }

    @Override
    protected void updateOrbitalPeriod(double orbitalPeriod)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_ORBITALPERIOD, orbitalPeriod);

        write(values);
    }

    @Override
    protected void updateOrbitalEccentricity(double orbitalEccentricity)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_ORBITALECCENTRICITY, orbitalEccentricity);

        write(values);
    }

    @Override
    protected void updateOrbitalInclination(double orbitalInclination)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_ORBITALINCLINATION, orbitalInclination);

        write(values);
    }

    @Override
    protected void updateArgPeriapsis(double argPeriapsis)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_ARGPERIAPSIS, argPeriapsis);

        write(values);
    }

    private void write(ContentValues values)
    {
        String selection = CmdrDbContract.Planets._ID + "=?";
        String[] selectionArgs = { id.toString() };

        db.update(CmdrDbContract.Planets.TABLE_NAME, values, selection, selectionArgs);
    }
}
