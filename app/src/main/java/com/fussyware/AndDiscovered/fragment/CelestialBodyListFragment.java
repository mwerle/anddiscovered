package com.fussyware.AndDiscovered.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.CelestialSatelliteAdapter;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;

import java.util.List;

/**
 * Created by wes on 9/30/15.
 */
public class CelestialBodyListFragment extends Fragment
{
    private static final String LOG_NAME = CelestialBodyListFragment.class.getSimpleName();

    private HolderFragment holderFragment;
    private CmdrSystemInfo selectedSystem;
    private CelestialBodyListener listener;

    private final Object mutex = new Object();

    public interface CelestialBodyListener {
        void onCelestialBodyItemClick(int position, View view, CelestialBody body);
        boolean onCelestialBodyItemLongClick(int position, View view, CelestialBody body);
        boolean onCelestialBodyDrag(View view, DragEvent event, CelestialBody body);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof CelestialBodyListener) {
            listener = (CelestialBodyListener) activity;
        } else {
            throw new ClassCastException("Activity does not implement Listeners.");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        holderFragment = (HolderFragment) getFragmentManager().findFragmentByTag(HolderFragment.HOLDER_FRAGMENT);
        if (holderFragment == null) {
            holderFragment = new HolderFragment();
            holderFragment.setRetainInstance(true);
            getFragmentManager()
                    .beginTransaction()
                    .add(holderFragment, HolderFragment.HOLDER_FRAGMENT)
                    .commit();
        }

        View layout = inflater.inflate(R.layout.celestial_body_list_layout,
                                       container,
                                       false);

        layout.setOnDragListener(new View.OnDragListener()
        {
            @Override
            public boolean onDrag(View v, DragEvent event)
            {
                synchronized (mutex) {
                    return ((listener != null) &&
                            listener.onCelestialBodyDrag(v, event, holderFragment.root));
                }
            }
        });

        if (savedInstanceState == null) {
            holderFragment.adapter = new CelestialSatelliteAdapter(getActivity().getApplicationContext());
            holderFragment.adapter.setSatelliteClickListener(new CelestialSatelliteAdapter.SatelliteListener()
            {
                @Override
                public void onSatelliteClick(int position, View view, CelestialBody body)
                {
                    synchronized (mutex) {
                        if (listener != null) {
                            listener.onCelestialBodyItemClick(position, view, body);
                        }
                    }
                }

                @Override
                public boolean onSatelliteLongClick(int position, View view, CelestialBody body)
                {
                    synchronized (mutex) {
                        return ((listener != null) &&
                                listener.onCelestialBodyItemLongClick(position, view, body));
                    }
                }

                @Override
                public boolean onSatelliteDrag(View view, DragEvent event, CelestialBody body)
                {
                    synchronized (mutex) {
                        return ((listener != null) &&
                                listener.onCelestialBodyDrag(view, event, body));
                    }
                }
            });
        } else {
            selectedSystem = CmdrDbHelper.getInstance()
                                         .getSystem(savedInstanceState.getString("system"));
        }

        RecyclerView satView = (RecyclerView) layout.findViewById(R.id.celestial_body_list);
        satView.setAdapter(holderFragment.adapter);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString("system", selectedSystem.getSystem());
    }

    public void add(CelestialBody body)
    {
        holderFragment.adapter.add(body);
    }

    public void remove(int position)
    {
        holderFragment.adapter.remove(position);
    }

    public void setSystem(@NonNull CmdrSystemInfo systemInfo)
    {
        setSystem(systemInfo, null);
    }

    public void setSystem(@NonNull CmdrSystemInfo systemInfo, CelestialBody root)
    {
        synchronized (mutex) {
            if (!systemInfo.equals(selectedSystem)) {
                selectedSystem = systemInfo;
                holderFragment.root = root;

                setBodies(systemInfo, root);
            }
        }
    }

    public void setCelestialRoot(CelestialBody root)
    {
        synchronized (mutex) {
            if (root != holderFragment.root) {
                holderFragment.root = root;
                setBodies(selectedSystem, root);
            }
        }
    }

    private void setBodies(@NonNull  CmdrSystemInfo systemInfo, CelestialBody root)
    {
        List<CelestialBody> bodyList;

        if (root == null) {
            bodyList = systemInfo.getSatellites();
        } else {
            bodyList = root.getSatellites();
        }

        holderFragment.adapter.set(bodyList);
    }

    public static class HolderFragment extends Fragment
    {
        public static final String HOLDER_FRAGMENT = "celestial_body_list_fragment.holder_fragment";

        private CelestialSatelliteAdapter adapter;
        public CelestialBody root;
    }

}
