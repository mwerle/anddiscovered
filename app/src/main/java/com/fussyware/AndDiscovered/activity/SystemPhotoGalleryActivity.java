package com.fussyware.AndDiscovered.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.MenuItem;

import com.fussyware.AndDiscovered.EDProxyIntentService;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.dialog.CancelableAlertDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.SystemImageInfo;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.SystemPhotoGalleryFragment;

/**
 * Created by wes on 9/30/15.
 */
public class SystemPhotoGalleryActivity
        extends BaseActivity
        implements CancelableAlertDialogFragment.OnAlertListener

{
    private String systemName;

    private SystemPhotoGalleryFragment systemGalleryFragment;

    private LocalBroadcastManager broadcastManager;
    private final BroadcastReceiver broadcastReceiver = new BroadcastIntentReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.system_photo_gallery_activity_title);
        setContentView(R.layout.system_photo_gallery_activity_layout);
        setDrawerIndicatorEnabled(false);

        systemGalleryFragment = (SystemPhotoGalleryFragment) getFragmentManager().findFragmentById(R.id.system_photo_gallery_fragment);

        if (savedInstanceState == null) {
            systemName = getIntent().getStringExtra("system");
        } else {
            systemName = savedInstanceState.getString("system");
        }

        systemGalleryFragment.setSystem(systemName);

        IntentFilter filter = new IntentFilter();
        filter.addAction(EDProxyIntentService.BROADCAST_IMAGE_EVENT);

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastManager.registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString("system", systemName);
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        super.onBackPressed();
        return true;
    }

    @Override
    public void onPositive(String tag)
    {
        switch (tag) {
            case FragmentTag.system_photo_gallery_fragment.alert_delete_dialog:
                systemGalleryFragment.onPositive(tag);
                break;
            default:
                break;
        }
    }

    @Override
    public void onNegative(String tag)
    {
        switch (tag) {
            case FragmentTag.system_photo_gallery_fragment.alert_delete_dialog:
                systemGalleryFragment.onNegative(tag);
                break;
            default:
                break;
        }
    }

    private class BroadcastIntentReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            switch (action) {
                case EDProxyIntentService.BROADCAST_IMAGE_EVENT: {
                    SystemImageInfo info = intent.getParcelableExtra(EDProxyIntentService.EXTRA_IMAGE_INFO);
                    systemGalleryFragment.add(info);

                    break;
                }
                default:
                    break;
            }
        }
    }
}
