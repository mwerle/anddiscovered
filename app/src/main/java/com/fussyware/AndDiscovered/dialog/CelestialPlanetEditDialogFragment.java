package com.fussyware.AndDiscovered.dialog;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.PlanetBody;

/**
 * Created by wes on 11/20/15.
 */
public class CelestialPlanetEditDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, Bundle changedItems);
    }

    public static final String TAG_MASS = "CelestialPlanetEditDialogFragment.mass";
    public static final String TAG_RADIUS = "CelestialPlanetEditDialogFragment.radius";
    public static final String TAG_SURFACETEMP = "CelestialPlanetEditDialogFragment.surfacetemp";
    public static final String TAG_SURFACEPRESSURE = "CelestialPlanetEditDialogFragment.surfacepressure";
    public static final String TAG_VOLCANISM = "CelestialPlanetEditDialogFragment.volcanism";
    public static final String TAG_ATMOSPHERETYPE = "CelestialPlanetEditDialogFragment.atmospheretype";
    public static final String TAG_ROTATIONPERIOD = "CelestialPlanetEditDialogFragment.rotationperiod";
    public static final String TAG_TIDALOCKED = "CelestialPlanetEditDialogFragment.tidallocked";
    public static final String TAG_AXISTILT = "CelestialPlanetEditDialogFragment.axistilt";

    private static final String TAG_ORIGINAL_BUNDLE = "CelestialPlanetEditDialogFragment.bundle";

    private static final String LOG_NAME = CelestialPlanetEditDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;

    private Bundle originalBundle;

    private EditText massText;
    private EditText radiusText;
    private EditText surfaceTempText;
    private EditText surfacePressureText;
    private EditText rotationText;
    private EditText axisTiltText;

    private Spinner atmosphereTypeSpinner;
    private CheckBox volcansimCheck;
    private CheckBox tidalLockedCheck;
    private Button doneButton;

    public static CelestialPlanetEditDialogFragment newInstance(@NonNull final PlanetBody body)
    {
        Bundle bundle = new Bundle();

        bundle.putString(TAG_MASS,
                         (body.getMass() == null) ?
                         "" :
                         String.format("%1.4f", body.getMass()));

        bundle.putString(TAG_RADIUS,
                         (body.getRadius() == null) ?
                         "" :
                         String.format("%1.2f", body.getRadius()));

        bundle.putString(TAG_SURFACETEMP,
                         (body.getSurfaceTemp() == null) ?
                         "" :
                         String.format("%1.2f", body.getSurfaceTemp()));

        bundle.putString(TAG_SURFACEPRESSURE,
                         (body.getSurfacePressure() == null) ?
                         "" :
                         String.format("%1.2f", body.getSurfacePressure()));

        bundle.putString(TAG_ROTATIONPERIOD,
                         (body.getRotationPeriod() == null) ?
                         "" :
                         String.format("%1.2f", body.getRotationPeriod()));

        bundle.putString(TAG_AXISTILT,
                         (body.getAxisTilt() == null) ?
                         "" :
                         String.format("%1.2f", body.getAxisTilt()));

        bundle.putInt(TAG_ATMOSPHERETYPE, body.getAtmosphereType().value);

        bundle.putBoolean(TAG_VOLCANISM, ((body.hasVolcanism() != null) && body.hasVolcanism()));
        bundle.putBoolean(TAG_TIDALOCKED,
                          ((body.getTidalLocked() != null) && body.getTidalLocked()));

        CelestialPlanetEditDialogFragment df = new CelestialPlanetEditDialogFragment();
        df.setArguments(bundle);

        return df;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        getDialog().setTitle("Update Details");
        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        View layout = inflater.inflate(R.layout.cb_dialog_planetary_layout,
                                       container,
                                       false);

        createViews(layout);
        createButtons(layout);

        if (savedInstanceState == null) {
            savedInstanceState = originalBundle = getArguments();
        } else {
            originalBundle = savedInstanceState.getBundle(TAG_ORIGINAL_BUNDLE);
        }

        populateViews(savedInstanceState);

        return layout;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        listener = null;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (!(activity instanceof OnDoneListener)) {
            throw new ClassCastException("Activity does not implement OnDoneListener.");
        }

        listener = (OnDoneListener) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putBundle(TAG_ORIGINAL_BUNDLE, originalBundle);

        outState.putString(TAG_MASS, massText.getText().toString());
        outState.putString(TAG_RADIUS, radiusText.getText().toString());
        outState.putString(TAG_SURFACETEMP, surfaceTempText.getText().toString());
        outState.putString(TAG_SURFACEPRESSURE, surfacePressureText.getText().toString());
        outState.putString(TAG_ROTATIONPERIOD, rotationText.getText().toString());
        outState.putString(TAG_AXISTILT, axisTiltText.getText().toString());

        outState.putInt(TAG_ATMOSPHERETYPE, atmosphereTypeSpinner.getSelectedItemPosition());

        outState.putBoolean(TAG_VOLCANISM, volcansimCheck.isChecked());
        outState.putBoolean(TAG_TIDALOCKED, tidalLockedCheck.isChecked());
    }

    private void createButtons(View layout)
    {
        final Button cancel = (Button) layout.findViewById(R.id.CancelButton);
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        doneButton = (Button) layout.findViewById(R.id.DoneButton);
        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (listener != null) {
                    Bundle bundle = new Bundle();

                    if (!massText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_MASS))) {
                        String value = massText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_MASS, Double.valueOf(value));
                    }

                    if (!radiusText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_RADIUS))) {
                        String value = radiusText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_RADIUS, Double.valueOf(value));
                    }

                    if (!surfaceTempText.getText()
                                   .toString()
                                   .equals(originalBundle.getString(TAG_SURFACETEMP))) {
                        String value = surfaceTempText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_SURFACETEMP, Double.valueOf(value));
                    }

                    if (!surfacePressureText.getText()
                                .toString()
                                .equals(originalBundle.getString(TAG_SURFACEPRESSURE))) {
                        String value = surfacePressureText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_SURFACEPRESSURE, Double.valueOf(value));
                    }

                    if (!rotationText.getText()
                                        .toString()
                                        .equals(originalBundle.getString(TAG_ROTATIONPERIOD))) {
                        String value = rotationText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_ROTATIONPERIOD, Double.valueOf(value));
                    }

                    if (!axisTiltText.getText()
                                         .toString()
                                         .equals(originalBundle.getString(TAG_AXISTILT))) {
                        String value = axisTiltText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_AXISTILT, Double.valueOf(value));
                    }

                    if (atmosphereTypeSpinner.getSelectedItemPosition() !=
                        originalBundle.getInt(TAG_ATMOSPHERETYPE)) {
                        bundle.putInt(TAG_ATMOSPHERETYPE,
                                      atmosphereTypeSpinner.getSelectedItemPosition());
                    }

                    if (volcansimCheck.isChecked() != originalBundle.getBoolean(TAG_VOLCANISM)) {
                        bundle.putBoolean(TAG_VOLCANISM, volcansimCheck.isChecked());
                    }

                    if (tidalLockedCheck.isChecked() != originalBundle.getBoolean(TAG_TIDALOCKED)) {
                        bundle.putBoolean(TAG_TIDALOCKED, tidalLockedCheck.isChecked());
                    }

                    listener.onDone(getTag(), bundle);
                }

                dismiss();
            }
        });
    }

    private void createViews(View layout)
    {
        massText = (EditText) layout.findViewById(R.id.cb_planet_mass_value);
        radiusText = (EditText) layout.findViewById(R.id.cb_planet_radius_value);
        surfaceTempText = (EditText) layout.findViewById(R.id.cb_planet_surfacetemp_value);
        surfacePressureText = (EditText) layout.findViewById(R.id.cb_planet_surfacepressure_value);
        rotationText = (EditText) layout.findViewById(R.id.cb_planet_rotationperiod_value);
        axisTiltText = (EditText) layout.findViewById(R.id.cb_planet_axistilt_value);
        axisTiltText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doneButton.callOnClick();
                }

                return false;
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                                                             R.array.atmosphere_types,
                                                                             android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        atmosphereTypeSpinner = (Spinner) layout.findViewById(R.id.cb_planet_atmospheretype_value);
        atmosphereTypeSpinner.setAdapter(adapter);

        volcansimCheck = (CheckBox) layout.findViewById(R.id.cb_planet_volcanism_value);
        tidalLockedCheck = (CheckBox) layout.findViewById(R.id.cb_planet_tidallocked_value);
    }

    private void populateViews(Bundle bundle)
    {
        massText.setText(bundle.getString(TAG_MASS));
        radiusText.setText(bundle.getString(TAG_RADIUS));
        surfaceTempText.setText(bundle.getString(TAG_SURFACETEMP));
        surfacePressureText.setText(bundle.getString(TAG_SURFACEPRESSURE));
        rotationText.setText(bundle.getString(TAG_ROTATIONPERIOD));
        axisTiltText.setText(bundle.getString(TAG_AXISTILT));

        atmosphereTypeSpinner.setSelection(bundle.getInt(TAG_ATMOSPHERETYPE));

        volcansimCheck.setChecked(bundle.getBoolean(TAG_VOLCANISM));
        tidalLockedCheck.setChecked(bundle.getBoolean(TAG_TIDALOCKED));
    }
}
