package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.fussyware.AndDiscovered.AndDiscoveredActivity;
import com.fussyware.AndDiscovered.EDProxyIntentService;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.StarCoordinator;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.TextDialogFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.preference.PreferenceTag;

/**
 * Created by wes on 9/30/15.
 */
public abstract class BaseActivity extends Activity implements TextDialogFragment.OnDoneListener
{
    private static final String LOG_NAME = BaseActivity.class.getSimpleName();
    private static final int DISCONNECTED = 1;
    private static final int CONNECTING = 2;
    private static final int CONNECTED = 3;

    protected SharedPreferences preferences;
    protected ActionBarDrawerToggle drawerToggle;
    protected DrawerLayout drawerLayout;

    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;

    private int connectedState = DISCONNECTED;
    private MenuItem connectionIcon;

    private EDProxyIntentService proxyService;

    private LocalBroadcastManager broadcastManager;
    private final BroadcastReceiver broadcastReceiver = new ConnectionBroadcastReceiver();

    private ListView drawerList;
    private TextView cmdrText;
    private TextView routeText;

    private final ServiceConnection proxyConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            proxyService = ((EDProxyIntentService.EDProxyBinder) service).getService();
            proxyService.resendState();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            proxyService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            connectedState = savedInstanceState.getInt("connectedState");
        }

        prefListener = new SharedPreferences.OnSharedPreferenceChangeListener()
        {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
            {
                switch (key) {
                    case PreferenceTag.CMDR_NAME: {
                        String cmdr = preferences.getString(PreferenceTag.CMDR_NAME,
                                                            getResources().getString(R.string.unknown));
                        cmdrText.setText(cmdr);
                        break;
                    }
                    case PreferenceTag.ROUTE: {
                        String route = preferences.getString(PreferenceTag.ROUTE,
                                                             getResources().getString(R.string.unknown));
                        routeText.setText(route);
                        break;
                    }
                    default:
                        break;
                }
            }
        };

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferences.registerOnSharedPreferenceChangeListener(prefListener);

        IntentFilter filter = new IntentFilter();

        filter.addAction(EDProxyIntentService.BROADCAST_DISCOVERY_STARTED);
        filter.addAction(EDProxyIntentService.BROADCAST_DISCOVERY_FAILED);
        filter.addAction(EDProxyIntentService.BROADCAST_DISCOVERY_SUCCESS);
        filter.addAction(EDProxyIntentService.BROADCAST_CONNECTED);
        filter.addAction(EDProxyIntentService.BROADCAST_DISCONNECTED);

        filter.addAction(EDProxyIntentService.BROADCAST_SYSTEM_UPDATED);
        filter.addAction(StarCoordinator.BROADCAST_SYSTEM_UPDATED);

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastManager.registerReceiver(broadcastReceiver, filter);

        bindService(new Intent(getApplicationContext(), EDProxyIntentService.class),
                    proxyConnection,
                    BIND_AUTO_CREATE);
    }

    @Override
    public void setContentView(int layoutResID)
    {
        super.setContentView(layoutResID);
        onCreateDrawer();
    }

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);
        onCreateDrawer();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params)
    {
        super.setContentView(view, params);
        onCreateDrawer();
    }

    @Override
    protected void onDestroy()
    {
        unbindService(proxyConnection);

        preferences.unregisterOnSharedPreferenceChangeListener(prefListener);
        broadcastManager.unregisterReceiver(broadcastReceiver);

        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("connectedState", connectedState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        connectionIcon = menu.findItem(R.id.connection_state);

        switch (connectedState) {
            case CONNECTING:
                connectionIcon.setIcon(R.drawable.ic_yellow_icon);
                break;
            case CONNECTED:
                connectionIcon.setIcon(R.drawable.ic_green_circle);
                break;
            default:
                break;
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else {
                    return onHomeItemSelected(item);
                }
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
            case R.id.action_route: {
                TextDialogFragment tdf = TextDialogFragment.newInstance(getResources().getString(R.string.route_destination_dialog_title),
                                                                        getResources().getString(R.string.route_destination_dialog_summary),
                                                                        preferences.getString(PreferenceTag.ROUTE, ""),
                                                                        InputType.TYPE_CLASS_TEXT);
                tdf.show(getFragmentManager(), FragmentTag.main_activity.route_dialog);

                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            super.onBackPressed();
        }
    }

    protected boolean onDrawerBackPressed()
    {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }

        return false;
    }

    protected boolean onHomeItemSelected(MenuItem item)
    {
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    protected void setDrawerIndicatorEnabled(boolean enabled)
    {
        drawerToggle.setDrawerIndicatorEnabled(enabled);
    }

    protected void onCreateDrawerList()
    {
        drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerList.setAdapter(new ArrayAdapter<String>(this,
                                                       R.layout.drawer_item_layout,
                                                       getResources().getStringArray(R.array.drawerlist_string_array)));
        drawerList.setOnItemClickListener(new DrawerOnClickListener());
    }

    private void onCreateDrawer()
    {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        cmdrText = (TextView) findViewById(R.id.drawer_cmdr_name);
        routeText = (TextView) findViewById(R.id.drawer_route);

        String cmdr = preferences.getString(PreferenceTag.CMDR_NAME,
                                            getResources().getString(R.string.unknown));
        String route = preferences.getString(PreferenceTag.ROUTE,
                                             getResources().getString(R.string.unknown));

        cmdrText.setText(cmdr);
        routeText.setText(route);

        drawerToggle = new ActionBarDrawerToggle(this,
                                                 drawerLayout,
                                                 R.string.drawer_open,
                                                 R.string.drawer_close) {
            private CharSequence previous_title;

            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);

                if (getActionBar() != null) {
                    previous_title = getActionBar().getTitle();
                    getActionBar().setTitle(R.string.drawer_navigation_title);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView)
            {
                super.onDrawerClosed(drawerView);

                if (getActionBar() != null) {
                    getActionBar().setTitle(previous_title);
                }
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }

        onCreateDrawerList();
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.main_activity.route_dialog: {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(PreferenceTag.ROUTE, text);
                editor.apply();

                break;
            }
            default:
                break;
        }
    }

    protected void onNewSystem(SystemInfo system)
    {

    }

    protected void onUpdateSystems()
    {

    }

    private class DrawerOnClickListener implements AdapterView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            Intent intent;

            // TODO: This sucks giant &^%$#@! Replace with something more abstract please!
            switch (position) {
                case 0:
                    intent = new Intent(getApplicationContext(), AndDiscoveredActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 1:
                    intent = new Intent(getApplicationContext(), PhotoGalleryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(getApplicationContext(), RouteOptimizerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 4:
                    intent = new Intent(getApplicationContext(), GalnetNewsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                    break;
                default:
                    break;
            }

            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private class ConnectionBroadcastReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            switch (action) {
                case EDProxyIntentService.BROADCAST_DISCOVERY_STARTED:
                    Log.d(LOG_NAME, "EDProxy discovery started.");
                    if (connectionIcon != null) {
                        connectionIcon.setIcon(R.drawable.ic_yellow_icon);
                    }

                    connectedState = CONNECTING;
                    break;
                case EDProxyIntentService.BROADCAST_DISCOVERY_SUCCESS:
                    String hostname = intent.getStringExtra(EDProxyIntentService.EXTRA_HOSTNAME);
                    int port = intent.getIntExtra(EDProxyIntentService.EXTRA_PORT, -1);
                    Log.d(LOG_NAME, "EDProxy discovery success: hostname [" + hostname + "], port [" + Integer.toString(port) + "]");
                    break;
                case EDProxyIntentService.BROADCAST_DISCOVERY_FAILED:
                    Log.d(LOG_NAME, "EDProxy discovery failed!");
                    if (connectionIcon != null) {
                        connectionIcon.setIcon(R.drawable.ic_red_circle);
                    }

                    connectedState = DISCONNECTED;
                    break;
                case EDProxyIntentService.BROADCAST_CONNECTED:
                    if (connectionIcon != null) {
                        connectionIcon.setIcon(R.drawable.ic_green_circle);
                    }

                    connectedState = CONNECTED;
                    break;
                case EDProxyIntentService.BROADCAST_DISCONNECTED:
                    Log.d(LOG_NAME, "EDProxy disconnected!");
                    if (connectionIcon != null) {
                        connectionIcon.setIcon(R.drawable.ic_red_circle);
                    }

                    connectedState = DISCONNECTED;
                    break;
                case EDProxyIntentService.BROADCAST_SYSTEM_UPDATED: {
                    SystemInfo info = intent.getParcelableExtra("system");
                    onNewSystem(info);
                    break;
                }
                case StarCoordinator.BROADCAST_SYSTEM_UPDATED:
                    onUpdateSystems();
                    break;
                default:
                    break;
            }
        }
    }
}
