package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.fragment.DistanceListFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;

/**
 * Created by wes on 9/30/15.
 */
public class CoordinateCalculatorActivity
        extends BaseActivity
        implements DistanceListFragment.OnSystemLongClickListener,
                   DistanceListFragment.OnPositionTrilaterationListener
{
    private static final String LOG_NAME = CoordinateCalculatorActivity.class.getSimpleName();

    private CmdrSystemInfo systemInfo;

    private TitleInfoFragment titleFragment;
    private DistanceListFragment distanceFragment;

    private boolean modified;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.distance_activity_title);
        setContentView(R.layout.distance_info_activity_layout);
        setDrawerIndicatorEnabled(false);

        titleFragment = (TitleInfoFragment) getFragmentManager().findFragmentById(R.id.title_info_fragment);
        distanceFragment = (DistanceListFragment) getFragmentManager().findFragmentById(R.id.distance_list_fragment);

        if (savedInstanceState == null) {
            systemInfo = CmdrDbHelper
                    .getInstance()
                    .getSystem(getIntent().getStringExtra("system"));
        } else {
            systemInfo = CmdrDbHelper
                    .getInstance()
                    .getSystem(savedInstanceState.getString("system"));
            modified = savedInstanceState.getBoolean("modified");
        }

        titleFragment.setSystem(systemInfo);
        distanceFragment.setSystem(systemInfo);
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            if (modified) {
                Intent intent = new Intent();
                intent.putExtra("modified", modified);

                setResult(Activity.RESULT_OK, intent);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString("system", systemInfo.getSystem());
        outState.putBoolean("modified", modified);
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        onBackPressed();
        return true;
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.distance_list_fragment.distance_update_dialog:
                if (distanceFragment != null) {
                    distanceFragment.onDistanceUpdateDialogDone(text);
                    modified = true;
                }
                break;
            default:
                super.onDone(tag, text);
                break;
        }
    }

    @Override
    public void onPositionCalculation(String text)
    {
        titleFragment.onPositionCalculation(text);
    }

    @Override
    public void onPositionFound(Position position)
    {
        titleFragment.onPositionFound(position);
        modified = true;
    }

    @Override
    public void onSystemLongClick(String system)
    {
        Log.d(LOG_NAME, "Long press on system: " + system);
        Intent intent = new Intent(getApplicationContext(), SystemInfoActivity.class);
        intent.putExtra("system", system);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if (modified) {
            Intent retIntent = new Intent();
            retIntent.putExtra("modified", modified);

            setResult(Activity.RESULT_OK, retIntent);
            finish();
        }
        startActivity(intent);
    }
}
