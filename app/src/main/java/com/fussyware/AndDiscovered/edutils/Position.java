package com.fussyware.AndDiscovered.edutils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wes on 6/14/15.
 */
public final class Position implements Parcelable
{
    public static final Parcelable.Creator<Position> CREATOR = new Parcelable.Creator<Position>() {

        @Override
        public Position createFromParcel(Parcel source)
        {
            return new Position(source);
        }

        @Override
        public Position[] newArray(int size)
        {
            return new Position[size];
        }
    };

    public final double x;
    public final double y;
    public final double z;

    public Position(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    private Position(Parcel parcel)
    {
        x = parcel.readDouble();
        y = parcel.readDouble();
        z = parcel.readDouble();
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("(");
        sb.append(x);
        sb.append(", ");
        sb.append(y);
        sb.append(", ");
        sb.append(z);
        sb.append(")");

        return sb.toString();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeDouble(x);
        dest.writeDouble(z);
        dest.writeDouble(y);
    }
}
