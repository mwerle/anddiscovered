package com.fussyware.AndDiscovered.eddiscovery;

import android.util.JsonReader;
import android.util.JsonWriter;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by wes on 6/14/15.
 */
public class QueryMessage extends DiscoveryMessage
{
    public QueryMessage()
    {
        this(null);
    }

    public QueryMessage(String serviceName)
    {
        super(DiscoveryType.Query, serviceName);
    }

    static DiscoveryMessage getMessage(String json)
    {
        try {
            StringReader sr = new StringReader(json);
            JsonReader reader = new JsonReader(sr);

            DiscoveryType _type = null;
            String _serviceName = null;

            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();

                if (name.equals("type")) {
                    name = reader.nextString();

                    if (name.equals(DiscoveryType.Query.toString())) {
                        _type = DiscoveryType.Query;
                    }
                } else if (name.equals("name")) {
                    _serviceName = reader.nextString();
                }
            }
            reader.endObject();
            reader.close();

            if (null == _type) {
                return null;
            } else {
                return new QueryMessage(_serviceName);
            }
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void fillJSON(JsonWriter writer) throws IOException
    {

    }
}
