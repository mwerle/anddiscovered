package com.fussyware.AndDiscovered;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by wboyd on 7/6/15.
 */
public abstract class StarCoordinator
{
    public static final String BROADCAST_DISTANCE_UPDATED = "StarCoordinator.DistanceUpdated";
    public static final String BROADCAST_SYSTEM_UPDATED = "StarCoordinator.SystemUpdated";

    public static final int SORT_BY_X = 1;
    public static final int SORT_BY_Y = 2;
    public static final int SORT_BY_Z = 3;
    public static final int SORT_BY_ANY = -1;

    protected CmdrDbHelper cmdrDbHelper;
    private Context context;

    public StarCoordinator(Context context)
    {
        cmdrDbHelper = CmdrDbHelper.getInstance();
        this.context = context;
    }

    protected CmdrDbHelper getCmdrDbHelper()
    {
        return cmdrDbHelper;
    }

    public abstract Intent getIntentService(Context context);
    public abstract SystemInfo getSystem(String system);
    public abstract boolean containsSystem(String system);
    public abstract DistanceInfo getDistance(String s1, String s2);
    public abstract List<? extends DistanceInfo> getDistances(String system, boolean duplicatesAllowed);

    public List<? extends SystemInfo> getSystemSphere(Position origin, double radius, int sortBy)
    {
        return new ArrayList<>();
    }

    public Future<TrilatResponse> trilatSystem(String system, List<? extends DistanceInfo> distanceInfos)
    {
        return trilatSystem(system, distanceInfos, false);
    }

    public Future<TrilatResponse> trilatSystem(String system, List<? extends DistanceInfo> distanceInfos, boolean debounce)
    {
        throw new UnsupportedOperationException();
    }

    public void sendDistanceUpdatedIntent()
    {
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(BROADCAST_DISTANCE_UPDATED));
    }

    public void sendSystemUpdatedIntent()
    {
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(BROADCAST_SYSTEM_UPDATED));
    }
}
