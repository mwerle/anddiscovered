package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 8/29/15.
 */
public enum StarType
{
    A(1, "A"),
    AeBe(2, "AeBe"),
    B(3, "B"),
    C(4, "C"),
    F(5, "F"),
    G(6, "G"),
    K(7, "K"),
    L(8, "L"),
    M(9, "M"),
    O(10, "O"),
    S(11, "S"),
    T(12, "T"),
    TT(13, "TT"),
    W(14, "W"),
    Y(15, "Y"),
    White_Dwarf(16, "White Dwarf"),
    Neutron(17, "Neutron"),
    Black_Hole(18, "Black Hole"),
    // TODO: Change all Unknown 19 to 0xFFFFFFFF
    Unknown(19, "NA");

    public int value;
    // TODO: Get rid of name. The applications should use a R.string value for translation.
    public String name;

    StarType(int value, String name)
    {
        this.value = value;
        this.name = name;
    }

    public static StarType getStarType(String value)
    {
        for (StarType t : StarType.values()) {
            if (t.name.equals(value)) {
                return t;
            }
        }

        return StarType.Unknown;
    }

    public static StarType getStarType(int value)
    {
        for (StarType t : StarType.values()) {
            if (t.value == value) {
                return t;
            }
        }

        return StarType.Unknown;
    }

    public int value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
