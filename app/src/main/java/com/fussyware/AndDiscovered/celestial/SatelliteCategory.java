package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 11/7/15.
 */
public enum SatelliteCategory
{
    Star(1),
    Planet(2),
    Asteroid(3),
    Unknown(Integer.MAX_VALUE);

    public final int value;

    SatelliteCategory(int value)
    {
        this.value = value;
    }

    public static SatelliteCategory getSatelliteCategory(int value)
    {
        for (SatelliteCategory category : SatelliteCategory.values()) {
            if (value == category.value) {
                return category;
            }
        }

        return SatelliteCategory.Unknown;
    }
}
