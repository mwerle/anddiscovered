package com.fussyware.AndDiscovered.edsc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.fussyware.AndDiscovered.edutils.Position;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wboyd on 7/6/15.
 */
public class EDSCDbHelper extends SQLiteAssetHelper
{
    private static final String LOG_NAME = EDSCDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "edsc.db";

    private static EDSCDbHelper singleton = null;

    private Date lastDistanceCheck = null;
    private Date lastSystemCheck = null;
    private Date lastUpload = null;

    public final Object mutex = new Object();

    public EDSCDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();
    }

    public static synchronized EDSCDbHelper getInstance(Context context)
    {
        if (null == singleton) {
            singleton = new EDSCDbHelper(context);
        }

        return singleton;
    }

    public boolean containsSystem(String system)
    {
        String[] columns = {
                EDSCDbContract.EDSCSystems.COLUMN_NAME_SYSTEM,
        };

        String selection = EDSCDbContract.EDSCSystems.COLUMN_NAME_SYSTEM + "=?";
        String[] selectionArgs = { system };

        Cursor cursor = getReadableDatabase().query(EDSCDbContract.EDSCSystems.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            cursor.moveToFirst();
            return !cursor.isAfterLast();
        } finally {
            cursor.close();
        }
    }

    public EDSCSystemInfo createSystem(String system, Position xyz)
    {
        if ((system == null) || system.isEmpty()) {
            throw new IllegalArgumentException("Invalid system name specified.");
        }

        if (containsSystem(system)) {
            return getSystem(system);
        } else {
            ContentValues values = new ContentValues();
            values.put(EDSCDbContract.EDSCSystems.COLUMN_NAME_SYSTEM, system);

            if (xyz == null) {
                values.putNull(EDSCDbContract.EDSCSystems.COLUMN_NAME_XCOORD);
                values.putNull(EDSCDbContract.EDSCSystems.COLUMN_NAME_YCOORD);
                values.putNull(EDSCDbContract.EDSCSystems.COLUMN_NAME_ZCOORD);
            } else {
                values.put(EDSCDbContract.EDSCSystems.COLUMN_NAME_XCOORD, xyz.x);
                values.put(EDSCDbContract.EDSCSystems.COLUMN_NAME_YCOORD, xyz.y);
                values.put(EDSCDbContract.EDSCSystems.COLUMN_NAME_ZCOORD, xyz.z);
            }

            long id = getWritableDatabase().insert(EDSCDbContract.EDSCSystems.TABLE_NAME,
                                                   null,
                                                   values);

            return new EDSCSystemInfo(getWritableDatabase(), id, system, xyz);
        }
    }

    public EDSCSystemInfo getSystem(String system)
    {
        String[] columns = {
                EDSCDbContract.EDSCSystems._ID,
                EDSCDbContract.EDSCSystems.COLUMN_NAME_SYSTEM,
                EDSCDbContract.EDSCSystems.COLUMN_NAME_XCOORD,
                EDSCDbContract.EDSCSystems.COLUMN_NAME_YCOORD,
                EDSCDbContract.EDSCSystems.COLUMN_NAME_ZCOORD,
        };

        String selection = EDSCDbContract.EDSCSystems.COLUMN_NAME_SYSTEM + "=?";
        String[] selectionArgs = { system };

        Cursor cursor = getReadableDatabase().query(EDSCDbContract.EDSCSystems.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            EDSCSystemInfo info = null;

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                Position xyz = null;

                if (!cursor.isNull(2) && !cursor.isNull(3) && !cursor.isNull(4)) {
                    xyz = new Position(cursor.getDouble(2), cursor.getDouble(3), cursor.getDouble(4));
                }

                info = new EDSCSystemInfo(getWritableDatabase(),
                                          cursor.getLong(0),
                                          cursor.getString(1),
                                          xyz);
            }

            return info;
        } finally {
            cursor.close();
        }
    }

    public EDSCDistanceInfo createDistance(String from, String to, Double distance)
    {
        if ((from == null) || from.isEmpty()) {
            throw new IllegalArgumentException("Invalid beginning system specified.");
        }

        if ((to == null) || to.isEmpty()){
            throw new IllegalArgumentException("Invalid end system specified.");
        }

        if (distance == null) {
            distance = 0.0;
        }

        distance = (distance < 0.0) ? -distance : distance;

        ContentValues values = new ContentValues();
        values.put(EDSCDbContract.EDSCDistances.COLUMN_NAME_FROM, from);
        values.put(EDSCDbContract.EDSCDistances.COLUMN_NAME_DISTANCE, distance);
        values.put(EDSCDbContract.EDSCDistances.COLUMN_NAME_EDSC_UPDATED, true);
        values.put(EDSCDbContract.EDSCDistances.COLUMN_NAME_TO, to);

        long id = getWritableDatabase().insert(EDSCDbContract.EDSCDistances.TABLE_NAME,
                                               null,
                                               values);

        return new EDSCDistanceInfo(getWritableDatabase(),
                                    id,
                                    from,
                                    to,
                                    distance);
    }

    public EDSCDistanceInfo getDistance(String s1, String s2)
    {
        String[] columns = {
                EDSCDbContract.EDSCDistances._ID,
                EDSCDbContract.EDSCDistances.COLUMN_NAME_FROM,
                EDSCDbContract.EDSCDistances.COLUMN_NAME_TO,
                EDSCDbContract.EDSCDistances.COLUMN_NAME_DISTANCE,
        };

        String selection = EDSCDbContract.EDSCDistances.COLUMN_NAME_FROM + " IN (?,?) AND " +
                EDSCDbContract.EDSCDistances.COLUMN_NAME_TO + " IN (?,?)";
        String[] selectionArgs = { s1, s2, s1, s2 };

        Cursor cursor = getReadableDatabase().query(EDSCDbContract.EDSCDistances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            cursor.moveToFirst();
            return cursor.isAfterLast() ? null : new EDSCDistanceInfo(getWritableDatabase(),
                                                                      cursor.getLong(0),
                                                                      cursor.getString(1),
                                                                      cursor.getString(2),
                                                                      cursor.getDouble(3));
        } finally {
            cursor.close();
        }
    }

    public List<EDSCDistanceInfo> getDistances(String system, boolean duplicatesAllowed)
    {
        ArrayList<EDSCDistanceInfo> list = new ArrayList<EDSCDistanceInfo>();

        String[] columns = {
                EDSCDbContract.EDSCDistances._ID,
                EDSCDbContract.EDSCDistances.COLUMN_NAME_FROM,
                EDSCDbContract.EDSCDistances.COLUMN_NAME_TO,
                EDSCDbContract.EDSCDistances.COLUMN_NAME_DISTANCE,
        };

        String selection = EDSCDbContract.EDSCDistances.COLUMN_NAME_FROM + " =? OR " +
                EDSCDbContract.EDSCDistances.COLUMN_NAME_TO + " =?";
        String[] selectionArgs = { system, system };

        Cursor cursor = getReadableDatabase().query(EDSCDbContract.EDSCDistances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            while (cursor.moveToNext()) {
                String s1 = cursor.getString(1);
                String s2 = cursor.getString(2);

                EDSCDistanceInfo info;

                if (system.equalsIgnoreCase(s1)) {
                    info = new EDSCDistanceInfo(getWritableDatabase(),
                                                cursor.getLong(0),
                                                s1,
                                                s2,
                                                cursor.getDouble(3));
                } else {
                    info = new EDSCDistanceInfo(getWritableDatabase(),
                                                cursor.getLong(0),
                                                s2,
                                                s1,
                                                cursor.getDouble(3));
                }

                if (duplicatesAllowed) {
                    list.add(info);
                } else if (!list.contains(info)) {
                    list.add(info);
                }
            }

            return list;
        } finally {
            cursor.close();
        }
    }

    public Date getLastDistanceCheck()
    {
        synchronized (mutex) {
            if (lastDistanceCheck == null) {
                String[] columns = {
                        EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG,
                        EDSCDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {EDSCDbContract.DbInfo.CONFIG_NAME_LAST_EDSC_DISTANCE_CHECK};

                Cursor c = getReadableDatabase().query(EDSCDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();
                    lastDistanceCheck = EDSCDbContract.DATE_FORMATTER.parse(c.getString(1));
                } catch (ParseException e) {
                    lastDistanceCheck = new Date(0);
                } finally {
                    c.close();
                }
            }

            return lastDistanceCheck;
        }
    }

    public void setLastDistanceCheck(Date newTime)
    {
        String selection = EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
        String[] selectionArgs = {EDSCDbContract.DbInfo.CONFIG_NAME_LAST_EDSC_DISTANCE_CHECK};

        ContentValues values = new ContentValues();
        values.put(EDSCDbContract.DbInfo.COLUMN_NAME_VALUE, EDSCDbContract.DATE_FORMATTER.format(newTime));

        getWritableDatabase().update(EDSCDbContract.DbInfo.TABLE_NAME,
                                     values,
                                     selection,
                                     selectionArgs);

        synchronized (mutex) {
            lastDistanceCheck = newTime;
        }
    }

    public Date getLastSystemCheck()
    {
        synchronized (mutex) {
            if (lastSystemCheck == null) {
                String[] columns = {
                        EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG,
                        EDSCDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {EDSCDbContract.DbInfo.CONFIG_NAME_LAST_EDSC_SYSTEM_CHECK};

                Cursor c = getReadableDatabase().query(EDSCDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();
                    lastSystemCheck = EDSCDbContract.DATE_FORMATTER.parse(c.getString(1));
                } catch (ParseException e) {
                    lastSystemCheck = new Date(0);
                } finally {
                    c.close();
                }
            }

            return lastSystemCheck;
        }
    }

    public void setLastSystemCheck(Date newTime)
    {
        String selection = EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
        String[] selectionArgs = {EDSCDbContract.DbInfo.CONFIG_NAME_LAST_EDSC_SYSTEM_CHECK};

        ContentValues values = new ContentValues();
        values.put(EDSCDbContract.DbInfo.COLUMN_NAME_VALUE, EDSCDbContract.DATE_FORMATTER.format(newTime));

        getWritableDatabase().update(EDSCDbContract.DbInfo.TABLE_NAME,
                                     values,
                                     selection,
                                     selectionArgs);

        synchronized (mutex) {
            lastSystemCheck = newTime;
        }
    }

    public Date getLastUploadTime()
    {
        synchronized (mutex) {
            if (lastUpload == null) {
                String[] columns = {
                        EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG,
                        EDSCDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {EDSCDbContract.DbInfo.CONFIG_NAME_LAST_EDSC_UPLOAD};

                Cursor c = getReadableDatabase().query(EDSCDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();
                    lastUpload = EDSCDbContract.DATE_FORMATTER.parse(c.getString(1));
                } catch (ParseException e) {
                    lastUpload = new Date(0);
                } finally {
                    c.close();
                }
            }

            return lastUpload;
        }
    }

    public void setLastUploadTime(Date newTime)
    {
        String selection = EDSCDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
        String[] selectionArgs = {EDSCDbContract.DbInfo.CONFIG_NAME_LAST_EDSC_UPLOAD};

        ContentValues values = new ContentValues();
        values.put(EDSCDbContract.DbInfo.COLUMN_NAME_VALUE, EDSCDbContract.DATE_FORMATTER.format(newTime));

        getWritableDatabase().update(EDSCDbContract.DbInfo.TABLE_NAME,
                                     values,
                                     selection,
                                     selectionArgs);

        synchronized (mutex) {
            lastUpload = newTime;
        }
    }
}

