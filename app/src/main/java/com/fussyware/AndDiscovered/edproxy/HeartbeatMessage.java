package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;

import java.io.IOException;
import java.util.Date;

/**
 * Created by wes on 9/27/15.
 */
public class HeartbeatMessage extends ProxyMessage
{

    protected HeartbeatMessage()
    {
        super(ProxyMessageType.Heartbeat, new Date());
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {

    }
}
