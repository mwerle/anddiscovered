package com.fussyware.AndDiscovered.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.method.NumberKeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 9/2/15.
 */
public class TextDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, String text);
    }

    private static final String LOG_NAME = TextDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;

    private String initialContent;
    private EditText contentText;

    public static TextDialogFragment newInstance(String title,
                                                 String summary,
                                                 String content,
                                                 int inputType)
    {
        TextDialogFragment tdf = new TextDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("content", content);
        bundle.putString("title", (title == null) ? "" : title);
        bundle.putString("summary", (summary == null) ? "" : summary);
        bundle.putInt("inputType", inputType);

        tdf.setArguments(bundle);

        return tdf;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle bundle = getArguments();

        String title = bundle.getString("title");
        String summary = bundle.getString("summary");

        int inputType = bundle.getInt("inputType");

        initialContent = bundle.getString("content");

        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        if (!title.isEmpty()) {
            Dialog dialog = getDialog();
            dialog.setTitle(title);
        }

        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.text_dialog_layout, null);

        if (layout != null) {
            TextView summaryText = (TextView) layout.findViewById(R.id.SummaryText);
            final Button doneButton = (Button) layout.findViewById(R.id.DoneButton);
            Button cancelButton = (Button) layout.findViewById(R.id.CancelButton);

            contentText = (EditText) layout.findViewById(R.id.ContentText);

            if (summary.isEmpty()) {
                layout.removeView(summaryText);
            } else {
                summaryText.setText(summary);
            }

            switch (inputType) {
                case InputType.TYPE_NUMBER_FLAG_SIGNED:
                    contentText.setKeyListener(new NumberKeyListener() {
                        @Override
                        protected char[] getAcceptedChars()
                        {
                            return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
                        }

                        @Override
                        public int getInputType()
                        {
                            return InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_CLASS_NUMBER;
                        }
                    });

                    break;
                case InputType.TYPE_NUMBER_FLAG_DECIMAL:
                    contentText.setKeyListener(new NumberKeyListener() {
                        @Override
                        protected char[] getAcceptedChars()
                        {
                            return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'};
                        }

                        @Override
                        public int getInputType()
                        {
                            return InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER;
                        }
                    });
                    break;
                default:
                    break;
            }

            contentText.setInputType(inputType);
            contentText.setText(initialContent);
            contentText.setOnEditorActionListener(new TextView.OnEditorActionListener()
            {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
                {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        doneButton.callOnClick();
                    }

                    return false;
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    dismiss();
                }
            });

            doneButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    String currentContent = contentText.getText().toString();

                    if (listener != null) {
                        if (!currentContent.equals(initialContent)) {
                            listener.onDone(getTag(), currentContent);
                        }
                    }

                    dismiss();
                }
            });
        }

        return layout;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        listener = null;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (!(activity instanceof OnDoneListener)) {
            throw new ClassCastException("Activity does not implement OnDoneListener.");
        }

        listener = (OnDoneListener) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }
}
