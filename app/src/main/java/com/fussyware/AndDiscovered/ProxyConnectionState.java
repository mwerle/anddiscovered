package com.fussyware.AndDiscovered;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by wes on 9/18/15.
 */
public final class ProxyConnectionState
{
    public enum ConnectionState
    {
        DISCONNECTED,
        CONNECTING,
        CONNECTED
    }

    public static final int HTTP_IMAGE_PORT = 8097;

    private static final String LOG_NAME = ProxyConnectionState.class.getSimpleName();
    private static ProxyConnectionState singleton = null;
    private static final Object initMutex = new Object();

    private ConnectionState state;

    private String hostname = "";
    private int proxyPort = -1;

    private final BroadcastReceiver broadcastReceiver;
    private final LocalBroadcastManager broadcastManager;

    private final Object mutex = new Object();

    private ProxyConnectionState(Context context)
    {
        state = ConnectionState.DISCONNECTED;

        broadcastReceiver = new BroadcastIntentReceiver();
        broadcastManager = LocalBroadcastManager.getInstance(context);

        broadcastManager.registerReceiver(broadcastReceiver,
                                          new IntentFilter(EDProxyIntentService.BROADCAST_DISCOVERY_SUCCESS));
        broadcastManager.registerReceiver(broadcastReceiver,
                                          new IntentFilter(EDProxyIntentService.BROADCAST_DISCOVERY_FAILED));
        broadcastManager.registerReceiver(broadcastReceiver,
                                          new IntentFilter(EDProxyIntentService.BROADCAST_CONNECTED));
        broadcastManager.registerReceiver(broadcastReceiver,
                                          new IntentFilter(EDProxyIntentService.BROADCAST_DISCONNECTED));
    }

    public static void init(Context context)
    {
        synchronized (initMutex) {
            singleton = new ProxyConnectionState(context);
        }
    }

    public static ProxyConnectionState getInstance() throws IllegalStateException
    {
        synchronized (initMutex) {
            if (singleton == null) {
                throw new IllegalStateException("Connection state has not yet been initialized.");
            }

            return singleton;
        }
    }

    public URL generateImageURL(String path) throws
                                             IllegalStateException,
                                             MalformedURLException
    {
        synchronized (mutex) {
            if ((state != ConnectionState.CONNECTED) || hostname.isEmpty()) {
                throw new IllegalStateException("Unknown hostname to generate the URL, thus not connected.");
            }

            String encodedPath;

            try {
                encodedPath = URLEncoder.encode(path, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                encodedPath = path;
            }

            return new URL("http", hostname, HTTP_IMAGE_PORT, encodedPath);
        }
    }

    public ConnectionState getState()
    {
        synchronized (mutex) {
            return state;
        }
    }

    public String getHostname()
    {
        synchronized (mutex) {
            return hostname;
        }
    }

    public int getProxyPort()
    {
        synchronized (mutex) {
            return proxyPort;
        }
    }

    public void shutdown()
    {
        synchronized (initMutex) {
            singleton = null;
        }

        synchronized (mutex) {
            state = ConnectionState.DISCONNECTED;
            hostname = "";
            proxyPort = -1;

            broadcastManager.unregisterReceiver(broadcastReceiver);
        }
    }

    private class BroadcastIntentReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            synchronized (mutex) {
                switch (action) {
                    case EDProxyIntentService.BROADCAST_DISCOVERY_STARTED:
                        state = ConnectionState.CONNECTING;
                        break;
                    case EDProxyIntentService.BROADCAST_DISCOVERY_SUCCESS:
                        hostname = intent.getStringExtra(EDProxyIntentService.EXTRA_HOSTNAME);
                        proxyPort = intent.getIntExtra(EDProxyIntentService.EXTRA_PORT,
                                                       -1);
                        break;
                    case EDProxyIntentService.BROADCAST_DISCOVERY_FAILED:
                        state = ConnectionState.DISCONNECTED;
                        break;
                    case EDProxyIntentService.BROADCAST_CONNECTED:
                        hostname = intent.getStringExtra(EDProxyIntentService.EXTRA_HOSTNAME);
                        proxyPort = intent.getIntExtra(EDProxyIntentService.EXTRA_PORT,
                                                       -1);
                        Log.d(LOG_NAME, "Broadcast connected: " + (hostname == null ? "null" : hostname) + ", port" + proxyPort);
                        state = ConnectionState.CONNECTED;
                        break;
                    case EDProxyIntentService.BROADCAST_DISCONNECTED:
                        state = ConnectionState.DISCONNECTED;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
