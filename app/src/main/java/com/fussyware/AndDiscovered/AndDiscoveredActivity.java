 package com.fussyware.AndDiscovered;

 import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.fussyware.AndDiscovered.activity.BaseActivity;
import com.fussyware.AndDiscovered.activity.CelestialListActivity;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.fragment.SystemListFragment;
import com.fussyware.AndDiscovered.preference.PreferenceTag;

 public class AndDiscoveredActivity
         extends BaseActivity
         implements SystemListFragment.SystemSelectedListener
{
    private static final String LOG_NAME = AndDiscoveredActivity.class.getSimpleName();
    private static final int RESULT_SYSTEM_INFO = 101;

    private CmdrDbHelper dbHelper;
    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;
    private SystemListFragment listFragment;

    private OnUpgradeReceiver upgradeReceiver;

    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.system_list_activity_title);

        dbHelper = CmdrDbHelper.getInstance();

        prefListener = new SharedPreferences.OnSharedPreferenceChangeListener()
        {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
            {
                Intent intent = new Intent(getApplicationContext(),
                                           EDProxyIntentService.class);

                switch (key) {
                    case PreferenceTag.DISCOVERY_TTL: {
                        if (sharedPreferences.getString(key, "").isEmpty()) {
                            boolean enabled = sharedPreferences.getBoolean(PreferenceTag.DISCOVERY_ENABLED,
                                                                           true);

                            stopService(intent);

                            if (enabled) {
                                startService(intent);
                            } else {
                                if (sharedPreferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
//                                    DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.no_ipaddr_title,
//                                                                                                R.string.no_ipaddr_message);
//                                    fragment.show(getFragmentManager(), FragmentTag.main_activity.no_ipaddr_dialog);
                                } else {
                                    startService(intent);
                                }
                            }
                        }

                        break;
                    }
                    case PreferenceTag.IPADDR: {
                        if (!sharedPreferences.getString(key, "").isEmpty()) {
                            stopService(intent);
                            startService(intent);
                        } else {
                            boolean enabled = sharedPreferences.getBoolean(PreferenceTag.DISCOVERY_ENABLED,
                                                                           true);

                            if (enabled) {
                                startService(intent);
                            } else {
//                                DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.no_ipaddr_title,
//                                                                                            R.string.no_ipaddr_message);
//                                fragment.show(getFragmentManager(),
//                                              FragmentTag.main_activity.no_ipaddr_dialog);
                            }
                        }

                        break;
                    }
                    case PreferenceTag.DISCOVERY_ENABLED: {
                        boolean enabled = sharedPreferences.getBoolean(key, true);

                        if (enabled) {
                            stopService(intent);
                            startService(intent);
                        } else {
                            stopService(intent);

                            if (sharedPreferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
//                                DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.no_ipaddr_title,
//                                                                                            R.string.no_ipaddr_message);
//                                fragment.show(getFragmentManager(), FragmentTag.main_activity.no_ipaddr_dialog);
                            } else {
                                startService(intent);
                            }
                        }
                    }
                    default:
                        break;
                }
            }
        };

        preferences.registerOnSharedPreferenceChangeListener(prefListener);

        setContentView(R.layout.main);

        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int oldVersion = preferences.getInt("version_code", 3);

            onUpgrade(oldVersion, pinfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOG_NAME, "Failed to get the package information.", e);
        }

        listFragment = (SystemListFragment) getFragmentManager().findFragmentById(R.id.system_list_fragment);

        upgradeReceiver = new OnUpgradeReceiver();
        registerReceiver(upgradeReceiver,
                         new IntentFilter("android.intent.action.MY_PACKAGE_REPLACED"));
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        unregisterReceiver(upgradeReceiver);

        preferences.unregisterOnSharedPreferenceChangeListener(prefListener);
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        if (intent.hasExtra("modified_list")) {
            String[] list = intent.getStringArrayExtra("modified_list");
            for (String item : list) {
                Log.d(LOG_NAME, "Modified: " + item);
            }

            listFragment.invalidate();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if ((resultCode == RESULT_OK) && (requestCode == RESULT_SYSTEM_INFO)) {
            String[] list = data.getStringArrayExtra("modified_list");
            for (String item : list) {
                Log.d(LOG_NAME, "Modified: " + item);
            }

            listFragment.invalidate();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if ((item.getItemId() == android.R.id.home) &&
            drawerLayout.isDrawerVisible(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSystemSelected(String system, View view, int position, long id)
    {
        Log.d(LOG_NAME, "System Selected: " + system);

        CmdrSystemInfo info = dbHelper.getSystem(system);

//        Intent intent = new Intent(getApplicationContext(), SystemInfoActivity.class);
        Intent intent = new Intent(getApplicationContext(), CelestialListActivity.class);
        intent.putExtra("system", info);

        startActivityForResult(intent, RESULT_SYSTEM_INFO);
    }

    private class OnUpgradeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            switch (intent.getAction()) {
                case Intent.ACTION_MY_PACKAGE_REPLACED:
                    try {
                        PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        int oldVersion = preferences.getInt("version_code", 3);

                        onUpgrade(oldVersion, pinfo.versionCode);
                    } catch (PackageManager.NameNotFoundException e) {
                        Log.e(LOG_NAME, "Failed to get the package information.", e);
                    }

                    break;
                default:
                    break;
            }
        }
    }

    private void onUpgrade(int oldVersion, int newVersion)
    {
        Log.d(LOG_NAME, "oldVersion: " + Integer.toString(oldVersion) + ", newVersion: " + Integer.toString(newVersion));

        if (oldVersion != newVersion) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                                               .edit();

            switch (oldVersion) {
                case 2:
                case 3:
                    editor.putString(PreferenceTag.CMDR_NAME, dbHelper.getCmdrName());
                    editor.putInt(PreferenceTag.DISPLAY_DAYS, dbHelper.getDisplayDays());
                    editor.putString(PreferenceTag.ROUTE, dbHelper.getRouteDestination());
                    break;
                default:
                    break;
            }

            editor.putInt("version_code", newVersion);
            editor.apply();
        }
    }
}
