ALTER TABLE Planets RENAME TO PlanetDead;
ALTER TABLE Stars RENAME TO StarDead;

DROP INDEX IF EXISTS CmdrSystemCoordIndex;
DROP INDEX IF EXISTS CmdrFromDistIndex;
DROP INDEX IF EXISTS CmdrToDistIndex;
DROP INDEX IF EXISTS CmdrDistanceIndex;
DROP INDEX IF EXISTS CmdrFromRefDistIndex;
DROP INDEX IF EXISTS CmdrToRefDistIndex;
DROP INDEX IF EXISTS CmdrRefDistanceIndex;
DROP INDEX IF EXISTS StarIndex;
DROP INDEX IF EXISTS PlanetIndex;

CREATE TABLE Satellites (_id INTEGER PRIMARY KEY, SystemId INTEGER NOT NULL, BodyId INTEGER NOT NULL, CategoryId INTEGER NOT NULL, ParentId INTEGER, ParentCategoryId INTEGER);
CREATE TABLE Stars (_id INTEGER PRIMARY KEY, Type INTEGER NOT NULL, ScanLevel INTEGER NOT NULL, Name TEXT COLLATE NOCASE, Distance DOUBLE, Age DOUBLE, Mass DOUBLE, Radius DOUBLE, SurfaceTemp DOUBLE, OrbitalPeriod DOUBLE, SemiMajorAxis DOUBLE, OrbitalEccentricity DOUBLE, OrbitalInclination DOUBLE, ArgPeriapsis DOUBLE);
CREATE TABLE Planets (_id INTEGER PRIMARY KEY, Type INTEGER NOT NULL, ScanLevel INTEGER NOT NULL, Name TEXT COLLATE NOCASE, Distance DOUBLE, Mass DOUBLE, Radius DOUBLE, SurfaceTemp DOUBLE, SurfacePressure DOUBLE, Volcanism BOOLEAN, AtmosphereType INTEGER, OrbitalPeriod DOUBLE, SemiMajorAxis DOUBLE, OrbitalEccentricity DOUBLE, OrbitalInclination DOUBLE, ArgPeriapsis DOUBLE, RotationPeriod DOUBLE, TidalLocked BOOLEAN, AxisTilt DOUBLE);
CREATE TABLE Asteroids (_id INTEGER PRIMARY KEY, Type INTEGER NOT NULL, Distance DOUBLE, MoonMasses DOUBLE, OrbitalPeriod DOUBLE, SemiMajorAxis DOUBLE, OrbitalEccentricity DOUBLE, OrbitalInclination DOUBLE, ArgPeriapsis DOUBLE);
CREATE TABLE Rings (_id INTEGER PRIMARY KEY, BodyId INTEGER NOT NULL, CategoryId INTEGER NOT NULL, Type INTEGER, Quality INTEGER, Level INTEGER, Mass DOUBLE, SemiMajorAxis DOUBLE, InnerRadius DOUBLE, OuterRadius DOUBLE);

INSERT INTO Stars (_id, Type, ScanLevel) SELECT _id, Star, 2 FROM StarDead;
INSERT INTO Satellites (SystemId, BodyId, CategoryId) SELECT SystemId, _id, 1 FROM StarDead;

INSERT INTO Planets (_id, Type, ScanLevel) SELECT _id, Planet, 2 FROM PlanetDead;
INSERT INTO Satellites (SystemId, BodyId, CategoryId) SELECT SystemId, _id, 2 FROM PlanetDead;

DROP TABLE PlanetDead;
DROP TABLE StarDead;

CREATE INDEX CmdrSystemCoordIndex ON CmdrSystems(xCoord);
CREATE INDEX CmdrFromDistIndex ON CmdrDistances(FromSystem);
CREATE INDEX CmdrToDistIndex ON CmdrDistances(ToSystem);
CREATE INDEX CmdrDistanceIndex ON CmdrDistances(Distance);
CREATE INDEX CmdrFromRefDistIndex ON CmdrRefDistances(FromSystem);
CREATE INDEX CmdrToRefDistIndex ON CmdrRefDistances(ToSystem);
CREATE INDEX CmdrRefDistanceIndex ON CmdrRefDistances(Distance);

CREATE INDEX SatellitesIndex ON Satellites(SystemId, ParentId, ParentCategoryId);
