package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 11/7/15.
 */
public enum Atmosphere
{
    Oxygen(0),
    Carbon_Dioxide(1),
    Ammonia(2),
    Unknown(0xFFFFFFFF);

    public final int value;

    Atmosphere(int value)
    {
        this.value = value;
    }

    public static Atmosphere getAtmosphere(int value)
    {
        for (Atmosphere type : Atmosphere.values()) {
            if (type.value == value) {
                return type;
            }
        }

        return Unknown;
    }
}
