package com.fussyware.AndDiscovered.edsm;

import android.provider.BaseColumns;

/**
 * Created by wes on 6/15/15.
 */
public final class EDSMDbContract
{
    public static abstract class Systems implements BaseColumns
    {
        public static final String TABLE_NAME = "EDSMSystems";
        public static final String COLUMN_NAME_SYSTEM = "System";
        public static final String COLUMN_NAME_XCOORD = "xCoord";
        public static final String COLUMN_NAME_YCOORD = "yCoord";
        public static final String COLUMN_NAME_ZCOORD = "zCoord";
//        public static final String COLUMN_NAME_ = "";
    }

    public static abstract class Distances implements BaseColumns
    {
        public static final String TABLE_NAME = "EDSMDistances";
        public static final String COLUMN_NAME_FROM = "FromSystem";
        public static final String COLUMN_NAME_TO = "ToSystem";
        public static final String COLUMN_NAME_DISTANCE = "Distance";
//        public static final String COLUMN_NAME_ = "";
    }

    public static abstract class DbInfo implements BaseColumns
    {
        public static final String TABLE_NAME = "DbInfo";
        public static final String COLUMN_NAME_CONFIG = "ConfigName";
        public static final String COLUMN_NAME_VALUE = "Value";

        public static final String CONFIG_NAME_LAST_EDSM_DISTANCE_CHECK = "LastEDSMDistanceCheck";
        public static final String CONFIG_NAME_LAST_EDSM_SYSTEM_CHECK = "LastEDSMSystemCheck";
//        public static final String COLUMN_NAME_ = "";
    }
}
