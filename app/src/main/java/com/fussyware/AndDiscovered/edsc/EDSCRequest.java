package com.fussyware.AndDiscovered.edsc;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by wes on 6/16/15.
 */
public class EDSCRequest extends EDSCJSON
{
    public static final int DEFAULT_OUTPUT_MODE = 1;
    public static final int DEFAULT_KNOWN_STATUS = 0;
    public static final String DEFAULT_SYSTEM_NAME = "";
    public static final int DEFAULT_CR = 5;

    public static final Date DEFAULT_DATE()
    {
        return new Date(new Date().getTime() - (24 /*hours*/ * 60 /*minutes*/ * 60 /*seconds*/ * 1000 /*milliseconds*/));
    }

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    static {
        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private int outputMode = DEFAULT_OUTPUT_MODE;
    private int knownStatus = DEFAULT_KNOWN_STATUS;
    private String system = DEFAULT_SYSTEM_NAME;
    private int cr = DEFAULT_CR;
    private Date date = null;

    public void setKnownStatus(int knownStatus)
    {
        synchronized (mutex) {
            this.knownStatus = knownStatus;
        }
    }

    public void setSystem(String system)
    {
        synchronized (mutex) {
            this.system = system;
        }
    }

    public void setCr(int cr)
    {
        synchronized (mutex) {
            this.cr = cr;
        }
    }

    public void setDate(Date date)
    {
        synchronized (mutex) {
            this.date = date;
        }
    }

    public void setOutputMode(int mode)
    {
        synchronized (mutex) {
            outputMode = mode;
        }
    }

    public int getOutputMode()
    {
        synchronized (mutex) {
            return outputMode;
        }
    }

    public int getKnownStatus()
    {
        synchronized (mutex) {
            return knownStatus;
        }
    }

    public String getSystem()
    {
        synchronized (mutex) {
            return system;
        }
    }

    public int getCr()
    {
        synchronized (mutex) {
            return cr;
        }
    }

    public Date getDate()
    {
        synchronized (mutex) {
            if (null == date) {
                return DEFAULT_DATE();
            } else {
                return date;
            }
        }
    }

    @Override
    protected void fillJSON(JSONObject baseObject) throws JSONException
    {
        if (DEFAULT_OUTPUT_MODE != outputMode) {
            baseObject.put("outputmode", outputMode);
        }

        if ((DEFAULT_KNOWN_STATUS != knownStatus) ||
                (DEFAULT_CR != cr) ||
                (!DEFAULT_SYSTEM_NAME.equals(system)) ||
                (null != date)) {
            JSONObject o = new JSONObject();

            if (DEFAULT_KNOWN_STATUS != knownStatus) {
                o.put("knownstatus", knownStatus);
            }

            if (DEFAULT_CR != cr) {
                o.put("cr", cr);
            }

            if (!DEFAULT_SYSTEM_NAME.equals(system)) {
                o.put("systemname", system);
            }

            if (null != date) {
                o.put("date", DATE_FORMATTER.format(date));
            }

            baseObject.put("filter", o);
        }
    }
}
