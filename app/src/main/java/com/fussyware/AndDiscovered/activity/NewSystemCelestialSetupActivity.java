package com.fussyware.AndDiscovered.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.StarIconGridFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

/**
 * Created by wes on 11/29/15.
 */
public class NewSystemCelestialSetupActivity
        extends BaseActivity
        implements StarIconGridFragment.OnStarClickListeners
{
    private static final String LOG_NAME = NewSystemCelestialSetupActivity.class.getSimpleName();

    private SystemInfo systemInfo;
    private CmdrDbHelper dbHelper;

    private StarIconGridFragment starFragment = new StarIconGridFragment();
    private View slideHandlebar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.new_system_celestial_setup_layout);
        setDrawerIndicatorEnabled(false);

        dbHelper = CmdrDbHelper.getInstance();

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.icon_selection_frame,
               starFragment,
               FragmentTag.star_icon_grid_fragment.tag);
        ft.commit();

        slideHandlebar = findViewById(R.id.slide_handle_bar);
        if (slideHandlebar != null) {
            SlidingUpPanelLayout slidePanel = (SlidingUpPanelLayout) findViewById(R.id.slide_up_panel);
            slidePanel.setPanelSlideListener(new SlidingUpPanelLayout.SimplePanelSlideListener()
            {
                @Override
                public void onPanelSlide(View panel, float slideOffset)
                {
                    if (slideHandlebar.getVisibility() != View.GONE) {
                        slideHandlebar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onPanelCollapsed(View panel)
                {
                    slideHandlebar.setVisibility(View.VISIBLE);
                }
            });
        }

        if (savedInstanceState == null) {
            systemInfo = getIntent().getParcelableExtra("system");
        } else {
            systemInfo = savedInstanceState.getParcelable("system");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("system", systemInfo);
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        super.onBackPressed();
        return true;
    }

    @Override
    public void onStarIconClick(int position, View view, StarType type)
    {
        Log.d(LOG_NAME, type.toString());
    }

    @Override
    public void onStarLongClick(int position, View view, StarType type)
    {
        Log.d(LOG_NAME, type.toString());
    }
}
