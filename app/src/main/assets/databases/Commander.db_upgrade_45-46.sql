CREATE TABLE CmdrRefDistances (_id INTEGER PRIMARY KEY, FromSystem TEXT COLLATE NOCASE, ToSystem TEXT COLLATE NOCASE, Distance DOUBLE);
CREATE INDEX CmdrRefDistIndex ON CmdrRefDistances(FromSystem, ToSystem);
